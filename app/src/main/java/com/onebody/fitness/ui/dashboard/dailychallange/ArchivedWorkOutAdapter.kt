package com.onebody.fitness.ui.dashboard.dailychallange

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.onebody.fitness.R
import com.onebody.fitness.retrofit.SessionManager
import com.onebody.fitness.ui.dashboard.greatyou.GreatYouActivity
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ArchivedWorkOutAdapter(
    private val context: Context?,
    var archivedWorkOutModel: MutableList<ArchivedWorkOutModel.Data>?,
    val is_fromFragment: Boolean
) : RecyclerView.Adapter<ArchivedWorkOutAdapter.ViewHolder>() {

    private lateinit var mSessionManager: SessionManager

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_archived_work_out, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SimpleDateFormat")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        try {
            val cm = archivedWorkOutModel?.get(position)
            holder.tvStationName.text = cm?.station_name
            holder.tvStationName.text = cm?.station_name
            holder.tvTime.text = cm?.workoutmInutes+"  min"

            val dateString = cm?.date

           /* val date: String = dateString!!.substring(0, 10)
            println("Before:-" + date)

            val d = SimpleDateFormat("dd/MM/yyyy")
            try {
                val date1 = d.parse(date)
                println("Final Date:-" + date1)

            } catch (e: ParseException) {
                e.printStackTrace();
            }*/
            val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val outputFormat: DateFormat = SimpleDateFormat("dd MMM yyyy")
            val inputDateStr = dateString
            val date: Date = inputFormat.parse(inputDateStr)
            val outputDateStr: String = outputFormat.format(date)
            holder.tvDate.text = outputDateStr

            mSessionManager = SessionManager(context)
            mSessionManager.setData(SessionManager.VIDEOID, cm!!.videoid.toString())
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
        holder.card_view.setOnClickListener {
            val intent = Intent(context, GreatYouActivity::class.java)
            intent.putExtra("goal_comment",archivedWorkOutModel?.get(position)?.goal_comment)
            context!!.startActivity(intent)
            Animatoo.animateSlideLeft(context)
        }
    }

    override fun getItemCount(): Int {
        if (is_fromFragment) {

            if (archivedWorkOutModel?.size == null) {
                return 0
            }
            if (archivedWorkOutModel?.size == 1) {
                return 1
            }
            if (archivedWorkOutModel?.size == 2) {
                return 2
            }
            if (archivedWorkOutModel?.size == 3) {
                return 3
            }
            if (archivedWorkOutModel?.size!! >= 4) {
                return 3
            }
        }
        return archivedWorkOutModel?.size!!
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val tvStationName: TextView = itemView.findViewById(R.id.tvStationName)
        val tvDate: TextView = itemView.findViewById(R.id.tvDate)
        val card_view: CardView = itemView.findViewById(R.id.card_view)
        val tvTime: TextView = itemView.findViewById(R.id.tvTime)
    }
}