package com.onebody.fitness.ui.dashboard.dailychallangedetail

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.hardware.display.DisplayManager
import android.hardware.display.DisplayManager.DisplayListener
import android.view.Display
import android.widget.RemoteViews
import com.onebody.fitness.R

class MiracastWidgetProvider : AppWidgetProvider() {
    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        val length = appWidgetIds.size
        for (i in 0 until length) {
            val appWidgetId = appWidgetIds[i]
            val intent = Intent(context, MeeraCastActivity::class.java)

            intent.putExtra(MeeraCastActivity.EXTRA_WIDGET_LAUNCH, true)

            val pendingIntent = PendingIntent.getActivity(
                context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT
            )
            val views = RemoteViews(context.packageName, R.layout.miracast_widget)
            views.setOnClickPendingIntent(R.id.widget_layout_parent, pendingIntent)
            val displayManager = context.getSystemService(Context.DISPLAY_SERVICE) as DisplayManager
            val displays = displayManager.displays
            var displaySet = false
            var currentDisplay = -1
            for (j in displays.indices) {
                val display = displays[j]
                if (display.displayId != Display.DEFAULT_DISPLAY) {
                    views.setTextViewText(R.id.widget_text, display.name)
                    views.setTextColor(
                        R.id.widget_text,
                        context.resources.getColor(R.color.holo_blue_bright)
                    )
                    currentDisplay = display.displayId
                    displaySet = true
                }
            }
            if (!displaySet) {
                views.setTextViewText(R.id.widget_text, "Cast Screen")
                views.setTextColor(R.id.widget_text, context.resources.getColor(R.color.white))
            }
            val displayListener: MiracastDisplayListener = MiracastDisplayListener(
                currentDisplay,
                views,
                displayManager,
                appWidgetManager,
                appWidgetId,
                context
            )
            displayManager.registerDisplayListener(displayListener, null)

            // Tell the AppWidgetManager to perform an update on the current app widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }

    private inner class MiracastDisplayListener(
        currentDisplay: Int,
        widgetRemoteViews: RemoteViews,
        displayManager: DisplayManager,
        appWidgetManager: AppWidgetManager,
        appWidgetId: Int,
        context: Context
    ) :
        DisplayListener {
        var mCurrentDisplay = -1
        var mViews: RemoteViews
        var mDisplayManager: DisplayManager
        var mAppWidgetId: Int
        var mAppWidgetManager: AppWidgetManager
        var mContext: Context
        override fun onDisplayRemoved(displayId: Int) {
            if (displayId == mCurrentDisplay) {
                mCurrentDisplay = -1
            }
            mViews.setTextViewText(R.id.widget_text, "Cast Screen")
            mViews.setTextColor(R.id.widget_text, mContext.resources.getColor(R.color.white))

            // Tell the AppWidgetManager to perform an update on the current app widget
            mAppWidgetManager.updateAppWidget(mAppWidgetId, mViews)
        }

        override fun onDisplayChanged(displayId: Int) {}
        override fun onDisplayAdded(displayId: Int) {
            mCurrentDisplay = displayId
            val display = mDisplayManager.getDisplay(displayId)
            mViews.setTextViewText(R.id.widget_text, display.name)
            mViews.setTextColor(
                R.id.widget_text,
                mContext.resources.getColor(R.color.holo_blue_bright)
            )

            // Tell the AppWidgetManager to perform an update on the current app widget
            mAppWidgetManager.updateAppWidget(mAppWidgetId, mViews)
        }

        init {
            mCurrentDisplay = currentDisplay
            mViews = widgetRemoteViews
            mDisplayManager = displayManager
            mAppWidgetManager = appWidgetManager
            mAppWidgetId = appWidgetId
            mContext = context
        }
    }
}
