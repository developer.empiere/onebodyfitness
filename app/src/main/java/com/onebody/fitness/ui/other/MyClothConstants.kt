package com.onebody.fitness.ui.other

class MyClothConstants {

    val clothId: String = "ClothId"
    val clothStyle: String = "ClothStyle"

    companion object
    {
        enum class CLOTH_STYLE_PROPERTY{
            TANK, SHORTS, SWEATS, T_SHIRT
        }
    }
}