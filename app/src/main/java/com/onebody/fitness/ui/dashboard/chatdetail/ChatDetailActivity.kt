package com.onebody.fitness.ui.dashboard.chatdetail

import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityChatDetailBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import kotlinx.android.synthetic.main.chat_toolbar.*
import org.json.JSONObject
import java.lang.reflect.Type


class ChatDetailActivity : AppCompatActivity() {

    lateinit var binding: ActivityChatDetailBinding

    private var chatDetailAdapter: ChatDetailAdapter? = null
    private var layoutManager: LinearLayoutManager? = null
    private var chatDetailModel = ArrayList<ChatDetailModel.Data>()
    val ha = Handler()

    var id: String = ""
    var name: String = ""
    var profilepic: String = ""
    var message: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityChatDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        id = intent.getStringExtra("id").toString()
        name = intent.getStringExtra("name").toString()
        profilepic = intent.getStringExtra("profile_pic").toString()

        binding.editChatMessage.requestFocus()

        getChatMessages()

        img_back.setOnClickListener(View.OnClickListener {
            onBackPressed()
            Animatoo.animateSlideRight(this)

        })
        binding.imgSend.setOnClickListener {
            sendMsg()
        }
        ha.postDelayed(object : Runnable {
            override fun run() {
                getChatMessages()
                ha.postDelayed(this, 10000)
            }
        }, 10000)


        binding.editChatMessage.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                sendMsg()
            }
            false
        })

        tv_chat_name.text = name
        Glide.with(this)
            .load(profilepic)
            .placeholder(R.drawable.ic_user_profile)
            .into(img_profile)
    }

    /* private fun createChat() {
         val map = HashMap<String, String>()
         map.put("RecieverUserID", id)

         ApiServiceProvider.getInstance(this).sendHashmapPostData(
             Constants.UrlPath.CREATE_CHAT, map, true,
             RetrofitListener { response ->
                 try {
                     val responsObj = JSONObject(response.body().toString())
                 } catch (e: Exception) {
                        e.printStackTrace()
                 }
             })
     }*/

    private fun getChatMessages() {
        val map = HashMap<String, String>()
        map.put("ChatID", id)

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.CHAT_MESSAGE_LIST, map, false,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")
                    val message = responsObj.getString("message")
                    if (status) {
                        val data = responsObj.getJSONArray("data")
                        val gson = Gson()
                        val listType: Type =
                            object : TypeToken<MutableList<ChatDetailModel.Data?>?>() {}.type
                        chatDetailModel = gson.fromJson(data.toString(), listType)
                        setAdapter()
                    } else {
                        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun sendMsg() {
        val map = HashMap<String, String>()
        map.put("ChatID", id)
        map.put("Message", binding.editChatMessage.text.toString())

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.SEND_MESSAGE, map, true,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")
                    if (status) {
                        getChatMessages()
                        binding.editChatMessage.text.clear()
                    } else {
                        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {

                    e.printStackTrace()
                }
            })
    }


    private fun setAdapter() {
        chatDetailAdapter = ChatDetailAdapter(name, this, chatDetailModel)
        layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        layoutManager!!.reverseLayout = true
        binding.rvChat.layoutManager = layoutManager
        binding.rvChat.adapter = chatDetailAdapter

        binding.rvChat.scrollToPosition(chatDetailModel!!.size)
        chatDetailAdapter!!.notifyDataSetChanged()
    }
}
