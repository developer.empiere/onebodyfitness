package com.onebody.fitness.ui.dashboard.avatar

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityFinalAvatarBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.retrofit.SessionManager
import com.onebody.fitness.ui.dashboard.DashBoardActivity
import org.json.JSONObject

class AvatarViewActivity : AppCompatActivity() {

    lateinit var binding: ActivityFinalAvatarBinding

    //male
    var selectedSkinColor: Int = -1
    var selectedHairColor: Int = -1
    var selectedFacialHair: Int = -1
    var selectedHairStyle: Int = -1

    //fermale
    var selectedSkinColorf: Int = -1
    var selectedHairColorf: Int = -1
    var selectedHairStylef: Int = -1

    var clothesID: String = "0"
    var bodyType: String = "0"

    var clothesIDF: String = "0"
    var bodyTypeF: String = "0"

    var email: String = ""
    var password: String = ""

    private lateinit var mSessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFinalAvatarBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mSessionManager = SessionManager(applicationContext)

        email = mSessionManager.getData(SessionManager.EMAIL)!!
        password = mSessionManager.getData(SessionManager.PASSWORD)!!

        bodyType = intent.getStringExtra("BodyType").toString()
        selectedSkinColor = intent.getIntExtra("SkinID", -1)
        selectedHairColor = intent.getIntExtra("HairColor", -1)
        selectedFacialHair = intent.getIntExtra("FacialHair", -1)
        selectedHairStyle = intent.getIntExtra("HairStyle", -1)
        clothesID = intent.getStringExtra("ClothID").toString()


        bodyTypeF = intent.getStringExtra("BodyTypeF").toString()
        selectedSkinColorf = intent.getIntExtra("SkinIDF", -1)
        selectedHairColorf = intent.getIntExtra("HairColorF", -1)
        selectedHairStylef = intent.getIntExtra("HairStyleIDF", -1)
        clothesIDF = intent.getStringExtra("ClothIDF").toString()

        previewAvatar()
        binding.btnSubmit.setOnClickListener {

            if (mSessionManager.getBooleanData(SessionManager.IS_MALE, true)) {
                SubmitMaleAvatarApi()
            } else {
                SubmitFeMaleAvatarApi()
            }
        }
    }

    private fun SubmitFeMaleAvatarApi() {

        val map = java.util.HashMap<String, String>()
        map["BodyType"] = bodyTypeF
        map["SkinID"] = selectedSkinColor.toString()
        map["HairColorID"] = selectedHairColor.toString()
        map["HairStyleID"] = selectedHairStyle.toString()
        map["ClothingShortId"] = clothesID
        if(mSessionManager.getBooleanData(SessionManager.IS_MALE)){
            map["Gender"] = "male"
        }else{
            map["Gender"] = "female"
        }
        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_SUBMIT_AVATAR_FEMALE, map, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    val message = responseObj.getString("message")

                    if (status) {

                        Toast.makeText(this, "Avtar Submitted Successfully" , Toast.LENGTH_SHORT).show()

                        if (mSessionManager!!.getBooleanData(SessionManager.IS_FROM_PROFILE)) {
                            val i = Intent(this, DashBoardActivity::class.java)
                            startActivity(i)
                            finish()
                        } else {
                            loginApiCall()
                        }
                    } else {
                        Toast.makeText(this, ""+message , Toast.LENGTH_SHORT).show()

                    }
                } catch (e: Exception) {
                }
            })
    }

    private fun SubmitMaleAvatarApi() {

        val map = HashMap<String, String>()

        map.put("BodyType", bodyType)
        map.put("SkinID", selectedSkinColor.toString())
        map.put("HairColorID", selectedHairColor.toString())
        // map.put("Gender", .toString())
        map.put("FacialHairID", selectedFacialHair.toString())
        map.put("HairStyleID", selectedHairStyle.toString())
        map.put("ClothingShortId", clothesID)
        if(mSessionManager.getBooleanData(SessionManager.IS_MALE)){
            map["Gender"] = "male"
        }else{
            map["Gender"] = "female"
        }

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_SUBMIT_AVATAR_MALE, map, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    val message = responseObj.getString("message")

                    if (status) {

                        Toast.makeText(this, "Avtar Submitted Successfully" , Toast.LENGTH_LONG).show()

                        if (mSessionManager.getBooleanData(SessionManager.IS_FROM_PROFILE)) {
                            val i = Intent(this, DashBoardActivity::class.java)
                            startActivity(i)
                            finish()

                        } else {
                            loginApiCall()
                        }

                    } else {
                        Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun loginApiCall() {
        val requestData = JsonObject()
        requestData.addProperty("email", email)
        requestData.addProperty("psw", password)

        ApiServiceProvider.getInstance(this)
            .sendPostData(Constants.UrlPath.LOGIN, requestData, true,
                RetrofitListener { response ->

                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")

                        if (status) {
                            Toast.makeText(this,"Login Successfully",Toast.LENGTH_SHORT).show()

                            val response = responseObj.getJSONArray("data")
                            val name = response.getJSONObject(0).getString("name")
                            val id = response.getJSONObject(0).getString("id")
                            val email = response.getJSONObject(0).getString("email")
                            val isSocialLogin = response.getJSONObject(0).getString("IsSocialLogin")
                            val avatarStatus = response.getJSONObject(0).getString("avatarStatus")
                            val token = responseObj.getString("token")

                            mSessionManager.setData(SessionManager.AUTH_TOKEN, token)
                            mSessionManager.setData(SessionManager.USER_NAME, name)
                            mSessionManager.setData(SessionManager.USER_ID, id)
                            mSessionManager.setData(SessionManager.EMAIL, email)
                            mSessionManager.setData(SessionManager.IIS_SOCIAL_LOGIN, isSocialLogin)
                            mSessionManager.setData(SessionManager.AVATAR_STATUS, avatarStatus)

                            Animatoo.animateSlideLeft(this)

                            val i = Intent(this, DashBoardActivity::class.java)
                            startActivity(i)
                            finish()

                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun previewAvatar() {
        val map = HashMap<String, String>()
        map.put("BodyType", bodyType)
        map.put("SkinID", selectedSkinColor.toString())
        map.put("HairColorID", selectedHairColor.toString())
        map.put("FacialHairID", selectedFacialHair.toString())
        map.put("HairStyleID", selectedHairStyle.toString())
        map.put("ClothingShortId", clothesID)

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_PREVIEWAVTAR, map, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())

                    val status = responseObj.getBoolean("status")
                    val message = responseObj.getString("message")

                    if (status) {

                        var url = responseObj.getString("avtar_url")

                        Glide.with(this)
                            .load(url)
                            .error(R.drawable.ic_launcher_background)
                            .placeholder(R.drawable.ic_launcher_foreground)
                            .into(binding.imgAvatar)

                        /*  if (mSessionManager.getBooleanData(SessionManager.IS_FROM_PROFILE)) {
                              val i = Intent(this, DashBoardActivity::class.java)
                              startActivity(i)
                              finish()

                          } else {
                              loginApiCall()
                          }*/

                    } else {
                        Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }
}