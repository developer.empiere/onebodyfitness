package com.onebody.fitness.ui.dashboard.dailychallange

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.Button
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.onebody.fitness.R
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.AppUtils.dimBehind
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.PsDialogs
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.dailychallengelist.DailyChallengeList
import com.onebody.fitness.ui.dashboard.homemore.HomeMoreStationActivity
import kotlinx.android.synthetic.main.activity_daily_challenge.view.rvWorkOut
import kotlinx.android.synthetic.main.activity_daily_challenge.view.tvSeeMore
import kotlinx.android.synthetic.main.fragment_daily_challange.*
import kotlinx.android.synthetic.main.fragment_daily_challange.view.*
import org.json.JSONObject


class DailyChallangeFragment : Fragment() {

    lateinit var mRootView: View
    private var layoutManager: LinearLayoutManager? = null

    private var archivedWorkOutAdapter: ArchivedWorkOutAdapter? = null
    private var archivedWorkOutModel: MutableList<ArchivedWorkOutModel.Data>? = null

    private var dailyChallangeDialogAdapter: DailyChallangeDialogAdapter? = null
    private var dailyChallangeDialogModel: MutableList<DailyChallangeDialogModel.Data>? = null

    var selectedRadioPosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mRootView = inflater.inflate(R.layout.fragment_daily_challange, container, false)

        setWorkOutAdapter()
        getDailyChallenge()
        getMyArchivedWorkouts()

        mRootView.tvSeeMore.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, HomeMoreStationActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(context)

        })
        mRootView.tvStart.setOnClickListener(View.OnClickListener {
            selectedRadioPosition = 0
            getLevelList()

        })

        return mRootView
    }

    private fun getLevelList() {
        var jsonObject = JsonObject()

        ApiServiceProvider.getInstance(context).sendPostData(
            Constants.UrlPath.GET_LEVELS, jsonObject, true,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")
                    val message = responsObj.getString("message")

                    if (status) {

                        val data = responsObj.getJSONArray("data")

                        dailyChallangeDialogModel = Gson().fromJson<Any>(
                            data.toString(),
                            object :
                                TypeToken<MutableList<DailyChallangeDialogModel.Data>?>() {}.type
                        ) as MutableList<DailyChallangeDialogModel.Data>
                        object : TypeToken<MutableList<DailyChallangeDialogModel.Data?>?>() {}.type

                        showPopupStart(mRootView.tvStart)

                    } else {
                        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {

                    e.printStackTrace()
                }
            })
    }

    @SuppressLint("ClickableViewAccessibility", "InflateParams")
    fun showPopupStart(view: View) {

        val inflater =
            view.context.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView: View = inflater.inflate(R.layout.popup_daily_challange_dialog, null)
        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = false

        val rcvLevel: RecyclerView = popupView.findViewById(R.id.rcvLevel) as RecyclerView
        val btnok: Button = popupView.findViewById(R.id.btnok) as Button

        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(view, Gravity.CENTER, width, height)

        dimBehind(popupWindow)

        dailyChallangeDialogAdapter =
            DailyChallangeDialogAdapter(context, dailyChallangeDialogModel)
        layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        rcvLevel.layoutManager = layoutManager
        rcvLevel.adapter = dailyChallangeDialogAdapter

        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }

        dailyChallangeDialogAdapter?.setOnClickListener {
            selectedRadioPosition = it
        }
        btnok.setOnClickListener {

            if (!checkValidationForAllRadio()) {
                Animatoo.animateSlideLeft(context)
                getSubmitLevel(popupWindow)
            }
        }
    }

    private fun checkValidationForAllRadio(): Boolean {

        if (selectedRadioPosition == 0) {
            PsDialogs().error(context, getString(R.string.alert_select_item))
            return true
        }
        return false
    }

    private fun getSubmitLevel(popupWindow: PopupWindow) {

        val map = HashMap<String, String>()
        map.put("levelid", selectedRadioPosition.toString())

        ApiServiceProvider.getInstance(context).sendHashmapPostData(
            Constants.UrlPath.GET_LEVELS_SUBMIT, map, true,
            RetrofitListener { response ->

                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")
                    val message = responsObj.getString("message")

                    if (status) {

                        popupWindow.dismiss()

                        val intent = Intent(context, DailyChallengeList::class.java)
                        startActivity(intent)
                        Animatoo.animateSlideLeft(context)
                        selectedRadioPosition = 0

                    } else {
                        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show()

                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun getMyArchivedWorkouts() {
        var jsonObject = JsonObject()

        ApiServiceProvider.getInstance(context).sendPostData(
            Constants.UrlPath.MY_ARCHIVED_WORKOUTS, jsonObject, true,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")

                    if (status) {
                        val data = responsObj.getJSONArray("data")

                        archivedWorkOutModel = Gson().fromJson<Any>(
                            data.toString(),
                            object : TypeToken<MutableList<ArchivedWorkOutModel.Data>?>() {}.type
                        ) as MutableList<ArchivedWorkOutModel.Data>
                        object : TypeToken<MutableList<ArchivedWorkOutModel.Data?>?>() {}.type
                        setAdapterArchivedWork()
                    }
                } catch (e: Exception) {

                    e.printStackTrace()
                }
            })
    }

    private fun setAdapterArchivedWork() {
        archivedWorkOutAdapter = ArchivedWorkOutAdapter(context, archivedWorkOutModel, true)
        layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        layoutManager!!.setStackFromEnd(true)
        rvWorkOut.layoutManager = layoutManager
        rvWorkOut.adapter = archivedWorkOutAdapter

    }

    private fun getDailyChallenge() {

        val requestData = JsonObject()

        ApiServiceProvider.getInstance(context)
            .sendPostData(Constants.UrlPath.GET_DAILY_CHALLENGE, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")

                        if (status) {

                            val response = responseObj.getJSONArray("data")
                            val stationName = response.getJSONObject(0).getString("staion_name")
                            val comment = response.getJSONObject(0).getString("comment")
//                            val video = response.getJSONObject(0).getString("video")

                            tvStationName.setText(stationName)
                            tvComment.setText(comment)

                            val size = response.length()
                            if (size == 1) {
                                val jsonObjectone = response.getJSONObject(0)

                                val videoUri = getMedia(jsonObjectone.getString("video"))
                                videoView1.setVideoURI(videoUri)
                                videoView1.seekTo(1000)

                            } else if (size == 2) {
                                val jsonObjectone = response.getJSONObject(0)
                                val jsonObjecttwo = response.getJSONObject(1)

                                val videoUri1 = getMedia(jsonObjectone.getString("video"))
                                videoView1.setVideoURI(videoUri1)
                                videoView1.seekTo(5000)

                                val videoUri2 = getMedia(jsonObjecttwo.getString("video"))
                                videoView2.setVideoURI(videoUri2)
                                videoView2.seekTo(5000)

                            } else if (size >= 3) {
                                val jsonObjectone = response.getJSONObject(0)
                                val jsonObjecttwo = response.getJSONObject(1)

                                val videoUri1 = getMedia(jsonObjectone.getString("video"))
                                videoView1.setVideoURI(videoUri1)
                                videoView1.seekTo(5000)

                                val videoUri2 = getMedia(jsonObjecttwo.getString("video"))
                                videoView2.setVideoURI(videoUri2)
                                videoView2.seekTo(5000)

                                val counterSize = size - 2
                                tvVideoCounter.setText("+$counterSize")

                            } else {
                                tvStationName.visibility = View.INVISIBLE
                                tvComment.visibility = View.INVISIBLE
                                videoView1.visibility = View.INVISIBLE
                                videoView1.visibility = View.INVISIBLE
                            }

                        } else {
                            Toast.makeText(context, "" + message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun setWorkOutAdapter() {

        /* marketSliderAdapter = DailyChallengeAdapter(context, dailyChallengeList, true)
         layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
         mRootView.rvWorkOut.layoutManager = layoutManager
         mRootView.rvWorkOut.adapter = marketSliderAdapter*/

        archivedWorkOutAdapter = ArchivedWorkOutAdapter(context, archivedWorkOutModel, true)
        layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        mRootView.rvWorkOut.layoutManager = layoutManager
        mRootView.rvWorkOut.adapter = archivedWorkOutAdapter

    }

    companion object {
        fun newInstance(param1: String, param2: String) =
            DailyChallangeFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }

    private fun getMedia(mediaName: String): Uri? {
        return if (URLUtil.isValidUrl(mediaName)) {
            Uri.parse(mediaName)
        } else {

            // you can also put a video file in raw package and get file from there as shown below
            Uri.parse(
                "android.resource://" + requireContext().packageName.toString() +
                        "/raw/" + mediaName
            )
        }
    }


}