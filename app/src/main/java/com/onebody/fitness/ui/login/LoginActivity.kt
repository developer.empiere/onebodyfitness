package com.onebody.fitness.ui.login

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.transition.Fade
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.Task
import com.google.gson.JsonObject
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityLoginBinding
import com.onebody.fitness.retrofit.*
import com.onebody.fitness.ui.dashboard.DashBoardActivity
import com.onebody.fitness.ui.dashboard.gender.GenderActivity
import com.onebody.fitness.ui.other.AsteriskPasswordTransformationMethod
import com.onebody.fitness.ui.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONArray
import org.json.JSONObject

class LoginActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {

    lateinit var binding: ActivityLoginBinding
    lateinit var context: Context
    private lateinit var mSessionManager: SessionManager
    private var googleApiClient: GoogleApiClient? = null
    private val RC_SIGN_IN = 1
    var isShowText = true
    var email: String = ""
    var gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestEmail()
        .build()

    @SuppressLint("ClickableViewAccessibirrLoginWithMobilelity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupWindowAnimations()

        mSessionManager = SessionManager(applicationContext)
        SessionManager(applicationContext).checkLogin()

        var mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        email = getIntent().getStringExtra("email").toString()

        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()
        googleApiClient = GoogleApiClient.Builder(this).enableAutoManage(this, this)
            .addApi(Auth.GOOGLE_SIGN_IN_API, gso).build()
        binding.edtPassword.transformationMethod = AsteriskPasswordTransformationMethod()

        btnLogin.setOnClickListener(View.OnClickListener {
            if (checkValidation()) {
                //generateMindBodyToken()
                loginAPICall()
            }
        })

        txtSignUp.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        })

        txtForgotPassword.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, ForgotPasswordEmailActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        })

        rrLoginWithGmail.setOnClickListener(View.OnClickListener {
            val signInIntent: Intent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        })

        binding.imgPasswordOn.setOnClickListener(View.OnClickListener {
            isShowText
            edtPassword.transformationMethod = PasswordTransformationMethod.getInstance()
            binding.edtPassword.setSelection(binding.edtPassword.length())
            binding.imgPasswordOn.visibility = View.GONE
            binding.imgPasswordOff.visibility = View.VISIBLE
        })

        binding.imgPasswordOff.setOnClickListener(View.OnClickListener {
            isShowText = false
            edtPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
            binding.edtPassword.setSelection(binding.edtPassword.length())
            binding.imgPasswordOff.visibility = View.GONE
            binding.imgPasswordOn.visibility = View.VISIBLE
        })
    }

    private fun generateMindBodyToken() {
        val requestData = JsonObject()
        requestData.addProperty("Username", Constants.UrlPath.MINDBODY_USERNAME)
        requestData.addProperty("Password", Constants.UrlPath.MINDBODY_PASSWORD)

        ApiServiceProvider.getInstance(this, Constants.UrlPath.MINDBODY_ISSUE_TOKEN)
            .sendPostData(Constants.UrlPath.MINDBODY_ISSUE_TOKEN, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        mSessionManager.setData(
                            SessionManager.MINDBODY_TOKEN,
                            responseObj.getString("AccessToken")
                        )
                        val user: JSONObject = responseObj.getJSONObject("User")
                        mSessionManager.setData(
                            SessionManager.MINDBODY_USER_ID,
                            user.getString("Id")
                        )

                        getMindBodyClients()

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun getMindBodyClients() {

        ApiServiceProvider.getInstance(this, Constants.UrlPath.MINDBODY_GET_CLIENTS)
            .sendPostDataWithoutParams(
                Constants.UrlPath.MINDBODY_GET_CLIENTS,
                "",
                "",
                "",
                edt_email.text.toString(),
                "",
                "",
                true,
                RetrofitListener { response ->
                    try {

                        val responseObj = JSONObject(response.body().toString())
                        Log.d("TAG", "getMindBodyClients: $responseObj")
                        val clients: JSONArray = responseObj.getJSONArray("Clients")
                        for (i in 0 until clients.length()) {
                            val jsonObject: JSONObject = clients.getJSONObject(i)
                            val suspensionInfo: JSONObject =
                                jsonObject.getJSONObject("SuspensionInfo")
                            if (edt_email.text.toString() == jsonObject.getString("Email")) {
                                if (suspensionInfo.getBoolean("BookingSuspended")) {
                                    //loginAPICall()
                                } else {
                                    Toast.makeText(
                                        this,
                                        "you are registered with Mindbody but, your booking is suspended",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            } else {
                                Toast.makeText(
                                    this,
                                    "You are not register with Mindbody... so, you can't login with this app",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }

                        Log.d("mindbody clients", "getMindBodyClients: $responseObj")

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })

    }

    private fun setupWindowAnimations() {
        val fade = Fade()
        fade.duration = 1000
        window.enterTransition = fade
    }

    private fun loginAPICall() {
        val requestData = JsonObject()
        requestData.addProperty("email", edt_email.text.toString())
        requestData.addProperty("psw", edtPassword.text.toString())

        ApiServiceProvider.getInstance(this)
            .sendPostData(Constants.UrlPath.LOGIN, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")

                        if (status) {

                            Toast.makeText(this, "Login Successfully", Toast.LENGTH_SHORT).show()

                            val response = responseObj.getJSONArray("data")
                            val name = response.getJSONObject(0).getString("name")
                            val id = response.getJSONObject(0).getString("id")
                            val email = response.getJSONObject(0).getString("email")
                            val isSocialLogin = response.getJSONObject(0).getString("IsSocialLogin")
                            val avatarStatus = response.getJSONObject(0).getString("avatarStatus")
                            val token = responseObj.getString("token")

                            mSessionManager.setData(SessionManager.AUTH_TOKEN, token)
                            mSessionManager.setData(SessionManager.USER_NAME, name)
                            mSessionManager.setData(SessionManager.USER_ID, id)
                            mSessionManager.setData(SessionManager.EMAIL, email)
                            mSessionManager.setData(SessionManager.IIS_SOCIAL_LOGIN, isSocialLogin)
                            mSessionManager.setData(SessionManager.AVATAR_STATUS, avatarStatus)

                            Animatoo.animateSlideLeft(this)

                            binding.edtEmail.setText("")
                            binding.edtPassword.setText("")

                            if (avatarStatus == "0") {
                                val intent = Intent(this, GenderActivity::class.java)
                                startActivity(intent)
                                Animatoo.animateSlideLeft(this)
                            } else if (avatarStatus == "1") {
                                SessionManager(this).setUserLogin(true)
                                val window =
                                    Intent(applicationContext, DashBoardActivity::class.java)
                                window.putExtra("email", email)
                                startActivity(window)
                            }

                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    fun checkValidation(): Boolean {
        if (AppUtils.isEditTextEmpty(edt_email)) {
            edt_email.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_email))
            return false
        } else if (!AppUtils.isValidEmail(edt_email)) {
            edt_email.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_invalid_email))
            return false
        } else if (AppUtils.isEditTextEmpty(edtPassword)) {
            edtPassword.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_password))
            return false
        } else if (AppUtils.getEditTextValue(edtPassword)!!.length < 3) {
            edtPassword.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_less_pass))
            return false
        }
        return true
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(result: Task<GoogleSignInAccount>) {
        try {
            val account: GoogleSignInAccount = result.getResult(ApiException::class.java)
            signUpAPICall(account)
        } catch (e: ApiException) {
            e.printStackTrace()
        }
    }

    override fun onPointerCaptureChanged(hasCapture: Boolean) {}

    override fun onBackPressed() {
        super.onBackPressed()
    }

    @SuppressLint("HardwareIds")
    private fun signUpAPICall(account: GoogleSignInAccount) {

        try {
            googleApiClient!!.clearDefaultAccountAndReconnect()

        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        val android_id: String = Settings.Secure.getString(
            applicationContext.contentResolver,
            Settings.Secure.ANDROID_ID
        )
        val os_version: String = android.os.Build.VERSION.RELEASE
        val device_type: String = android.os.Build.BRAND

        val requestData = JsonObject()
        requestData.addProperty("email", account.email)
        requestData.addProperty("DeviceToken", android_id)
        requestData.addProperty("DeviceType", "android")
        requestData.addProperty("OSVersion", os_version)
        requestData.addProperty("DeviceName", device_type)
        requestData.addProperty("IsSocialLogin", "1")
        requestData.addProperty("SocialID", account.id)
        requestData.addProperty("SocialType", "google")
        requestData.addProperty("name", "" + account.displayName)
        if (account.photoUrl != null) {
            requestData.addProperty("imgurl", "" + account.photoUrl.toString())
        } else {
            requestData.addProperty("imgurl", "")
        }

        ApiServiceProvider.getInstance(this)
            .sendPostData(Constants.UrlPath.LOGIN, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")

                        if (status) {

                            Toast.makeText(this, "Login Successfully", Toast.LENGTH_SHORT).show()

                            val response = responseObj.getJSONArray("data")
                            val name = response.getJSONObject(0).getString("name")
                            val id = response.getJSONObject(0).getString("id")
                            val email = response.getJSONObject(0).getString("email")
                            val isSocialLogin = response.getJSONObject(0).getString("IsSocialLogin")
                            val avatarStatus = response.getJSONObject(0).getString("avatarStatus")
                            val token = responseObj.getString("token")

                            mSessionManager.setData(SessionManager.AUTH_TOKEN, token)
                            mSessionManager.setData(SessionManager.USER_NAME, name)
                            mSessionManager.setData(SessionManager.USER_ID, id)
                            mSessionManager.setData(SessionManager.EMAIL, email)
                            mSessionManager.setData(SessionManager.IIS_SOCIAL_LOGIN, isSocialLogin)
                            mSessionManager.setData(SessionManager.AVATAR_STATUS, avatarStatus)

                            Animatoo.animateSlideLeft(this)

                            binding.edtEmail.setText("")
                            binding.edtPassword.setText("")

                            if (avatarStatus == "0") {
                                val intent = Intent(this, GenderActivity::class.java)
                                startActivity(intent)
                                Animatoo.animateSlideLeft(this)
                            } else if (avatarStatus == "1") {
                                SessionManager(this).setUserLogin(true)
                                val window =
                                    Intent(applicationContext, DashBoardActivity::class.java)
                                window.putExtra("email", email)
                                startActivity(window)
                            }
                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }
}