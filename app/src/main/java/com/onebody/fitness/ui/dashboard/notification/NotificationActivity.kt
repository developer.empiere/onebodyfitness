package com.onebody.fitness.ui.dashboard.notification

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.onebody.fitness.databinding.ActivityNotificationBinding
import kotlinx.android.synthetic.main.toolbar.*

class NotificationActivity : AppCompatActivity() {
    lateinit var binding: ActivityNotificationBinding
    private var notificationAdapter: NotificationAdapter? = null
    private var layoutManager: LinearLayoutManager? = null
    private var notificationModel: ArrayList<NotificationModel>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityNotificationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setDailyAdapter()

        ivPrevious.setOnClickListener(View.OnClickListener {
            Animatoo.animateSlideRight(this)
            onBackPressed()
        })

        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        })
    }

    private fun setDailyAdapter() {
        notificationAdapter = NotificationAdapter(this, notificationModel)
        layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.recyclerNotification.layoutManager = layoutManager
        binding.recyclerNotification.adapter = notificationAdapter
    }

    override fun onBackPressed() {
        finish()
    }
}