package com.onebody.fitness.ui.dashboard.product

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityProductDetailBinding
import com.onebody.fitness.databinding.ActivityProductListBinding

class ProductDetailActivity : AppCompatActivity() {
    lateinit var binding: ActivityProductDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityProductDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}