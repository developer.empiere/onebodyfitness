package com.onebody.fitness.ui.dashboard.movement

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class MoveMentModel {

    @SerializedName("data")
    var data: MutableList<Data>? = null

    class Data : Serializable {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("video")
        var video: String? = null

        @SerializedName("title")
        var title: String? = null
    }

}