package com.onebody.fitness.ui.dashboard.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.dailychallange.DailyChallengeModel

class HomeAdapter(
    private val context: Context?,
    val dailyChallengeModel: ArrayList<DailyChallengeModel>?,
    val is_fromFragment: Boolean
) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_home_station, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.card_view.setOnClickListener(View.OnClickListener {

           /* val intent = Intent(context, DailyChallengeList::class.java)
            context!!.startActivity(intent)
            Animatoo.animateSlideLeft(context)*/

        })
    }

    override fun getItemCount(): Int {


        if (is_fromFragment) {
            if (dailyChallengeModel?.size == null) {
                return 0
            }
            if (dailyChallengeModel.size == 1) {
                return 1
            }
            if (dailyChallengeModel.size == 2) {
                return 2
            }
            if (dailyChallengeModel.size == 3) {
                return 3
            }
            if (dailyChallengeModel.size >= 4) {
                return 3
            }
        }
        return dailyChallengeModel?.size!!

    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val card_view: CardView = itemView.findViewById(R.id.card_view)
    }


}