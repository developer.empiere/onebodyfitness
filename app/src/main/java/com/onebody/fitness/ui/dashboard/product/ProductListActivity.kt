package com.onebody.fitness.ui.dashboard.product

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.onebody.fitness.databinding.ActivityProductListBinding

class ProductListActivity : AppCompatActivity() {
    lateinit var binding: ActivityProductListBinding

    private var productListAdapter: ProductListAdapter? = null
    private var layoutManager: LinearLayoutManager? = null
    private var dailyModel: ArrayList<ProductModel>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityProductListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setDailyAdapter()
    }

    private fun setDailyAdapter() {
        productListAdapter = ProductListAdapter(this@ProductListActivity, dailyModel)
        //layoutManager = LinearLayoutManager(this@ProductListActivity, RecyclerView.VERTICAL, false)
        layoutManager =
            GridLayoutManager(this@ProductListActivity, 2)/*,RecyclerView.VERTICAL, false)*/

        binding.rvProductList.layoutManager = layoutManager
        binding.rvProductList.adapter = productListAdapter
    }
}