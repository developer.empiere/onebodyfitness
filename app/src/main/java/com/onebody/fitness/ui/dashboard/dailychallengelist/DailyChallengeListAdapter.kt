package com.onebody.fitness.ui.dashboard.dailychallengelist

import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.media.MediaPlayer
import android.media.ThumbnailUtils
import android.net.Uri
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.VideoView
import androidx.recyclerview.widget.RecyclerView
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.dailychallangedetail.DailyChallengeDetailActivity
import java.io.IOException

class DailyChallengeListAdapter(
    private val context: Context?,
    var dailyChallengeListModel: MutableList<DailyChallengeListModel.Data>?
) : RecyclerView.Adapter<DailyChallengeListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_station1, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val cm = dailyChallengeListModel?.get(position)

        holder.tvStationNamee.text = cm?.staion_name
        var stationName = cm?.staion_name

        holder.tvTime.setText(cm?.time_minutes)
        var time = cm?.time_minutes
        var comment = cm?.comment

        var video = cm?.video
        holder.videoView.setVideoURI(Uri.parse(video))
        holder.videoView.seekTo(1000)

        holder.videoView.setOnCompletionListener(MediaPlayer.OnCompletionListener {
            holder.iconVideoPlay.setVisibility(View.VISIBLE)
            holder.iconVideoPause.setVisibility(View.GONE)
        })
        try {
            val thumb = ThumbnailUtils.createVideoThumbnail(
                video.toString(),
                MediaStore.Images.Thumbnails.MINI_KIND
            )

            val bitmapDrawable = BitmapDrawable(thumb)
            holder.videoView.setBackgroundDrawable(bitmapDrawable)

        } catch (e: IOException) {
            e.printStackTrace()
        }

        holder.iconVideoPlay.setOnClickListener {
            holder.videoView.start()
            holder.iconVideoPlay.setVisibility(View.GONE)
            holder.iconVideoPause.setVisibility(View.VISIBLE)

        }

        holder.iconVideoPause.setOnClickListener {
            holder.videoView.pause()
            holder.iconVideoPlay.setVisibility(View.VISIBLE)
            holder.iconVideoPause.setVisibility(View.GONE)
        }

        holder.linear.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, DailyChallengeDetailActivity::class.java)
            intent.putExtra("id", cm!!.dailychallenge_id)
            intent.putExtra("videopath", video)
            intent.putExtra("StationName", stationName)
            intent.putExtra("Time", time)
            intent.putExtra("Comment", comment)
            context?.startActivity(intent)
        })
    }

    /* @Throws(Throwable::class)
     fun retriveVideoFrameFromVideo(videoPath: String?): Bitmap? {
         var bitmap: Bitmap? = null
         var mediaMetadataRetriever: MediaMetadataRetriever? = null
         try {
             mediaMetadataRetriever = MediaMetadataRetriever()
             if (Build.VERSION.SDK_INT >= 14) mediaMetadataRetriever.setDataSource(
                 videoPath,
                 HashMap()
             ) else mediaMetadataRetriever.setDataSource(videoPath)
             //   mediaMetadataRetriever.setDataSource(videoPath);
             bitmap = mediaMetadataRetriever.frameAtTime
         } catch (e: Exception) {
             e.printStackTrace()
             throw Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.message)
         } finally {
             mediaMetadataRetriever?.release()
         }
         return bitmap
     }*/


    override fun getItemCount(): Int {
        return dailyChallengeListModel?.size!!
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {

        val tvStationNamee: TextView = itemView.findViewById(R.id.tvStationNamee)
        val tvTime: TextView = itemView.findViewById(R.id.tvTime)
        val videoView: VideoView = itemView.findViewById(R.id.videoView)
        val iconVideoPlay: ImageView = itemView.findViewById(R.id.iconVideoPlay)
        val iconVideoPause: ImageView = itemView.findViewById(R.id.iconVideoPause)
        val linear: LinearLayout = itemView.findViewById(R.id.linear)

    }
}


