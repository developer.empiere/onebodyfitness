package com.onebody.fitness.ui.dashboard.contactlist

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.onebody.fitness.databinding.ActivityContactListBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.activity_contact_list.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject
import java.lang.reflect.Type

class ContactListActivity : AppCompatActivity() {

    lateinit var binding: ActivityContactListBinding
    private var contactListAdapter: ContactListAdapter? = null
    private var layoutManager: LinearLayoutManager? = null
    private var contactListModel: MutableList<ContactListModel.Data>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityContactListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        contactListModel = ArrayList()

        val refreshListener = SwipeRefreshLayout.OnRefreshListener {
            binding.swipeRefreshLayout.isRefreshing = true
            getChatContactList()
        }

        binding.swipeRefreshLayout.setOnRefreshListener(refreshListener)

        getChatContactList()

        addTextListener()
        ivPrevious.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        })
    }

    private fun addTextListener() {

        binding.EdtSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(newText: CharSequence, start: Int, before: Int, count: Int) {
                if (contactListModel!!.size > 0 && contactListModel != null && !contactListModel!!.isEmpty()) {
                    if (newText.trim { it <= ' ' }.isNotEmpty()) {
                        filter(newText.toString())
                    } else {
                        getChatContactList()
                    }
                    filter(newText.toString())
                }
            }
        })
    }

    private fun filter(text: String) {
        if (text!!.isNotEmpty()) {
            val temp: MutableList<ContactListModel.Data> = ArrayList()
            for (d in contactListModel!!) {
                /* if (d.name!!.contains(text.lowercase()) || d.name!!.contains(text.uppercase())) {
                     temp.add(d)
                 }*/
                if (d.name!!.toLowerCase().contains(text.toLowerCase())) {
                    temp.add(d)
                }
            }
            contactListAdapter!!.updateList(temp as ArrayList<ContactListModel.Data>)
        }
    }

    private fun getChatContactList() {
        var jsonObject = JsonObject()

        ApiServiceProvider.getInstance(this).sendPostData(
            Constants.UrlPath.CHAT_CONTACT_LIST, jsonObject, true,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")
                    val message = responsObj.getString("message")
                    binding.swipeRefreshLayout.isRefreshing = false

                    if (status) {


                        val data = responsObj.getJSONArray("data")
                        val gson = Gson()
                        val listType: Type =
                            object : TypeToken<MutableList<ContactListModel.Data?>?>() {}.type
                        contactListModel = gson.fromJson(data.toString(), listType)
                        setAdapter()
                    }
                    else{

                        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun setAdapter() {
        contactListAdapter = ContactListAdapter(this, contactListModel)
        layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rvContactList.layoutManager = layoutManager
        rvContactList.adapter = contactListAdapter
    }

    override fun onBackPressed() {
        finish()
    }
}