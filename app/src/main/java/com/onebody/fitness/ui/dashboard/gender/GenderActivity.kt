package com.onebody.fitness.ui.dashboard.gender

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.onebody.fitness.databinding.ActivityGenderBinding
import com.onebody.fitness.retrofit.SessionManager
import com.onebody.fitness.ui.dashboard.bodytype.BodyTypeFemaleeActivity
import com.onebody.fitness.ui.dashboard.bodytype.BodyTypeMaleActivity
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.toolbar.*

class GenderActivity : AppCompatActivity() {

    lateinit var binding: ActivityGenderBinding

    //private var isMale: Boolean = true
    private lateinit var mSessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGenderBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mSessionManager = SessionManager(applicationContext)


        ivPrevious.setOnClickListener(View.OnClickListener {
            Animatoo.animateSlideRight(this)
            onBackPressed()
        })

        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)

        })

        binding.btnNext.setOnClickListener(View.OnClickListener {
            if (mSessionManager.getBooleanData(SessionManager.IS_MALE)) {
                Animatoo.animateSlideLeft(this)
                val intent = Intent(this, BodyTypeMaleActivity::class.java)
                startActivity(intent)
            }

            if (!mSessionManager.getBooleanData(SessionManager.IS_MALE)) {
                Animatoo.animateSlideLeft(this)
                val intent = Intent(this, BodyTypeFemaleeActivity::class.java)
                startActivity(intent)
            }

        })

        binding.ivMaleDeselect.setOnClickListener(View.OnClickListener {
            mSessionManager.setData(SessionManager.IS_MALE, true)
            binding.ivMaleselect.visibility = View.VISIBLE
            binding.ivMaleDeselect.visibility = View.GONE
            binding.ivFemaleDeselect.visibility = View.VISIBLE
            binding.ivFemaleSelect.visibility = View.GONE
            binding.txtAvatarBody.visibility = View.VISIBLE


        })
        binding.ivFemaleDeselect.setOnClickListener(View.OnClickListener {
            mSessionManager.setData(SessionManager.IS_MALE, false)
            binding.ivMaleselect.visibility = View.GONE
            binding.ivMaleDeselect.visibility = View.VISIBLE
            binding.ivFemaleDeselect.visibility = View.GONE
            binding.ivFemaleSelect.visibility = View.VISIBLE
            binding.txtAvatarBody.visibility = View.VISIBLE
        })
    }

    override fun onBackPressed() {
        finish()
    }
}