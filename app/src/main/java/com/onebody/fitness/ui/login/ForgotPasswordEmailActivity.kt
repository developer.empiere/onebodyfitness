package com.onebody.fitness.ui.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.gson.JsonObject
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityForgotPasswordEmailBinding
import com.onebody.fitness.retrofit.*
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import com.onebody.fitness.ui.dashboard.otp.OtpActivity
import kotlinx.android.synthetic.main.activity_forgot_password_email.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject


class ForgotPasswordEmailActivity : AppCompatActivity() {
    lateinit var binding: ActivityForgotPasswordEmailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityForgotPasswordEmailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        ivPrevious.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideRight(this)
            onBackPressed()
        })
        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
        })
        btnChangePassword.setOnClickListener(View.OnClickListener {
            if (checkValidationEmail())
                sendOTPtoMail()
        })
    }

    private fun checkValidationEmail(): Boolean {
        if (AppUtils.isEditTextEmpty(edtEmailAdd)) {
            edtEmailAdd.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_email))
            return false
        }
        return true
    }

    private fun sendOTPtoMail() {
        val requestData = JsonObject()
        requestData.addProperty("email", edtEmailAdd.text.toString())
        ApiServiceProvider.getInstance(this).sendPostData(
            Constants.UrlPath.FORGOT_PASSWORD, requestData, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    val message = responseObj.getString("message")
                    if (status) {
                        Animatoo.animateSlideLeft(this)
                        val window = Intent(applicationContext, OtpActivity::class.java)
                        window.putExtra("email", edtEmailAdd.text.toString())
                        startActivity(window)
                    } else {
                        Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    override fun onBackPressed() {
        finish()
    }
}