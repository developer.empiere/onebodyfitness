package com.onebody.fitness.ui.dashboard.leaderboard

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LeaderBoardModel {
    @SerializedName("data")
    var data: MutableList<Data>? = null

    class Data : Serializable {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("username")
        var username: String? = null

        @SerializedName("profile")
        var profile: String? = null

        @SerializedName("completed_percentage")
        var completed_percentage: String? = null

    }
}