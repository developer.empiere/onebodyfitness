package com.onebody.fitness.ui.dashboard.shoes

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.onebody.fitness.R
import com.onebody.fitness.ui.other.MyClothConstants
import com.onebody.fitness.ui.other.MyConstants

class ShoesAdapter(private val context: Context?, val shoesModel: ArrayList<ShoesModel>) :
    RecyclerView.Adapter<ShoesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_shoes_color, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 6
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        var rlShoes: RelativeLayout = itemView.findViewById(R.id.rlShoes)
        var ivShoesColor: ImageView = itemView.findViewById(R.id.ivShoesColor)
    }
}