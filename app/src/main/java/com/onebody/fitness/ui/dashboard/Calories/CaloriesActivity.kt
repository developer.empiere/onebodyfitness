package com.onebody.fitness.ui.dashboard.Calories

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.onebody.fitness.databinding.ActivityCalariesBinding
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.toolbar.*


class CaloriesActivity : AppCompatActivity() {
    lateinit var binding: ActivityCalariesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCalariesBinding.inflate(layoutInflater)
        setContentView(binding.root)


        ivPrevious.setOnClickListener(View.OnClickListener {
            onBackPressed()
            Animatoo.animateSlideRight(this)

        })
        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
            onBackPressed()
        })
        /*val anim = ValueAnimator.ofFloat(1f, 1.5f)
        anim.duration = 600
        anim.addUpdateListener { animation ->
            binding.ivHeart.setScaleX(animation.animatedValue as Float)
            binding.ivHeart.setScaleY(animation.animatedValue as Float)
        }*/

        /*anim.repeatCount = 1
        anim.repeatMode = ValueAnimator.REVERSE
        anim.start()*/
        /*binding.animationView.setAnimation("wave_animation.json")
        binding.animationView.repeatMode
        binding.animationView.playAnimation()

        binding.animationView.setAnimation("heart_animation.json")
        binding.animationView.repeatMode
        binding.animationView.playAnimation()
*/
        /* val animFadein = AnimationUtils.loadAnimation(
             applicationContext, com.onebody.fitness.R.anim.slide_in_left
         )
 */
// start the animation

// start the animation
        //binding.imgWaves.startAnimation(animFadein)

        Glide
            .with(this)
            .asGif()
            .load(com.onebody.fitness.R.drawable.ic_wave)
            .centerCrop()
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
            .placeholder(com.onebody.fitness.R.drawable.ic_wave)
            .listener(object : RequestListener<GifDrawable?> {
                override fun onLoadFailed(
                    @Nullable e: GlideException?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<GifDrawable?>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: GifDrawable?,
                    model: Any?,
                    target: Target<GifDrawable?>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    resource!!.setLoopCount(50)
                    return false
                }
            })
            .into(binding.imgWaves)
    }

    override fun onBackPressed() {
        finish()
    }
}