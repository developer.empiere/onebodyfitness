package com.onebody.fitness.ui.dashboard.greatyou

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.PopupWindow
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.onebody.fitness.databinding.ActivityGreatYouBinding
import kotlinx.android.synthetic.main.popup_layout.view.*
import com.onebody.fitness.R
import com.onebody.fitness.retrofit.SessionManager
import com.onebody.fitness.ui.dashboard.DashBoardActivity
import com.onebody.fitness.ui.dashboard.dailychallange.DailyChallengeActivity
import com.onebody.fitness.ui.dashboard.dailychallangedetail.DailyChallengeDetailActivity
import com.onebody.fitness.ui.dashboard.leaderboard.LeaderBoardActivity
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.toolbar.*


class GreatYouActivity : AppCompatActivity() {
    lateinit var binding: ActivityGreatYouBinding
    var time: String = ""
    var weight: String = ""
    var reps: String = ""
    var GoalId: String = ""
    var GoalComment: String = ""
    private lateinit var mSessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityGreatYouBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mSessionManager = SessionManager(applicationContext)

        time = mSessionManager.getData(SessionManager.TIME)!!
        weight = mSessionManager.getData(SessionManager.WEIGHT)!!
        reps = mSessionManager.getData(SessionManager.REPS)!!
        //GoalId = intent.getStringExtra("Goal").toString()
        GoalComment = intent.getStringExtra("goal_comment").toString()

        binding.tvTime.text = time
        binding.tvWeight.text = weight
        binding.tvReps.text = reps
        binding.tvGetGoal.text = GoalComment

        binding.btnViewMyPosition.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, LeaderBoardActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        })
        ivPrevious.setOnClickListener(View.OnClickListener {
            finish()
        })
        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        })
    }

    @SuppressLint("ClickableViewAccessibility")
    fun showPopupWindow(view: View) {
        val inflater =
            view.context.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView: View = inflater.inflate(R.layout.popup_layout, null)
        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = false

        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(view, Gravity.CENTER, width, height)

        popupView.btnSubmit.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, LeaderBoardActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        })

        dimBehind(popupWindow)

        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }
    }

    fun dimBehind(searchPopup: PopupWindow) {
        val container: View = if (searchPopup.background == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                searchPopup.contentView.parent as View
            } else {
                searchPopup.contentView
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                searchPopup.contentView.parent.parent as View
            } else {
                searchPopup.contentView.parent as View
            }
        }
        val context = searchPopup.contentView.context
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val p = container.layoutParams as WindowManager.LayoutParams
        p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
        p.dimAmount = 0.5f
        wm.updateViewLayout(container, p)
    }

    override fun onBackPressed() {
        finish()
    }
}