package com.onebody.fitness.ui.dashboard.clothtypemale

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityClothTypeBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.avatar.AvatarViewActivity
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject

class ClothTypeMaleActivity : AppCompatActivity() {

    lateinit var binding: ActivityClothTypeBinding
    private var clothTypeMaleAdapter: ClothTypeMaleAdapter? = null
    private var clothTypeMaleModel: MutableList<ClothTypeMaleModel.Data>? = null

    var selectedSkinColor: Int = -1
    var selectedHairColor: Int = -1
    var selectedFacialHair: Int = -1
    var selectedHairStyle: Int = -1

    var clothesID: String = "0"
    var bodyType: String = "0"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityClothTypeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getClothList()
        setClothButtonClickListner()

        try {
            bodyType = intent.getStringExtra("BodyType").toString()
            selectedSkinColor = intent.getIntExtra("SkinID", -1)
            selectedHairColor = intent.getIntExtra("HairColor", -1)
            selectedFacialHair = intent.getIntExtra("FacialHair", -1)
            selectedHairStyle = intent.getIntExtra("HairStyle", -1)

        } catch (e: Exception) {
            e.printStackTrace()
        }

        ivPrevious.setOnClickListener(View.OnClickListener {
//            val intent = Intent(this, MaleFaceTypeActivity::class.java)
//            startActivity(intent)

            onBackPressed()

        })

        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
        })

        binding.imgNext.setOnClickListener {
           /* val returnIntent = Intent()
            returnIntent.putExtra("result", clothesID)
            setResult(RESULT_OK, returnIntent)
            finish()*/
            Animatoo.animateSlideLeft(this)
            val intent = Intent(this, AvatarViewActivity::class.java)
            intent.putExtra("BodyType", bodyType)
            intent.putExtra("SkinID", selectedSkinColor)
            intent.putExtra("HairColor", selectedHairColor)
            intent.putExtra("FacialHair", selectedFacialHair)
            intent.putExtra("HairStyle", selectedHairStyle)
            intent.putExtra("ClothID", clothesID)
            startActivity(intent)
        }
        //binding.ctTank.performClick()
    }

    private fun getClothList() {
        val map = HashMap<String, String>()
        map.put("BodyType", bodyType)
        map.put("SkinID", selectedSkinColor.toString())
        map.put("HairColorID", selectedHairColor.toString())
        map.put("FacialHairID", selectedFacialHair.toString())
        map.put("HairStyleID", selectedHairStyle.toString())

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_CLOTHING_SHORT_MALE, map, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    if (status) {
                        val data = responseObj.getJSONArray("data")
                        val gson = Gson()
                        val listType =
                            object : TypeToken<MutableList<ClothTypeMaleModel.Data?>?>() {}.type
                        clothTypeMaleModel = gson.fromJson<Any>(
                            data.toString(),
                            listType
                        ) as MutableList<ClothTypeMaleModel.Data>?
                        setClothAdapter()
                        clothesID = clothTypeMaleModel!![0].id.toString()

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun setClothButtonClickListner() {
        binding.ctTank.setOnClickListener(View.OnClickListener {

            binding.ctTank.setBackgroundColor(R.drawable.d_red_17)
            binding.ctTank.setBackgroundDrawable(resources.getDrawable(R.drawable.d_red_17))
            binding.ctTank.setTextColor(resources.getColor(R.color.white))

            binding.ctTshirt.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctTshirt.setTextColor(resources.getColor(R.color.black))

            clothesID = clothTypeMaleModel!![0].id.toString()
            binding.rvClothType.scrollToPosition(0)

        })

        binding.ctTshirt.setOnClickListener(View.OnClickListener {

            binding.ctTshirt.setBackgroundDrawable(resources.getDrawable(R.drawable.d_red_17))
            binding.ctTshirt.setTextColor(resources.getColor(R.color.white))

            binding.ctTank.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctTank.setTextColor(resources.getColor(R.color.black))

            clothesID = clothTypeMaleModel!![1].id.toString()

            binding.rvClothType.scrollToPosition(1)
        })
    }

    private fun setClothAdapter() {
        val myLinearLayoutManager = object : LinearLayoutManager(this) {
            override fun canScrollVertically(): Boolean {
                return false
            }

            override fun canScrollHorizontally(): Boolean {
                return false
            }
        }
        clothTypeMaleAdapter = ClothTypeMaleAdapter(this, clothTypeMaleModel!!)
        binding.rvClothType.layoutManager = myLinearLayoutManager
        binding.rvClothType.adapter = clothTypeMaleAdapter
        //selectedHairStyle = 1
    }

    override fun onBackPressed() {
        finish()
    }
}