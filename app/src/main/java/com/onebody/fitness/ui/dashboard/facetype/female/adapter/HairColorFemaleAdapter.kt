package com.onebody.fitness.ui.dashboard.facetype.female.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.facetype.female.FemaleFaceTypeActivity
import com.onebody.fitness.ui.dashboard.facetype.female.model.HairColorFemaleModel
import kotlinx.android.synthetic.main.row_hair_color_female.view.*

class HairColorFemaleAdapter(
    private val context: Context?,
    private val hairColorFemaleModel: MutableList<HairColorFemaleModel.Data>
) :
    RecyclerView.Adapter<HairColorFemaleAdapter.ViewHolder>() {
    var selectedPosition = (context as FemaleFaceTypeActivity).selectedHairColor - 1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_hair_color_female, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = hairColorFemaleModel[position]
        context?.let {
            Glide.with(it)
                .load(hairColorFemaleModel.get(position).hair)
                .error(R.drawable.ic_launcher_background)
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(holder.imgHairColorFamale)
        }

        if (selectedPosition < 0) {
            selectedPosition = 0
        }

        if (selectedPosition == position) {
            holder.cv_Hair_Color_Female.background =
                context!!.resources.getDrawable(R.drawable.red_outside_border)
        } else {
            holder.cv_Hair_Color_Female.background =
                context!!.resources.getDrawable(R.drawable.white_outside_border)
        }

        holder.cv_Hair_Color_Female.setOnClickListener {
            selectedPosition = position
            onItemClickListener?.let {
                it(model.id)
            }
            notifyDataSetChanged()
        }
    }

    private var onItemClickListener: ((Int) -> Unit)? = null

    fun setOnClickListener(listener: (Int) -> Unit) {
        onItemClickListener = listener
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imgHairColorFamale: ImageView = itemView.imgHairColorFamale
        val cv_Hair_Color_Female = itemView.cv_Hair_Color_Female

    }

    override fun getItemCount(): Int {
        return hairColorFemaleModel.size
    }
}