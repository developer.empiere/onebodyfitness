package com.onebody.fitness.ui.dashboard.myprofile

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.bumptech.glide.Glide
import com.onebody.fitness.databinding.ActivityMyProfileBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.retrofit.SessionManager
import com.onebody.fitness.ui.dashboard.ImagePickerActivity
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_my_profile.*
import kotlinx.android.synthetic.main.toolbar.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.File

class MyProfileActivity : AppCompatActivity() {

    lateinit var binding: ActivityMyProfileBinding
    private lateinit var adapter: ArrayAdapter<*>
    private val genderModel: ArrayList<ProfileModel> = ArrayList()
    var uri: Uri? = null
    var profileFile: File? = null
    var profilePath = ""
    var MultiPart: MultipartBody.Part? = null

    private lateinit var mSessionManager: SessionManager

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMyProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mSessionManager = SessionManager(applicationContext)

        getUserProfile()
//        setSpinnerAdapter()

        ivPrevious.setOnClickListener(View.OnClickListener {
            Animatoo.animateSlideRight(this)
            onBackPressed()
        })

        ivNotification.setOnClickListener(View.OnClickListener {
            Animatoo.animateSlideLeft(this)
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
        })

        imgSelectProfile.setOnClickListener(View.OnClickListener {
            showImagePickerChooser()
        })

        btnSave.setOnClickListener {
            updateProfile()
        }
    }

    private fun getUserProfile() {
        val map = HashMap<String, String>()

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.SET_USER_PROFILE, map, true,
            RetrofitListener { response ->

                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")

                    if (status) {
                        val data = responseObj.getJSONArray("data")
                        val jsonObjct = data.getJSONObject(0)

                        binding.txtName.text = jsonObjct.getString("name")
                        binding.edtAge.setText(jsonObjct.getString("age"))
                        binding.edtWeight.setText(jsonObjct.getString("weight"))
                        binding.edtHeight.setText(jsonObjct.getString("height"))
                        binding.txtRole.text = jsonObjct.getString("role")
                        binding.txtLevel.text = jsonObjct.getString("level")
                        binding.tvGender.text = jsonObjct.getString("gender")

                        /* val gender = jsonObjct.getString("gender")
                         if (gender.equals("male")) {
                             binding.spinnerGender.setSelection(0)
                         } else if (gender.equals("female")) {
                             binding.spinnerGender.setSelection(1)
                         }*/

                        Glide.with(this)
                            .load(jsonObjct.getString("prof_pic"))
                            .into(binding.imgProfile)

                        Glide.with(this)
                            .load(jsonObjct.getString("image"))
                            .into(binding.imgAvatar)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun updateProfile() {

        val map = HashMap<String, String>()
        map.put("Name", binding.txtName.text.toString())
        map.put("Gender", "" + binding.tvGender.text.toString())
        map.put("Age", binding.edtAge.text.toString())
        map.put("Height", binding.edtHeight.text.toString())
        map.put("Weight", binding.edtWeight.text.toString())

        if (profileFile != null) {
            val requestType: RequestBody =
                RequestBody.create("image/*".toMediaTypeOrNull(), profileFile!!)
            MultiPart =
                MultipartBody.Part.createFormData(
                    "ProfilePic",
                    profileFile!!.name,
                    requestType
                )
        }
        ApiServiceProvider.getInstance(this).updateProfile(
            Constants.UrlPath.UPDATE_USER_PROFILE, map, MultiPart, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    val message = responseObj.getString("message")

                    if (status) {

                        val data = responseObj.getJSONArray("data")
                        val jsonObjct = data.getJSONObject(0)

                        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()

                        binding.txtName.text = jsonObjct.getString("name")
                        binding.edtAge.setText(jsonObjct.getString("age"))
                        binding.edtWeight.setText(jsonObjct.getString("weight"))
                        binding.edtHeight.setText(jsonObjct.getString("height"))
                        binding.txtRole.text = jsonObjct.getString("role")
                        binding.txtLevel.text = jsonObjct.getString("level")
                        binding.tvGender.text = jsonObjct.getString("gender")

                        Glide.with(this)
                            .load(jsonObjct.getString("prof_pic"))
                            .into(this.imgProfile)

                        Glide.with(this)
                            .load(jsonObjct.getString("avtarurl"))
                            .into(this.imgAvatar)

                        mSessionManager.setData(
                            SessionManager.AUTH_TOKEN,
                            responseObj.getString("token")
                        )
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun showImagePickerChooser() {
        ImagePickerActivity.showImagePickerOptions(
            this,
            object : ImagePickerActivity.PickerOptionListener {
                override fun onTakeCameraSelected() {
                    launchCameraIntent()
                }

                override fun onChooseGallerySelected() {
                    launchGalleryIntent()
                }
            })
    }

    private fun launchCameraIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_IMAGE_CAPTURE
        )
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)
        startActivityForResult(intent, Constants.REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_GALLERY_IMAGE
        )
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, Constants.REQUEST_IMAGE)
    }
/*
    @RequiresApi(Build.VERSION_CODES.O)
    private fun setSpinnerAdapter() {
        adapter = ArrayAdapter(
            this,
            R.layout.spinnerlayout,
            resources.getStringArray(R.array.gender_array)
        )
        spinnerGender.prompt = selectText

        spinnerGender.adapter = adapter

        spinnerGender.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
                ) {
                    val selectedText = resources.getStringArray(R.array.gender_array)[position]

                    if (position == 0) {
                        imgAvatar.setImageResource(R.drawable.ic_male)

                    } else {
                        imgAvatar.setImageResource(R.drawable.ic_female)
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {

                }
            }
        setGenderTypeData(resources.getStringArray(R.array.gender_array)[0])
    }
    private fun setGenderTypeData(genderType: String?) {
        genderModel.clear()

    }*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.REQUEST_IMAGE) {
            if (resultCode == RESULT_OK) {
                uri = data!!.getParcelableExtra<Uri>("path")
                profilePath = uri!!.getPath().toString()
                profileFile = File(profilePath)
                imgProfile.setImageURI(uri)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Constants.CAMERA -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(this)
            } else {
                Toast.makeText(
                    this,
                    "Storage and Camera permission is required for " + "upload profile image.",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    override fun onBackPressed() {
        finish()
    }
}