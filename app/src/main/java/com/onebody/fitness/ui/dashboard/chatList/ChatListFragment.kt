package com.onebody.fitness.ui.dashboard.chatList

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.onebody.fitness.R
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.LoadingDialog
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.contactlist.ContactListActivity
import kotlinx.android.synthetic.main.fragment_chat_list.view.*
import org.json.JSONObject


class ChatListFragment : Fragment() {

    lateinit var mRootView: View

    private var chatListAdapter: ChatListAdapter? = null
    private var layoutManager: LinearLayoutManager? = null
    private var chatListModel: MutableList<ChatListModel.Data>? = null
    val ha = Handler()
    private var mDialog: LoadingDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
        mDialog = LoadingDialog(context)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mRootView = inflater.inflate(R.layout.fragment_chat_list, container, false)

        chatListModel = mutableListOf()

        val refreshListener = SwipeRefreshLayout.OnRefreshListener {
            mRootView.swipeRefreshLayout.isRefreshing = true
            getChatList()
        }

        mRootView.swipeRefreshLayout.setOnRefreshListener(refreshListener);

        getChatList()

        ha.postDelayed(object : Runnable {
            override fun run() {
                getChatList()
                ha.postDelayed(this, 10000)
            }
        }, 10000)

        mRootView.fabAdd.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, ContactListActivity::class.java)
            startActivity(intent)
        })

        addTextListener()

        return mRootView
    }

    private fun getChatList() {
        var jsonObject = JsonObject()

        ApiServiceProvider.getInstance(context).sendPostData(
            Constants.UrlPath.MY_CHAT_LIST, jsonObject, false,
            RetrofitListener { response ->
                try {
                    mRootView.swipeRefreshLayout.isRefreshing = false
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")
                    val message = responsObj.getString("message")
                    if (status) {
                        val data = responsObj.getJSONArray("data")
                        chatListModel = Gson().fromJson<Any>(
                            data.toString(),
                            object : TypeToken<MutableList<ChatListModel.Data>?>() {}.type
                        ) as MutableList<ChatListModel.Data>
                        object : TypeToken<MutableList<ChatListModel.Data?>?>() {}.type
                        setAdapter()
                    } else {
                        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show()

                    }
                } catch (e: Exception) {

                    e.printStackTrace()
                }
            })
    }

    fun setAdapter() {
        chatListAdapter = ChatListAdapter(context, chatListModel)
        layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        mRootView.rvChatList.layoutManager = layoutManager
        mRootView.rvChatList.adapter = chatListAdapter
    }

    private fun addTextListener() {

        mRootView.edtSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(newText: CharSequence, start: Int, before: Int, count: Int) {
                if (chatListModel!!.size > 0 && chatListModel != null && !chatListModel!!.isEmpty()) {
                    if (newText.trim { it <= ' ' }.isNotEmpty()) {
                        filter(newText.toString())
                    } else {
                        getChatList()
                    }
                    filter(newText.toString())
                }
            }
        })
    }

    fun filter(text: String?) {
        if (text!!.isNotEmpty()) {
            val temp: MutableList<ChatListModel.Data> = ArrayList()
            for (d in chatListModel!!) {

                if (d.name!!.toLowerCase().contains(text.toLowerCase())) {
                    temp.add(d)
                }
            }
            chatListAdapter!!.updateList(temp as ArrayList<ChatListModel.Data>)
        }
    }

    override fun onResume() {
        super.onResume()
        chatListModel?.clear()
        getChatList()
    }

    override fun onDestroy() {
        super.onDestroy()
        ha.removeCallbacksAndMessages(null)
    }
}
