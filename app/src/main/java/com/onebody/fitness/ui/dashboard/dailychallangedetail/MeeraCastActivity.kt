package com.onebody.fitness.ui.dashboard.dailychallangedetail

import android.appwidget.AppWidgetManager
import android.content.ActivityNotFoundException
import android.content.ComponentName
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityMeeraCastBinding

class MeeraCastActivity : AppCompatActivity() {

    lateinit var binding: ActivityMeeraCastBinding

    val ACTION_WIFI_DISPLAY_SETTINGS = "android.settings.WIFI_DISPLAY_SETTINGS"
    val ACTION_CAST_SETTINGS = "android.settings.CAST_SETTINGS"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMeeraCastBinding.inflate(layoutInflater)
        setContentView(binding.root)

        updateWidget()

        val wifiActionIntent = Intent(ACTION_WIFI_DISPLAY_SETTINGS)
        val castActionIntent = Intent(ACTION_CAST_SETTINGS)

        var systemResolveInfo: ResolveInfo? = getSystemResolveInfo(wifiActionIntent)
        if (systemResolveInfo != null) {
            try {
                val systemWifiIntent = Intent()
                systemWifiIntent.setClassName(
                    systemResolveInfo.activityInfo.applicationInfo.packageName,
                    systemResolveInfo.activityInfo.name
                )
                startSettingsActivity(systemWifiIntent)
                finish()
                return
            } catch (ignored: ActivityNotFoundException) {
            }
        }

        systemResolveInfo = getSystemResolveInfo(castActionIntent)
        if (systemResolveInfo != null) {
            try {
                val systemCastIntent = Intent()
                systemCastIntent.setClassName(
                    systemResolveInfo.activityInfo.applicationInfo.packageName,
                    systemResolveInfo.activityInfo.name
                )
                startSettingsActivity(systemCastIntent)
                finish()
                return
            } catch (ignored: ActivityNotFoundException) {
            }
        }

        // Show an error and fail
        showErrorToast()
        finish()
    }

    private fun updateWidget() {
        val intent = Intent(this, MiracastWidgetProvider::class.java)
        intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
        val ids = AppWidgetManager.getInstance(application)
            .getAppWidgetIds(ComponentName(application, MiracastWidgetProvider::class.java))
        if (ids != null) {
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
            sendBroadcast(intent)
        }
    }

    private fun showErrorToast() {
        val errorMessage = resources.getString(R.string.error_toast)
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
    }

    private fun getSystemResolveInfo(intent: Intent): ResolveInfo? {
        val pm = packageManager
        val list = pm.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
        for (info in list) {
            try {
                val activityInfo = pm.getApplicationInfo(
                    info.activityInfo.packageName,
                    0
                )
                if (activityInfo.flags and ApplicationInfo.FLAG_SYSTEM != 0) {
                    return info
                }
            } catch (ignored: PackageManager.NameNotFoundException) {
            }
        }
        return null
    }

    private fun startSettingsActivity(intent: Intent) {
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        try {
            startActivity(intent)
        } catch (e: SecurityException) {
            // We don't have permission to launch this activity, alert the user and return.
            showErrorToast()
        }
    }

    /*override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the
        // action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }*/
    companion object {
        const val EXTRA_WIDGET_LAUNCH = "widget_launch"
    }
}