package com.onebody.fitness.ui.dashboard.movement

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.onebody.fitness.R
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import kotlinx.android.synthetic.main.fragment_move_ment.*
import kotlinx.android.synthetic.main.fragment_move_ment.view.*
import org.json.JSONObject

class MoveMentFragment : Fragment() {

    lateinit var mRootView: View
    private var layoutManager: LinearLayoutManager? = null
    private var movementAdapter: MovementAdapter? = null
    private var moveMentModel: MutableList<MoveMentModel.Data>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mRootView = inflater.inflate(R.layout.fragment_move_ment, container, false)

        getMovementList()

        mRootView.edtSearchMove.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(newText: CharSequence, start: Int, before: Int, count: Int) {
                try {
                    if (moveMentModel!!.size > 0 && moveMentModel != null && !moveMentModel!!.isEmpty()) {
                        if (newText.trim { it <= ' ' }.isNotEmpty()) {
                            filter(newText.toString())
                        } else {
                            searchData(newText.toString())
                        }
                        filter(newText.toString())
                    }
                } catch (e : NullPointerException){
                    e.printStackTrace()
                }
            }
        })
        return mRootView
    }

    fun filter(text: String?) {
        if (text!!.isNotEmpty()) {
            val temp: MutableList<MoveMentModel.Data> = ArrayList()
            for (d in moveMentModel!!) {

                if (d.title!!.toLowerCase().contains(text.toLowerCase())) {
                    temp.add(d)
                }
            }
            movementAdapter!!.updateList(temp as ArrayList<MoveMentModel.Data>)
        }
    }

    private fun searchData(search: String) {
        val map = HashMap<String, String>()
        map.put("search", search)

        ApiServiceProvider.getInstance(context).sendHashmapPostDataSearch(
            Constants.UrlPath.MOVEMENT_SEARCH, map, false,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")

                    if (status) {
                        val data = responsObj.getJSONArray("data")
                        moveMentModel = Gson().fromJson<Any>(
                            data.toString(),
                            object : TypeToken<MutableList<MoveMentModel.Data>?>() {}.type
                        ) as MutableList<MoveMentModel.Data>
                        object : TypeToken<MutableList<MoveMentModel.Data?>?>() {}.type
                        setAdapterMovement()
                    } else {

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun getMovementList() {
        var jsonObject = JsonObject()

        ApiServiceProvider.getInstance(context).sendPostData(
            Constants.UrlPath.MOVEMENT, jsonObject, true,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")

                    if (status) {
                        val data = responsObj.getJSONArray("data")

                        moveMentModel = Gson().fromJson<Any>(
                            data.toString(),
                            object : TypeToken<MutableList<MoveMentModel.Data>?>() {}.type
                        ) as MutableList<MoveMentModel.Data>
                        object : TypeToken<MutableList<MoveMentModel.Data?>?>() {}.type
                        setAdapterMovement()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun setAdapterMovement() {
        movementAdapter = MovementAdapter(context, moveMentModel)
        layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        recyclrMovement.layoutManager = layoutManager
        recyclrMovement.adapter = movementAdapter
    }
}

