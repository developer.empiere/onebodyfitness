package com.onebody.fitness.ui.dashboard.facetype.male.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.facetype.male.MaleFaceTypeActivity
import com.onebody.fitness.ui.dashboard.facetype.male.model.SkinColorMaleModel
import kotlinx.android.synthetic.main.row_skin_male.view.*

class SkinColorMaleAdapter(
    private val context: Context?,
    private val skinColorMaleModel: MutableList<SkinColorMaleModel.Data>
) :
    RecyclerView.Adapter<SkinColorMaleAdapter.ViewHolder>() {

    var selectedPosition = (context as MaleFaceTypeActivity).selectedSkinColor - 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_skin_male, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val model = skinColorMaleModel[position]
        context?.let {
            Glide.with(it)
                .load(skinColorMaleModel.get(position).skin)
                .error(R.drawable.ic_launcher_background)
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(holder.imgMaleSkin)
        }

        if (selectedPosition < 0) {
            selectedPosition = 0
        }
        if (selectedPosition == position) {
            holder.skinSelectionCv.background =
                context!!.resources.getDrawable(R.drawable.red_outside_border)
        } else {
            holder.skinSelectionCv.background =
                context!!.resources.getDrawable(R.drawable.white_outside_border)
        }

        holder.skinSelectionCv.setOnClickListener {
            selectedPosition = position
            onItemClickListener?.let {
                it(model.id)
            }
            notifyDataSetChanged()
        }
    }

    private var onItemClickListener: ((Int) -> Unit)? = null

    fun setOnClickListener(listener: (Int) -> Unit) {
        onItemClickListener = listener
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imgMaleSkin: ImageView = itemView.imgMaleSkin
        val skinSelectionCv = itemView.cv_skin_selection
    }

    override fun getItemCount(): Int {
        return skinColorMaleModel.size
    }
}