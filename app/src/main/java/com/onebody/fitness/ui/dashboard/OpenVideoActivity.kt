package com.onebody.fitness.ui.dashboard

import android.content.pm.ActivityInfo
import android.graphics.drawable.BitmapDrawable
import android.media.MediaPlayer
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityOpenVideoBinding
import java.io.IOException


class OpenVideoActivity : AppCompatActivity() {
    lateinit var binding: ActivityOpenVideoBinding
    var video: String = ""
    var isFullScreen = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOpenVideoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        video = intent.getStringExtra("video").toString()
        binding.videoView.setVideoURI(Uri.parse(video))
        binding.videoView.seekTo(1000)
        binding.videoView.start()
        binding.iconVideoPlay.setVisibility(View.GONE)
        binding.iconVideoPause.setVisibility(View.VISIBLE)
        binding.videoView.setOnCompletionListener(MediaPlayer.OnCompletionListener {
            binding.iconVideoPlay.setVisibility(View.VISIBLE)
            binding.iconVideoPause.setVisibility(View.GONE)
        })
        if (!isFullScreen) {

            binding.btnFullscreen.setImageDrawable(
                this.let { it1 ->
                    ContextCompat.getDrawable(
                        it1,
                        R.drawable.ic_fullscreen_exit
                    )
                }
            )
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE)
        }
        isFullScreen = !isFullScreen
        try {
            val thumb = ThumbnailUtils.createVideoThumbnail(
                video,
                MediaStore.Images.Thumbnails.MINI_KIND
            )
            val bitmapDrawable = BitmapDrawable(thumb)
            binding.videoView.setBackgroundDrawable(bitmapDrawable)

        } catch (e: IOException) {
            e.printStackTrace()
        }
        binding.iconVideoPlay.setOnClickListener {
            binding.videoView.start()
            binding.iconVideoPlay.setVisibility(View.GONE)
            binding.iconVideoPause.setVisibility(View.VISIBLE)
        }
        binding.iconVideoPause.setOnClickListener {
            binding.videoView.pause()
            binding.iconVideoPlay.setVisibility(View.VISIBLE)
            binding.iconVideoPause.setVisibility(View.GONE)
        }
        binding.btnFullscreen.setOnClickListener {
            finish()
        }
    }
}