package com.onebody.fitness.ui.dashboard.dailychallangedetail

import android.annotation.SuppressLint
import android.content.Intent
import android.media.MediaPlayer.OnCompletionListener
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.gson.JsonObject
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityDailyChallengeDetailBinding
import com.onebody.fitness.retrofit.*
import com.onebody.fitness.ui.dashboard.homemore.HomeMoreStationActivity
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.activity_daily_challenge_detail.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject
import java.util.*


class DailyChallengeDetailActivity : AppCompatActivity() {

    lateinit var binding: ActivityDailyChallengeDetailBinding

    var dailychallenge_id: String = "0"
    var videoPosition: String = "0"
    var stationName: String = "0"
    var mTime: String = "0"
    var comment: String = "0"
    var textview: String = ""
    private var seconds = 0
    private var running = false
    private var wasRunning = false
    var playtime = 10000
    val handler = Handler()
    private lateinit var mSessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDailyChallengeDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        dailychallenge_id = intent.getStringExtra("id").toString()
        videoPosition = intent.getStringExtra("videopath").toString()
        stationName = intent.getStringExtra("StationName").toString()
        mTime = intent.getStringExtra("Time").toString()
        comment = intent.getStringExtra("Comment").toString()

        binding.tvTimer.text = "00:00:00"
        binding.tvStationName.text = stationName
        binding.tvTime.text = mTime
        binding.tvComment.text = comment

        mSessionManager = SessionManager(applicationContext)
        binding.videoView.setOnPreparedListener { it.isLooping = true }

        if (savedInstanceState != null) {

            seconds = savedInstanceState
                .getInt("seconds")
            running = savedInstanceState
                .getBoolean("running")
            wasRunning = savedInstanceState
                .getBoolean("wasRunning")
        }
        //runTimer()
        binding.tvTime.text = "00:00:00"

        ivPrevious.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        })

        binding.btnSubmit.setOnClickListener(View.OnClickListener {
            showPopupSubmitWorkOut(this.btnSubmit)
            //submitWorkOut()
        })
        binding.imgviewCast.setOnClickListener {
            val intent = Intent(this, MeeraCastActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        }
        binding.videoView.setVideoURI(Uri.parse(videoPosition))

        if (binding.videoView.currentPosition == 0) {
            seconds = 0
            running = true

        } else if (binding.videoView.duration > playtime) {
            running = true
        } else {
            running = true
        }
        binding.btnStart.setOnClickListener(View.OnClickListener {
            binding.btnStart.isEnabled = false
            binding.btnStop.isEnabled = true
            binding.videoView.start()
            handler.removeCallbacksAndMessages(null)
            runTimer()
            binding.iconVideoPlay.visibility = View.GONE
            binding.iconVideoPause.visibility = View.VISIBLE

            if (binding.videoView.currentPosition == 0) {
                seconds = 0
                running = true

            } else if (binding.videoView.duration > playtime) {
                running = true
            } else {
                running = true
            }
        })
        binding.btnStop.setOnClickListener(View.OnClickListener {
            binding.btnStart.isEnabled = true
            binding.btnStop.isEnabled = false
            binding.videoView.pause()
            binding.iconVideoPlay.visibility = View.VISIBLE
            binding.iconVideoPause.visibility = View.GONE
            running = false
        })

        binding.iconVideoPlay.setOnClickListener {
            binding.videoView.start()
            binding.btnStart.isEnabled = false
            binding.btnStop.isEnabled = true
            handler.removeCallbacksAndMessages(null);
            runTimer()
            binding.iconVideoPlay.visibility = View.GONE
            binding.iconVideoPause.visibility = View.VISIBLE
            if (binding.videoView.currentPosition == 0) {
                seconds = 0
                running = true
            } else if (binding.videoView.duration > playtime) {
                running = true
            } else {
                running = true
            }
        }
        binding.iconVideoPause.setOnClickListener {
            binding.videoView.pause()
            binding.btnStart.isEnabled = true
            binding.btnStop.isEnabled = false
            binding.iconVideoPlay.visibility = View.VISIBLE
            binding.iconVideoPause.visibility = View.GONE
            running = false
        }

        binding.videoView.setOnCompletionListener(OnCompletionListener {
            binding.iconVideoPlay.visibility = View.VISIBLE
            binding.iconVideoPause.visibility = View.GONE
            running = false
        })
    }

    override fun onSaveInstanceState(
        savedInstanceState: Bundle
    ) {
        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState
            .putInt("seconds", seconds)
        savedInstanceState
            .putBoolean("running", running)
        savedInstanceState
            .putBoolean("wasRunning", wasRunning)
    }

    override fun onPause() {
        super.onPause()
        wasRunning = running
        running = false
    }

    override fun onResume() {
        super.onResume()
        if (wasRunning) {
            running = true
        }
    }

    private fun runTimer() {

        handler.post(object : Runnable {

            override fun run() {

                val hours = seconds / 3600
                val minutes = seconds % 3600 / 60
                val secs = seconds % 60

                val time = String.format(
                    Locale.getDefault(),
                    "%02d:%02d:%02d", hours,
                    minutes, secs
                )

                binding.tvTimer.text = time
                mTime = time

                if (running) {
                    seconds++
                }
                handler.postDelayed(this, 1000)
            }
        })
    }

    @SuppressLint("ClickableViewAccessibility")
    fun showPopupSubmitWorkOut(view: View) {
        val inflater = view.context.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView: View = inflater.inflate(R.layout.alert_submit_work_out, null)
        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = false

        val btn_submit: Button = popupView.findViewById(R.id.btn_submit) as Button

        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(view, Gravity.CENTER, width, height)

        AppUtils.dimBehind(popupWindow)

        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }

        btn_submit.setOnClickListener(View.OnClickListener {
            mSessionManager.setData(SessionManager.TIME, mTime)
            println("Set Time:-$mTime")
            popupWindow.dismiss()
            if (checkValidation()) {
                submitWorkOut()
            }
        })
    }

    private fun checkValidation(): Boolean {
        if (AppUtils.isEditTextEmpty(binding.edtWeight)) {
            binding.edtWeight.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_weight_in_kg))
            return false
        } else if (AppUtils.isEditTextEmpty(binding.edtReps)) {
            binding.edtReps.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_reps))
            return false
        }
        return true
    }

    private fun submitWorkOut() {
        val requestData = JsonObject()
        requestData.addProperty("dailychallenge_id", dailychallenge_id)
        requestData.addProperty("weight_used", edtWeight.text.toString())
        requestData.addProperty("reps_completed", edtReps.text.toString())
        requestData.addProperty("workoutmInutes", mTime)
        mSessionManager.setData(SessionManager.WEIGHT, edtWeight.text.toString())
        mSessionManager.setData(SessionManager.REPS, edtReps.text.toString())

        ApiServiceProvider.getInstance(this)
            .sendPostData(
                Constants.UrlPath.SUBMITWORKOUT, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")

                        if (status) {
                            Toast.makeText(
                                this,
                                "Workout Submitted Successfully",
                                Toast.LENGTH_SHORT
                            ).show()
                            val intent = Intent(this, HomeMoreStationActivity::class.java)
                            startActivity(intent)
                            Animatoo.animateSlideLeft(this)

                            finish()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    override fun onBackPressed() {
        finish()
    }
}