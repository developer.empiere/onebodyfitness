package com.onebody.fitness.ui.dashboard.facetype.female.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.facetype.female.FemaleBodyTypeActivity
import com.onebody.fitness.ui.dashboard.facetype.female.FemaleFaceTypeActivity
import kotlinx.android.synthetic.main.female_body_bottom.view.*
import kotlinx.android.synthetic.main.fragment_pear.view.*
import kotlinx.android.synthetic.main.toolbar.view.*

class InvertedTriangleFragment : Fragment() {

    lateinit var mRootView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mRootView = inflater.inflate(R.layout.fragment_inverted_triangle, container, false)

        mRootView.femaleBody.txtBodyAngle.setText("Inverted Triangle")
        mRootView.femaleBody.ivPreviousBody.setOnClickListener(View.OnClickListener {
            (activity as FemaleBodyTypeActivity).moveToPrevious()
        })

        mRootView.femaleBody.ivNextBody.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, FemaleFaceTypeActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(context)
        })

        mRootView.btnSubmit.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, FemaleFaceTypeActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(context)
        })
        mRootView.toolbar.ivPrevious.setOnClickListener(View.OnClickListener {
            (activity as FemaleBodyTypeActivity).moveToPrevious()
            Animatoo.animateSlideRight(context)
        })
        return mRootView
    }


    fun newInstance(param1: String, param2: String) =
        InvertedTriangleFragment().apply {
            arguments = Bundle().apply {

            }
        }
}
