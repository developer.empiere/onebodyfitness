package com.onebody.fitness.ui.dashboard.clothtypefemale

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityClothTypeFemaleBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject

class ClothTypeFemaleActivity : AppCompatActivity() {

    lateinit var binding: ActivityClothTypeFemaleBinding
    private var clothTypeFemaleAdapter: ClothTypeFemaleAdapter? = null
    private var clothTypeFemaleModel: MutableList<ClothTypeFemaleModel.Data>? = null

    var selectedSkinColor: Int = -1
    var selectedHairColor: Int = -1
    var selectedHairStyle: Int = -1

    var clothesID: String = "0"
    var bodyType: String = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityClothTypeFemaleBinding.inflate(layoutInflater)
        setContentView(binding.root)

      //  bodyType = intent.getStringExtra("BodyType").toString()

        getClothList()
        setClothButtonClickListner()

        try {

            bodyType = intent.getStringExtra("BodyType").toString()
            selectedSkinColor = intent.getIntExtra("SkinID", -1)
            selectedHairColor = intent.getIntExtra("HairColor", -1)
            selectedHairStyle = intent.getIntExtra("HairStyleID", -1)

        } catch (e: Exception) {
            e.printStackTrace()
        }

        ivPrevious.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
        })

        binding.imgNext.setOnClickListener {
            val returnIntent = Intent()
            returnIntent.putExtra("result", clothesID)
            setResult(RESULT_OK, returnIntent)
            finish()
        }
    }

    private fun getClothList() {

        val map = HashMap<String, String>()
        map.put("BodyType", bodyType)
        map.put("SkinID", selectedSkinColor.toString())
        map.put("HairColorID", selectedHairColor.toString())
        map.put("HairStyleID", selectedHairStyle.toString())

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_CLOTHING_SHORT_FEMALE, map, true,
            RetrofitListener {  response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    if (status) {
                        clothesID = "1"
                        val data = responseObj.getJSONArray("data")
                        val gson = Gson()
                        val listType =
                            object : TypeToken<MutableList<ClothTypeFemaleModel.Data?>?>() {}.type
                        clothTypeFemaleModel = gson.fromJson<Any>(
                            data.toString(),
                            listType
                        ) as MutableList<ClothTypeFemaleModel.Data>?
                        setAdapterClothFemale()
                        clothesID = clothTypeFemaleModel!![0].id.toString()

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun setClothButtonClickListner() {
        binding.ctLaggies.setOnClickListener(View.OnClickListener {

            binding.ctLaggies.setBackgroundColor(R.drawable.d_red_17)
            binding.ctLaggies.setBackgroundDrawable(resources.getDrawable(R.drawable.d_red_17))
            binding.ctLaggies.setTextColor(resources.getColor(R.color.white))

            binding.ctTank.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctTank.setTextColor(resources.getColor(R.color.black))

            binding.ctSorts.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctSorts.setTextColor(resources.getColor(R.color.black))

            binding.ctTshirt.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctTshirt.setTextColor(resources.getColor(R.color.black))

            binding.ctSweats.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctSweats.setTextColor(resources.getColor(R.color.black))

            clothesID = clothTypeFemaleModel!![0].id.toString()
            binding.rvClothType.scrollToPosition(0)


        })
        binding.ctTank.setOnClickListener(View.OnClickListener {

            binding.ctTank.setBackgroundColor(R.drawable.d_red_17)
            binding.ctTank.setBackgroundDrawable(resources.getDrawable(R.drawable.d_red_17))
            binding.ctTank.setTextColor(resources.getColor(R.color.white))

            binding.ctLaggies.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctLaggies.setTextColor(resources.getColor(R.color.black))

            binding.ctSorts.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctSorts.setTextColor(resources.getColor(R.color.black))

            binding.ctTshirt.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctTshirt.setTextColor(resources.getColor(R.color.black))

            binding.ctSweats.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctSweats.setTextColor(resources.getColor(R.color.black))

            clothesID = clothTypeFemaleModel!![1].id.toString()
            binding.rvClothType.scrollToPosition(1)

        })

        binding.ctSorts.setOnClickListener(View.OnClickListener {

            binding.ctSorts.setBackgroundColor(R.drawable.d_red_17)
            binding.ctSorts.setBackgroundDrawable(resources.getDrawable(R.drawable.d_red_17))
            binding.ctSorts.setTextColor(resources.getColor(R.color.white))

            binding.ctLaggies.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctLaggies.setTextColor(resources.getColor(R.color.black))

            binding.ctTank.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctTank.setTextColor(resources.getColor(R.color.black))

            binding.ctTshirt.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctTshirt.setTextColor(resources.getColor(R.color.black))

            binding.ctSweats.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctSweats.setTextColor(resources.getColor(R.color.black))


            clothesID = clothTypeFemaleModel!![2].id.toString()
            binding.rvClothType.scrollToPosition(2)

        })
        binding.ctTshirt.setOnClickListener(View.OnClickListener {

            binding.ctTshirt.setBackgroundColor(R.drawable.d_red_17)
            binding.ctTshirt.setBackgroundDrawable(resources.getDrawable(R.drawable.d_red_17))
            binding.ctTshirt.setTextColor(resources.getColor(R.color.white))

            binding.ctLaggies.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctLaggies.setTextColor(resources.getColor(R.color.black))

            binding.ctTank.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctTank.setTextColor(resources.getColor(R.color.black))

            binding.ctSorts.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctSorts.setTextColor(resources.getColor(R.color.black))

            binding.ctSweats.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctSweats.setTextColor(resources.getColor(R.color.black))


            clothesID = clothTypeFemaleModel!![3].id.toString()
            binding.rvClothType.scrollToPosition(3)

        })
        binding.ctSweats.setOnClickListener(View.OnClickListener {

            binding.ctSweats.setBackgroundColor(R.drawable.d_red_17)
            binding.ctSweats.setBackgroundDrawable(resources.getDrawable(R.drawable.d_red_17))
            binding.ctSweats.setTextColor(resources.getColor(R.color.white))

            binding.ctLaggies.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctLaggies.setTextColor(resources.getColor(R.color.black))

            binding.ctTank.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctTank.setTextColor(resources.getColor(R.color.black))

            binding.ctSorts.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctSorts.setTextColor(resources.getColor(R.color.black))

            binding.ctTshirt.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.ctTshirt.setTextColor(resources.getColor(R.color.black))

            clothesID = clothTypeFemaleModel!![4].id.toString()
            binding.rvClothType.scrollToPosition(4)

        })
    }

    private fun setAdapterClothFemale() {
        val myLinearLayoutManager = object : LinearLayoutManager(this) {
            override fun canScrollVertically(): Boolean {
                return false
            }
            override fun canScrollHorizontally(): Boolean {
                return false
            }
        }
        clothTypeFemaleAdapter = ClothTypeFemaleAdapter(this, clothTypeFemaleModel!!)
        binding.rvClothType.layoutManager = myLinearLayoutManager
        binding.rvClothType.adapter = clothTypeFemaleAdapter
//        selectedFacialHair = 1

    }

    override fun onBackPressed() {
        finish()
    }
}