package com.onebody.fitness.ui.dashboard.contactus

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.gson.JsonObject
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityContactusBinding
import com.onebody.fitness.retrofit.*
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.activity_contactus.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject

class ContactusActivity : AppCompatActivity() {
    lateinit var binding: ActivityContactusBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityContactusBinding.inflate(layoutInflater)
        setContentView(binding.root)

        ivPrevious.setOnClickListener(View.OnClickListener {
            Animatoo.animateSlideRight(this)
            onBackPressed()

        })

        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        })

        binding.btnSubmit.setOnClickListener {
            if (checkValidation()) {
                contactus()
            }
        }
    }

    private fun checkValidation(): Boolean {
        if (AppUtils.isEditTextEmpty(edtName)) {
            edtName.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_name))
            return false
        }else if (AppUtils.isEditTextEmpty(edtEmail)) {
            edtEmail.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_email))
            return false
        }
        else if (!AppUtils.isValidEmail(edtEmail)) {
            edtEmail.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_invalid_email))
            return false
        } else if (AppUtils.isEditTextEmpty(edtMsg)) {
            edtMsg.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_contact_msg))
            return false
        }
        return true

    }

    private fun contactus() {

         val requestData = JsonObject()
         requestData.addProperty("name", edtName.text.toString())
         requestData.addProperty("email", edtEmail.text.toString())
         requestData.addProperty("msg", edtMsg.text.toString())

         ApiServiceProvider.getInstance(this)
             .sendPostData(
                 Constants.UrlPath.CONTACTUS, requestData, true,
                 RetrofitListener { response ->
                     try {
                         val responseObj = JSONObject(response.body().toString())
                         val status = responseObj.getBoolean("status")
                         val message = responseObj.getString("message")

                         if (status) {

                             Toast.makeText(this,"Message Sent Successfully",Toast.LENGTH_SHORT).show()

                             binding.edtName.setText("")
                             binding.edtEmail.setText("")
                             binding.edtMsg.setText("")

                         }
                         else {
                             Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                         }

                     } catch (e: Exception) {
                         e.printStackTrace()
                     }
                 })
     }

    override fun onBackPressed() {
        finish()
    }

}