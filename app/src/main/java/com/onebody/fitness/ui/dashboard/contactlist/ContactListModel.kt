package com.onebody.fitness.ui.dashboard.contactlist

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ContactListModel : Serializable {

    @SerializedName("data")
    var data: MutableList<Data>? = null

    class Data : Serializable {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("email")
        var email: String? = null

        @SerializedName("gender")
        var gender: String? = null

        @SerializedName("prof_pic")
        var prof_pic: String? = null
    }

}