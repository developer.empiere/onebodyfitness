package com.onebody.fitness.ui.dashboard.facetype.female.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.facetype.female.FemaleBodyTypeActivity
import kotlinx.android.synthetic.main.female_body_bottom.view.*
import kotlinx.android.synthetic.main.fragment_pear.view.*
import kotlinx.android.synthetic.main.toolbar.view.*


class RectangleFragment : Fragment() {

    lateinit var mRootView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mRootView = inflater.inflate(R.layout.fragment_rectangle, container, false)
        mRootView.femaleBody.txtBodyAngle.text = "Rectangle"

        mRootView.femaleBody.ivPreviousBody.setOnClickListener(View.OnClickListener {
            (activity as FemaleBodyTypeActivity).moveToPrevious()
        })

        mRootView.femaleBody.ivNextBody.setOnClickListener(View.OnClickListener {
            (activity as FemaleBodyTypeActivity).moveToNext()
            Animatoo.animateSlideLeft(context)
        })

        mRootView.toolbar.ivPrevious.setOnClickListener(View.OnClickListener {
            (activity as FemaleBodyTypeActivity).moveToPrevious()
            Animatoo.animateSlideLeft(context)
        })
        return mRootView
    }

    companion object {
        fun newInstance(param1: String, param2: String) =
            RectangleFragment().apply {
                arguments = Bundle().apply {}
            }
    }
}