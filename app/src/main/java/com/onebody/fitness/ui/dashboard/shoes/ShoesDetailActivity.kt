package com.onebody.fitness.ui.dashboard.shoes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.onebody.fitness.databinding.ActivityShoesDetailBinding
import com.onebody.fitness.ui.other.MyClothConstants
import com.onebody.fitness.ui.other.MyConstants
import kotlinx.android.synthetic.main.toolbar.*

class ShoesDetailActivity : AppCompatActivity() {

    lateinit var binding: ActivityShoesDetailBinding
    private var shoesAdapter: ShoesAdapter? = null
    private var layoutManager: GridLayoutManager? = null
    var shoesModel: ArrayList<ShoesModel>? = ArrayList()
    var clothProperty: MyClothConstants.Companion.CLOTH_STYLE_PROPERTY =
        MyClothConstants.Companion.CLOTH_STYLE_PROPERTY.TANK
    var faceProperty: MyConstants.Companion.FACE_PROPERTY =
        MyConstants.Companion.FACE_PROPERTY.SHOESCOLOR

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityShoesDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        ivPrevious.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        setClothAdapter()
    }

    private fun setClothAdapter() {
        shoesAdapter = shoesModel?.let { ShoesAdapter(this, it) }
        layoutManager = GridLayoutManager(this, 2, RecyclerView.VERTICAL, false)
        binding.rvShoes.layoutManager = layoutManager
        binding.rvShoes.adapter = shoesAdapter
    }
}