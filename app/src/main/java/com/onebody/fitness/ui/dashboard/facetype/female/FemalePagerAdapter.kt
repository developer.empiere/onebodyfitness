package com.onebody.fitness.ui.dashboard.facetype.female

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.onebody.fitness.ui.dashboard.facetype.female.femalestyle.FEyeColorFragment
import com.onebody.fitness.ui.dashboard.facetype.female.femalestyle.FHairColorFragment
import com.onebody.fitness.ui.dashboard.facetype.female.femalestyle.FHairStyleFragment
import com.onebody.fitness.ui.dashboard.facetype.female.femalestyle.FskinColorFragment
import com.onebody.fitness.ui.dashboard.facetype.female.fragment.*

class FemalePagerAdapter(fm: FragmentManager, val totalTabs: Int) : FragmentPagerAdapter(fm) {

    override fun getCount(): Int {
        return totalTabs
    }

    override fun getItem(position: Int): Fragment {

        return when (position) {
            0 -> {
                PearFragment()
            }
            1 -> {
                ItriangleFragment()
            }
            2 -> {
                AppleFragment()
            }
            3 -> {
                RectangleFragment()
            }
            4 -> {
                InvertedTriangleFragment()
            }
           /* 5 -> {
                HourglassFragment()
            }*/
            5 -> {
                FskinColorFragment()
            }
            6 -> {
                FHairStyleFragment()
            }
            7 -> {
                FHairColorFragment()
            }
            8-> {
                FEyeColorFragment()
            }

            else -> PearFragment()
        }
    }
}