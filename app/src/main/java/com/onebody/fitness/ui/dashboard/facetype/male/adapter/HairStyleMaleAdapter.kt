package com.onebody.fitness.ui.dashboard.facetype.male.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.facetype.male.MaleFaceTypeActivity
import com.onebody.fitness.ui.dashboard.facetype.male.model.HairStyleMaleModel
import kotlinx.android.synthetic.main.row_hair_style_male.view.*


class HairStyleMaleAdapter(private val context: Context?,
                           private val hairStyleMaleModel: MutableList<HairStyleMaleModel.Data>
) :
    RecyclerView.Adapter<HairStyleMaleAdapter.ViewHolder>() {
    var selectedPosition = (context as MaleFaceTypeActivity).selectedHairStyle - 1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_hair_style_male, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = hairStyleMaleModel[position]
        context?.let {
            Glide.with(it)
                .load(hairStyleMaleModel.get(position).style)
                .error(R.drawable.ic_launcher_background)
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(holder.imgHairStyleMale)
        }

        if (selectedPosition < 0) {
            selectedPosition = 0
        }

        if (selectedPosition == position) {
            holder.cv_hair_style.background =
                context!!.resources.getDrawable(R.drawable.red_outside_border)
        } else {
            holder.cv_hair_style.background =
                context!!.resources.getDrawable(R.drawable.white_outside_border)
        }
        holder.cv_hair_style.setOnClickListener {
            selectedPosition = position
            onItemClickListener?.let {
                it(model.id)
            }
            notifyDataSetChanged()
        }
    }
    private var onItemClickListener: ((Int) -> Unit)? = null

    fun setOnClickListener(listener: (Int) -> Unit) {
        onItemClickListener = listener
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imgHairStyleMale: ImageView = itemView.imgHairStyleMale
        val cv_hair_style = itemView.cv_hair_style

    }

    override fun getItemCount(): Int {
        return hairStyleMaleModel.size
    }
}