package com.onebody.fitness.ui.dashboard.facetype.male.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.facetype.male.MaleFaceTypeActivity
import com.onebody.fitness.ui.dashboard.facetype.male.model.FacialHairMaleModel
import kotlinx.android.synthetic.main.row_facial_hair_male.view.*

class FacialHairMaleAdapter(
    private val context: Context?,
    private val facialHairMaleModel: MutableList<FacialHairMaleModel.Data>
) :
    RecyclerView.Adapter<FacialHairMaleAdapter.ViewHolder>() {

    var selectedPosition = (context as MaleFaceTypeActivity).selectedFacialHair - 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_facial_hair_male, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = facialHairMaleModel[position]
        context?.let {
            Glide.with(it)
                .load(facialHairMaleModel.get(position).style)
                .error(R.drawable.ic_launcher_background)
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(holder.imgFacialHairMale)
        }

        if (selectedPosition < 0) {
            selectedPosition = 0
        }

        if (selectedPosition == position) {
            holder.cv_facial_hair_color.background =
                context!!.resources.getDrawable(R.drawable.red_outside_border)
        } else {
            holder.cv_facial_hair_color.background =
                context!!.resources.getDrawable(R.drawable.white_outside_border)
        }
        holder.cv_facial_hair_color.setOnClickListener {
            selectedPosition = position
            onItemClickListener?.let {
                it(model.id)
            }
            notifyDataSetChanged()
        }
    }

    private var onItemClickListener: ((Int) -> Unit)? = null

    fun setOnClickListener(listener: (Int) -> Unit) {
        onItemClickListener = listener
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imgFacialHairMale: ImageView = itemView.imgFacialHairMale
        val cv_facial_hair_color = itemView.cv_facial_hair_color

    }

    override fun getItemCount(): Int {
        return facialHairMaleModel.size
    }
}