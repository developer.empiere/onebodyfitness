package com.onebody.fitness.ui.dashboard.dailychallengemore

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.onebody.fitness.databinding.ActivityDailyChallengeSeeMoreBinding
import com.onebody.fitness.databinding.ActivityWorkOutBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.dailychallange.ArchivedWorkOutAdapter
import com.onebody.fitness.ui.dashboard.dailychallange.ArchivedWorkOutModel
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject

class DailyChallengeMoreWorkoutActivity : AppCompatActivity() {

    lateinit var binding: ActivityWorkOutBinding

    private var archivedWorkOutAdapter: ArchivedWorkOutAdapter? = null
    private var layoutManager: LinearLayoutManager? = null
    private var archivedWorkOutModel: MutableList<ArchivedWorkOutModel.Data>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityWorkOutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getMyArchivedWorkouts()

        ivPrevious.setOnClickListener(View.OnClickListener {
            onBackPressed()
            Animatoo.animateSlideRight(this)
        })

        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)

        })
    }

    private fun getMyArchivedWorkouts() {
        var jsonObject = JsonObject()

        ApiServiceProvider.getInstance(this).sendPostData(
            Constants.UrlPath.MY_ARCHIVED_WORKOUTS, jsonObject, true,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")

                    if (status) {
                        val data = responsObj.getJSONArray("data")

                        archivedWorkOutModel = Gson().fromJson<Any>(
                            data.toString(),
                            object : TypeToken<MutableList<ArchivedWorkOutModel.Data>?>() {}.type
                        ) as MutableList<ArchivedWorkOutModel.Data>
                        object : TypeToken<MutableList<ArchivedWorkOutModel.Data?>?>() {}.type
                        setAdapter()
                    }
                } catch (e: Exception) {

                    e.printStackTrace()
                }
            })
    }

    private fun setAdapter() {
        archivedWorkOutAdapter = ArchivedWorkOutAdapter(this, archivedWorkOutModel,true)
        layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.rvWorkOut.layoutManager = layoutManager
        binding.rvWorkOut.adapter = archivedWorkOutAdapter
    }

    override fun onBackPressed() {
        finish()
    }
}