package com.onebody.fitness.ui.dashboard.howitworks

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.onebody.fitness.databinding.ActivityHowItWorksBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject

class HowItWorksActivity : AppCompatActivity() {
    lateinit var binding: ActivityHowItWorksBinding
    private var howItWorkAdapter: HowItWorkAdapter? = null
    private var layoutManager: LinearLayoutManager? = null
    private var howItWorkModel: MutableList<HowItWorkModel.Data>? = null
    var bodytype: String = "0"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHowItWorksBinding.inflate(layoutInflater)
        setContentView(binding.root)

        bodytype = intent.getStringExtra("BodyType").toString()

        howItWorks()

        ivPrevious.setOnClickListener(View.OnClickListener {
            Animatoo.animateSlideRight(this)
            onBackPressed()
        })

        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        })
    }

    private fun howItWorks() {
        val requestData = JsonObject()
//        val map = HashMap<String, String>()

        ApiServiceProvider.getInstance(this)
            .sendPostData(
                Constants.UrlPath.HOWITWORKS, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")

                        if (status) {
                            val data = responseObj.getJSONArray("data")

                            howItWorkModel = Gson().fromJson<Any>(
                                data.toString(),
                                object : TypeToken<MutableList<HowItWorkModel.Data>?>() {}.type
                            )
                                    as MutableList<HowItWorkModel.Data>
                            object : TypeToken<MutableList<HowItWorkModel.Data?>?>() {}.type

                            setAdapterHowItWorks()

                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun setAdapterHowItWorks() {
        howItWorkAdapter = HowItWorkAdapter(this, howItWorkModel)
        layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.recyclerHowItWork.layoutManager = layoutManager
        binding.recyclerHowItWork.adapter = howItWorkAdapter
    }

    override fun onBackPressed() {
        finish()
    }
}