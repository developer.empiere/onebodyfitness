package com.onebody.fitness.ui.dashboard.dailychallange

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.webkit.URLUtil
import android.widget.Button
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityDailyChallengeBinding
import com.onebody.fitness.retrofit.*
import com.onebody.fitness.ui.dashboard.DashBoardActivity
import com.onebody.fitness.ui.dashboard.dailychallengelist.DailyChallengeList
import com.onebody.fitness.ui.dashboard.homemore.HomeMoreStationActivity
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.fragment_daily_challange.view.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject

class DailyChallengeActivity : AppCompatActivity() {
    lateinit var binding: ActivityDailyChallengeBinding

    private var layoutManager: LinearLayoutManager? = null

    private var archivedWorkOutAdapter: ArchivedWorkOutAdapter? = null
    private var archivedWorkOutModel: MutableList<ArchivedWorkOutModel.Data>? = null

    private var dailyChallangeDialogAdapter: DailyChallangeDialogAdapter? = null
    private var dailyChallangeDialogModel: MutableList<DailyChallangeDialogModel.Data>? = null

    var selectedRadioPosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDailyChallengeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setArchivedWorkAdapter()
        getDailyChallenge()
        getMyArchivedWorkouts()

        ivPrevious.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, DashBoardActivity::class.java)
            startActivity(intent)
        })

        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
        })

        binding.tvStart.setOnClickListener(View.OnClickListener {
            selectedRadioPosition = 0
            getLevelList()
        })

        binding.tvSeeMore.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, HomeMoreStationActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        })
    }

    private fun getLevelList() {
        var jsonObject = JsonObject()

        ApiServiceProvider.getInstance(this).sendPostData(
            Constants.UrlPath.GET_LEVELS, jsonObject, true,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")
                    val message = responsObj.getString("message")

                    if (status) {

                        val data = responsObj.getJSONArray("data")

                        dailyChallangeDialogModel = Gson().fromJson<Any>(
                            data.toString(),
                            object :
                                TypeToken<MutableList<DailyChallangeDialogModel.Data>?>() {}.type
                        ) as MutableList<DailyChallangeDialogModel.Data>
                        object : TypeToken<MutableList<DailyChallangeDialogModel.Data?>?>() {}.type

                        showPopupStart(binding.tvStart)

                    } else {
                        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {

                    e.printStackTrace()
                }
            })
    }

    @SuppressLint("ClickableViewAccessibility", "InflateParams")
    fun showPopupStart(view: View) {
        val inflater =
            view.context.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView: View = inflater.inflate(R.layout.popup_daily_challange_dialog, null)
        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = false

        val rcvLevel: RecyclerView = popupView.findViewById(R.id.rcvLevel) as RecyclerView
        val btnok: Button = popupView.findViewById(R.id.btnok) as Button

        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(view, Gravity.CENTER, width, height)

        AppUtils.dimBehind(popupWindow)

        dailyChallangeDialogAdapter =
            DailyChallangeDialogAdapter(this, dailyChallangeDialogModel)
        layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rcvLevel.layoutManager = layoutManager
        rcvLevel.adapter = dailyChallangeDialogAdapter

        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }

        dailyChallangeDialogAdapter?.setOnClickListener {
            selectedRadioPosition = it
        }
        btnok.setOnClickListener {

            if (!checkValidationForAllRadio()) {
                Animatoo.animateSlideLeft(this)
                getSubmitLevel(popupWindow)
            }
        }
    }

    private fun getSubmitLevel(popupWindow: PopupWindow) {
        val map = HashMap<String, String>()
        map.put("levelid", selectedRadioPosition.toString())

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_LEVELS_SUBMIT, map, true,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")
                    val message = responsObj.getString("message")

                    if (status) {

                        popupWindow.dismiss()

                        val intent = Intent(this, DailyChallengeList::class.java)
                        startActivity(intent)
                        Animatoo.animateSlideLeft(this)
                        selectedRadioPosition = 0

                    } else {
                        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun checkValidationForAllRadio(): Boolean {
        if (selectedRadioPosition == 0) {
            PsDialogs().error(this, getString(R.string.alert_select_item))
            return true
        }
        return false
    }


    private fun getMyArchivedWorkouts() {

        var jsonObject = JsonObject()

        ApiServiceProvider.getInstance(this).sendPostData(
            Constants.UrlPath.MY_ARCHIVED_WORKOUTS, jsonObject, true,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")

                    if (status) {
                        val data = responsObj.getJSONArray("data")
                        archivedWorkOutModel = Gson().fromJson<Any>(
                            data.toString(),
                            object : TypeToken<MutableList<ArchivedWorkOutModel.Data>?>() {}.type
                        ) as MutableList<ArchivedWorkOutModel.Data>
                        object : TypeToken<MutableList<ArchivedWorkOutModel.Data?>?>() {}.type
                        setAdapterArchivedWork()
                    }
                } catch (e: Exception) {

                    e.printStackTrace()
                }
            })
    }

    private fun setAdapterArchivedWork() {
        archivedWorkOutAdapter = ArchivedWorkOutAdapter(this, archivedWorkOutModel, true)
        layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        layoutManager?.reverseLayout = true
        layoutManager?.stackFromEnd = true
        binding.rvWorkOut.layoutManager = layoutManager
        binding.rvWorkOut.adapter = archivedWorkOutAdapter
    }

    private fun getDailyChallenge() {

        val requestData = JsonObject()

        ApiServiceProvider.getInstance(this)

            .sendPostData(
                Constants.UrlPath.GET_DAILY_CHALLENGE, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")

                        if (status) {

                            val response = responseObj.getJSONArray("data")
                            val stationName = response.getJSONObject(0).getString("staion_name")
                            val comment = response.getJSONObject(0).getString("comment")
//                            val video = response.getJSONObject(0).getString("video")

                            binding.tvStationName.setText(stationName)
                            binding.tvComment.setText(comment)

                            val size = response.length()
                            if (size == 1) {
                                val jsonObjectone = response.getJSONObject(0)

                                val videoUri = getMedia(jsonObjectone.getString("video"))
                                binding.videoView1.setVideoURI(videoUri)
                                binding.videoView1.seekTo(1000)

                            } else if (size == 2) {
                                val jsonObjectone = response.getJSONObject(0)
                                val jsonObjecttwo = response.getJSONObject(1)

                                val videoUri1 = getMedia(jsonObjectone.getString("video"))
                                binding.videoView1.setVideoURI(videoUri1)
                                binding.videoView1.seekTo(5000)

                                val videoUri2 = getMedia(jsonObjecttwo.getString("video"))
                                binding.videoView2.setVideoURI(videoUri2)
                                binding.videoView2.seekTo(5000)

                            } else if (size >= 3) {
                                val jsonObjectone = response.getJSONObject(0)
                                val jsonObjecttwo = response.getJSONObject(1)

                                val videoUri1 = getMedia(jsonObjectone.getString("video"))
                                binding.videoView1.setVideoURI(videoUri1)
                                binding.videoView1.seekTo(5000)

                                val videoUri2 = getMedia(jsonObjecttwo.getString("video"))
                                binding.videoView2.setVideoURI(videoUri2)
                                binding.videoView2.seekTo(5000)

                                val counterSize = size - 2
                                binding.tvVideoCounter.setText("+$counterSize")

                            } else {
                                binding.tvStationName.visibility = View.INVISIBLE!!
                                binding.tvComment.visibility = View.INVISIBLE!!
                                binding.videoView1.visibility = View.INVISIBLE!!
                                binding.videoView1.visibility = View.INVISIBLE!!
                            }

                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun getMedia(mediaName: String): Uri? {
        return if (URLUtil.isValidUrl(mediaName)) {
            Uri.parse(mediaName)
        } else {
            // you can also put a video file in raw package and get file from there as shown below
            Uri.parse(
                "android.resource://" + this.packageName.toString() +
                        "/raw/" + mediaName
            )
        }
    }

    private fun setArchivedWorkAdapter() {
        archivedWorkOutAdapter = ArchivedWorkOutAdapter(this, archivedWorkOutModel, true)
        layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.rvWorkOut.layoutManager = layoutManager
        binding.rvWorkOut.adapter = archivedWorkOutAdapter
    }

}