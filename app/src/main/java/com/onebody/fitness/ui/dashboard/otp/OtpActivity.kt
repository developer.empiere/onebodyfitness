package com.onebody.fitness.ui.dashboard.otp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.gson.JsonObject
import com.onebody.fitness.databinding.ActivityOtpBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.login.ForgotPasswordActivity
import kotlinx.android.synthetic.main.activity_otp.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject

class OtpActivity : AppCompatActivity() {
    lateinit var binding: ActivityOtpBinding

    var email: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityOtpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        email = getIntent().getStringExtra("email").toString()
        binding.txtEmailId.setText("OTP has been sent to $email")

        ivPrevious.setOnClickListener(View.OnClickListener {
            onBackPressed()
            Animatoo.animateSlideRight(this)
        })
        btnVerify.setOnClickListener {
            callVerifiedApi()
        }
    }

    private fun callVerifiedApi() {

        val requestData = JsonObject()
        requestData.addProperty("Email", email)
        requestData.addProperty("OTP", otp_view.text.toString())
        requestData.addProperty("IsDeleteAccount", "0")

        ApiServiceProvider.getInstance(this).sendPostData(
            Constants.UrlPath.VERIFY_OTP, requestData, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    val message = responseObj.getString("message")
                    if (status) {
                        val window = Intent(applicationContext, ForgotPasswordActivity::class.java)
                        window.putExtra("email", email)
                        startActivity(window)
                    } else {
                        Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }
}