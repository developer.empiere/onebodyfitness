package com.onebody.fitness.ui.dashboard.facetype.female.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.facetype.female.FemaleBodyTypeActivity
import com.onebody.fitness.ui.dashboard.facetype.female.FemaleFaceTypeActivity
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.female_body_bottom.view.*
import kotlinx.android.synthetic.main.fragment_pear.view.*
import kotlinx.android.synthetic.main.toolbar.view.*

class AppleFragment : Fragment() {
    lateinit var mRootView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        mRootView = inflater.inflate(R.layout.fragment_apple, container, false)
        mRootView.femaleBody.txtBodyAngle.text = "Apple"

        mRootView.femaleBody.ivPreviousBody.setOnClickListener(View.OnClickListener {
            (activity as FemaleBodyTypeActivity).moveToPrevious()
        })
        mRootView.femaleBody.ivNextBody.setOnClickListener(View.OnClickListener {
            (activity as FemaleBodyTypeActivity).moveToNext()
        })
        mRootView.btnSubmit.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, FemaleFaceTypeActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(context)

        })
        mRootView.toolbar.ivPrevious.setOnClickListener(View.OnClickListener {
            (activity as FemaleBodyTypeActivity).moveToPrevious()
            Animatoo.animateSlideRight(context)

        })

        mRootView.toolbar.ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(activity, NotificationActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(context)

        })
        return mRootView
    }

    companion object {

        fun newInstance(param1: String, param2: String) =
            AppleFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}