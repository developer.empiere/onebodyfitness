package com.onebody.fitness.ui.dashboard.clothtypefemale

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ClothTypeFemaleModel {

    @SerializedName("data")
    var data: Data? = null

    class Data : Serializable {

        @SerializedName("id")
        var id: Int = 0

        @SerializedName("cloths")
        var cloths: String? = null

    }
}