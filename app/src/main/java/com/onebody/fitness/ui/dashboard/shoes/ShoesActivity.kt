package com.onebody.fitness.ui.dashboard.shoes

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.onebody.fitness.databinding.ActivityShoesBinding
import com.onebody.fitness.ui.dashboard.facetype.female.FemaleFaceTypeActivity
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.activity_shoes.*
import kotlinx.android.synthetic.main.toolbar.*

class ShoesActivity : AppCompatActivity() {
    lateinit var binding: ActivityShoesBinding
    private var shoesAdapter: ShoesAdapter? = null
    private var layoutManager: GridLayoutManager? = null
    lateinit var shoesModel: ArrayList<ShoesModel>

    var selectedshoesColor: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityShoesBinding.inflate(layoutInflater)

        setContentView(binding.root)


        ivPrevious.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
        })


        card_view_blue.setOnClickListener(View.OnClickListener {

            val intent = Intent(this, ShoesDetailActivity::class.java)
            startActivity(intent)
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}