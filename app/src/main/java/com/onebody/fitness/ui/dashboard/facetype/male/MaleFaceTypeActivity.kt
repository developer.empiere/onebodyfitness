package com.onebody.fitness.ui.dashboard.facetype.male

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.onebody.fitness.retrofit.SessionManager
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityFaceTypeBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.DashBoardActivity
import com.onebody.fitness.ui.dashboard.avatar.AvatarViewActivity
import com.onebody.fitness.ui.dashboard.clothtypemale.ClothTypeMaleActivity
import com.onebody.fitness.ui.dashboard.facetype.male.adapter.*
import com.onebody.fitness.ui.dashboard.facetype.male.model.*
import com.onebody.fitness.ui.dashboard.facetype.models.FacePropertyModel
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.activity_face_type.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject

class MaleFaceTypeActivity : AppCompatActivity() {

    lateinit var binding: ActivityFaceTypeBinding
    lateinit var facePropertyModels: ArrayList<FacePropertyModel>
    private var skinColorMaleAdapter: SkinColorMaleAdapter? = null
    private var gridLayoutManager: GridLayoutManager? = null
    private var skinColorMaleModel: MutableList<SkinColorMaleModel.Data>? = null
    private var hairColorMaleAdapter: HairColorMaleAdapter? = null
    private var hairColorMaleModel: MutableList<HairColorMaleModel.Data>? = null
    private var hairStyleMaleAdapter: HairStyleMaleAdapter? = null
    private var hairStyleMaleModel: MutableList<HairStyleMaleModel.Data>? = null
    private var facialHairMaleAdapter: FacialHairMaleAdapter? = null
    private var facialHairMaleModel: MutableList<FacialHairMaleModel.Data>? = null

    var selectedSkinColor: Int = -1
    var selectedHairColor: Int = -1
    var selectedHairStyle: Int = -1
    var selectedFacialHair: Int = -1

    var lastIndexPosition: Int = 0

    var bodytype: String = "0"
    var clothesID: String = "0"
    val LAUNCH_CLOTHES_ACTIVITY: Int = 1

    var email: String = ""
    var password: String = ""

    private lateinit var mSessionManager: SessionManager

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityFaceTypeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        bodytype = intent.getStringExtra("BodyType").toString()

        mSessionManager = SessionManager(applicationContext)

        email = mSessionManager.getData(SessionManager.EMAIL)!!
        password = mSessionManager.getData(SessionManager.PASSWORD)!!

        ivPrevious.setOnClickListener(View.OnClickListener {
            Animatoo.animateSlideRight(this)
            onBackPressed()
        })

        ivNotification.setOnClickListener(View.OnClickListener {
            Animatoo.animateSlideLeft(this)
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
        })

        binding.btnNext.setOnClickListener(View.OnClickListener {
            /*   if (!checkValidationForAll()) {
                   Animatoo.animateSlideLeft(this)
                   val intent = Intent(this, AvatarViewActivity::class.java)
                   intent.putExtra("BodyType", bodytype)
                   intent.putExtra("SkinID", selectedSkinColor)
                   intent.putExtra("HairColor", selectedHairColor)
                   intent.putExtra("FacialHair", selectedFacialHair)
                   intent.putExtra("HairStyle", selectedHairStyle)
                   intent.putExtra("ClothID", clothesID)
                   startActivity(intent)
                  // SubmitMaleAvatarApi()
               }*/
        })

        facePropertyModels = ArrayList()
        setButtonClickListner()
        binding.groupChoices.foregroundGravity = Gravity.CENTER_HORIZONTAL

        binding.ctSkinColor.performClick()
    }

    /*private fun checkValidationForAll(): Boolean {

        if (selectedHairColor == -1) {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select Hair color", Toast.LENGTH_LONG).show()
            return true

        } else if (selectedFacialHair == -1) {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select Facial Hair", Toast.LENGTH_LONG).show()

            return true

        } else if (selectedHairStyle == -1) {

            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select Hair Style", Toast.LENGTH_LONG).show()
            return true

        } else if (clothesID == "0") {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select Clothing Shorts", Toast.LENGTH_LONG).show()
            return true
        }
        return false
    }*/

    private fun SubmitMaleAvatarApi() {

        val map = HashMap<String, String>()
        map.put("BodyType", bodytype)
        map.put("sendHashmapPostData", selectedSkinColor.toString())
        map.put("HairColorID", selectedHairColor.toString())
        map.put("FacialHairID", selectedFacialHair.toString())
        map.put("HairStyleID", selectedHairStyle.toString())
        map.put("ClothingShortId", clothesID)

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_SUBMIT_AVATAR_MALE, map, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    val message = responseObj.getString("message")

                    if (status) {

                        Toast.makeText(this, "Avtar Submitted Successfully", Toast.LENGTH_SHORT)
                            .show()

                        if (mSessionManager.getBooleanData(SessionManager.IS_FROM_PROFILE)) {
                            val i = Intent(this, DashBoardActivity::class.java)
                            startActivity(i)
                            finish()

                        } else {
                            loginApiCall()
                        }

                    } else {
                        Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun loginApiCall() {
        val requestData = JsonObject()
        requestData.addProperty("email", email)
        requestData.addProperty("psw", password)

        ApiServiceProvider.getInstance(this)
            .sendPostData(Constants.UrlPath.LOGIN, requestData, true,
                RetrofitListener { response ->

                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")

                        if (status) {
                            Toast.makeText(this, "Login Successfully", Toast.LENGTH_SHORT).show()

                            val response = responseObj.getJSONArray("data")
                            val name = response.getJSONObject(0).getString("name")
                            val id = response.getJSONObject(0).getString("id")
                            val email = response.getJSONObject(0).getString("email")
                            val isSocialLogin = response.getJSONObject(0).getString("IsSocialLogin")
                            val avatarStatus = response.getJSONObject(0).getString("avatarStatus")
                            val token = responseObj.getString("token")

                            mSessionManager.setData(SessionManager.AUTH_TOKEN, token)
                            mSessionManager.setData(SessionManager.USER_NAME, name)
                            mSessionManager.setData(SessionManager.USER_ID, id)
                            mSessionManager.setData(SessionManager.EMAIL, email)
                            mSessionManager.setData(SessionManager.IIS_SOCIAL_LOGIN, isSocialLogin)
                            mSessionManager.setData(SessionManager.AVATAR_STATUS, avatarStatus)

                            Animatoo.animateSlideLeft(this)

                            val i = Intent(this, DashBoardActivity::class.java)
                            startActivity(i)
                            finish()

                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun setButtonClickListner() {

        binding.ctSkinColor.setOnClickListener(View.OnClickListener {
            lastIndexPosition = 0
            callSkinColorApi()
        })


        binding.ctHairColor.setOnClickListener(View.OnClickListener {
            lastIndexPosition = 1
            callApiHairColor()
        })

        binding.ctFacialHair.setOnClickListener(View.OnClickListener {
            if (!checkValidationFacialHair()) {
                lastIndexPosition = 2
                callFacialHairApi()
            }
        })
        binding.ctHairStyle.setOnClickListener(View.OnClickListener {
            if (!checkValidationHairStyle()) {
                lastIndexPosition = 3
                callApiHairStyle()
            }
        })
        binding.ctClothingShorts.setOnClickListener(View.OnClickListener {
            if (!checkValidationClothingShortes()) {
                val intent = Intent(this, ClothTypeMaleActivity::class.java)
                intent.putExtra("BodyType", bodytype)
                intent.putExtra("SkinID", selectedSkinColor)
                intent.putExtra("HairColor", selectedHairColor)
                intent.putExtra("FacialHair", selectedFacialHair)
                intent.putExtra("HairStyle", selectedHairStyle)
                startActivityForResult(intent, LAUNCH_CLOTHES_ACTIVITY)
            }
        })
    }

    private fun checkValidationFacialHair(): Boolean {
        if (selectedHairColor == -1) {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select Hair color", Toast.LENGTH_LONG).show()
            return true
        }
        return false
    }

    private fun checkValidationHairStyle(): Boolean {
        if (selectedHairColor == -1) {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select Hair color", Toast.LENGTH_LONG).show()
            return true
        } else if (selectedFacialHair == -1) {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select Facial Hair", Toast.LENGTH_LONG).show()
            return true
        }
        return false
    }

    private fun checkValidationClothingShortes(): Boolean {
        if (selectedHairColor == -1) {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select Hair color", Toast.LENGTH_LONG).show()
            return true

        } else if (selectedFacialHair == -1) {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select Facial Hair", Toast.LENGTH_LONG).show()
            return true

        } else if (selectedHairStyle == -1) {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select Hair Style", Toast.LENGTH_LONG).show()
            return true
        }
        return false
    }

    private fun callFacialHairApi() {
        val map = HashMap<String, String>()
        map.put("BodyType", bodytype)
        map.put("SkinID", selectedSkinColor.toString())
        map.put("HairColorID", selectedHairColor.toString())

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_FACIAL_HAIR_MALE, map, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    if (status) {
                        val data = responseObj.getJSONArray("data")
                        val gson = Gson()
                        val listType =
                            object : TypeToken<MutableList<FacialHairMaleModel.Data?>?>() {}.type
                        facialHairMaleModel = gson.fromJson<Any>(
                            data.toString(),
                            listType
                        ) as MutableList<FacialHairMaleModel.Data>?
                        binding.tvSelectSkin.visibility = View.VISIBLE
                        binding.tvSelectSkin.text =
                            resources.getString(R.string.select_your_facial_hair)
                        setAdapterFacialHair()
                    } else {
                    }
                } catch (e: Exception) {
                }
            })
    }

    private fun setAdapterFacialHair() {
        facialHairMaleAdapter = FacialHairMaleAdapter(this, facialHairMaleModel!!)
        gridLayoutManager = GridLayoutManager(this, 2)
        facialHairMaleAdapter?.setOnClickListener {
            selectedFacialHair = it
        }
        rvSkinColor!!.layoutManager = gridLayoutManager
        rvSkinColor!!.adapter = facialHairMaleAdapter
        selectedFacialHair = 1
    }

    private fun callApiHairStyle() {
        val map = HashMap<String, String>()
        map.put("BodyType", bodytype)
        map.put("SkinID", selectedSkinColor.toString())
        map.put("HairColorID", selectedHairColor.toString())
        map.put("FacialHairID", selectedFacialHair.toString())

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_HAIR_STYLE_MALE, map, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    val message = responseObj.getString("message")
                    if (status) {
                        val data = responseObj.getJSONArray("data")
                        val gson = Gson()
                        val listType =
                            object : TypeToken<MutableList<HairStyleMaleModel.Data?>?>() {}.type
                        hairStyleMaleModel = gson.fromJson<Any>(
                            data.toString(),
                            listType
                        ) as MutableList<HairStyleMaleModel.Data>?
                        binding.tvSelectSkin.visibility = View.VISIBLE
                        binding.tvSelectSkin.text =
                            resources.getString(R.string.select_your_hair_style)
                        setAdapterHairStyle()
                    } else {
                        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()

                    }
                } catch (e: Exception) {
                }
            })
    }

    private fun setAdapterHairStyle() {
        hairStyleMaleAdapter = HairStyleMaleAdapter(this, hairStyleMaleModel!!)
        gridLayoutManager = GridLayoutManager(this, 2)
        hairStyleMaleAdapter?.setOnClickListener {
            selectedHairStyle = it
        }
        rvSkinColor!!.layoutManager = gridLayoutManager
        rvSkinColor!!.adapter = hairStyleMaleAdapter
        selectedHairStyle = 1
    }

    private fun callApiHairColor() {
        val map = HashMap<String, String>()
        map.put("BodyType", bodytype.toString())
        map.put("SkinID", selectedSkinColor.toString())

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_HAIR_COLOR_MALE, map, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")

                    if (status) {
                        val data = responseObj.getJSONArray("data")
                        val gson = Gson()
                        val listType =
                            object : TypeToken<MutableList<HairColorMaleModel.Data?>?>() {}.type
                        hairColorMaleModel = gson.fromJson<Any>(
                            data.toString(),
                            listType
                        ) as MutableList<HairColorMaleModel.Data>?
                        binding.tvSelectSkin.visibility = View.VISIBLE
                        binding.tvSelectSkin.text =
                            resources.getString(R.string.select_your_hair_color)
                        setAdapterHairColor()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun selectToggleButtonWithCustomIndex(index: Int) {
        when (index) {
            0 -> binding.ctSkinColor.isChecked = true
            1 -> binding.ctHairColor.isChecked = true
            2 -> binding.ctFacialHair.isChecked = true
            3 -> binding.ctHairStyle.isChecked = true
        }
    }

    private fun setAdapterHairColor() {
        hairColorMaleAdapter = HairColorMaleAdapter(this, hairColorMaleModel!!)
        hairColorMaleAdapter?.setOnClickListener {
            selectedHairColor = it
        }
        gridLayoutManager = GridLayoutManager(this, 2)
        rvSkinColor!!.layoutManager = gridLayoutManager
        rvSkinColor!!.adapter = hairColorMaleAdapter
        selectedHairColor = 1
    }

    private fun callSkinColorApi() {
        val map = HashMap<String, String>()
        map.put("BodyType", bodytype.toString())
        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_SKINS_MALE, map, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    if (status) {
                        val data = responseObj.getJSONArray("data")
                        val gson = Gson()
                        val listType =
                            object : TypeToken<MutableList<SkinColorMaleModel.Data?>?>() {}.type
                        skinColorMaleModel = gson.fromJson<Any>(
                            data.toString(),
                            listType
                        ) as MutableList<SkinColorMaleModel.Data>?
                        binding.tvSelectSkin.visibility = View.VISIBLE
                        binding.tvSelectSkin.text = resources.getString(R.string.select_your_skin)
                        setAdapterSKinColor()

                    } else {
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun setAdapterSKinColor() {
        skinColorMaleAdapter = SkinColorMaleAdapter(this, skinColorMaleModel!!)
        skinColorMaleAdapter?.setOnClickListener {
            selectedSkinColor = it
        }
        gridLayoutManager = GridLayoutManager(this, 2)
        rvSkinColor.layoutManager = gridLayoutManager
        rvSkinColor!!.adapter = skinColorMaleAdapter
        selectedSkinColor = 1
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode === LAUNCH_CLOTHES_ACTIVITY) {
            if (resultCode === RESULT_OK) {
                clothesID = data!!.getStringExtra("result").toString()
            }
            if (resultCode === RESULT_CANCELED) {
                Toast.makeText(
                    this,
                    "Please Choose Clothes and then Create Your Avatar",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }
}