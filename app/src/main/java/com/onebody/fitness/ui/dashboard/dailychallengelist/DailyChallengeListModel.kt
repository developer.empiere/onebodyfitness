package com.onebody.fitness.ui.dashboard.dailychallengelist

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DailyChallengeListModel {
    @SerializedName("data")
    var data: MutableList<Data>? = null

    class Data : Serializable {

        @SerializedName("dailychallenge_id")
        var dailychallenge_id: String? = null

        @SerializedName("staion_name")
        var staion_name: String? = null

        @SerializedName("video")
        var video: String? = null

        @SerializedName("time_minutes")
        var time_minutes: String? = null

        @SerializedName("comment")
        var comment: String? = null

    }
}