package com.onebody.fitness.ui.dashboard.facetype.male.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.facetype.male.model.ClothingShortMaleModel
import kotlinx.android.synthetic.main.row_clothing_short_male.view.*

class ClothingShortMaleAdapter(
    private val context: Context?,
    private val clothingShortModel: MutableList<ClothingShortMaleModel.Data>
) :
    RecyclerView.Adapter<ClothingShortMaleAdapter.ViewHolder>() {
    var selectedPosition = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_clothing_short_male, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = clothingShortModel[position]
        context?.let {
            Glide.with(it)
                .load(clothingShortModel.get(position).cloths)
                .error(R.drawable.ic_launcher_background)
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(holder.imgMaleClothShort)
        }

        if (selectedPosition < 0) {
            selectedPosition = 0
        }

        if (selectedPosition == position) {
            holder.cv_clothing_shorts.background =
                context!!.resources.getDrawable(R.drawable.red_outside_border)
        } else {
            holder.cv_clothing_shorts.background =
                context!!.resources.getDrawable(R.drawable.white_outside_border)
        }
        holder.cv_clothing_shorts.setOnClickListener {
            selectedPosition = position
            onItemClickListener?.let {
                it(selectedPosition + 1)
            }
            notifyDataSetChanged()
        }
    }

    private var onItemClickListener: ((Int) -> Unit)? = null

    fun setOnClickListener(listener: (Int) -> Unit) {
        onItemClickListener = listener
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imgMaleClothShort: ImageView = itemView.imgMaleClothShort
        val cv_clothing_shorts = itemView.cv_clothing_shorts

    }

    override fun getItemCount(): Int {
        return clothingShortModel.size
    }
}