package com.onebody.fitness.ui.dashboard.facetype.female

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.onebody.fitness.retrofit.SessionManager
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityFemaleFaceTypeBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.DashBoardActivity
import com.onebody.fitness.ui.dashboard.avatar.AvatarViewActivity
import com.onebody.fitness.ui.dashboard.clothtypefemale.ClothTypeFemaleActivity
import com.onebody.fitness.ui.dashboard.facetype.female.adapter.*
import com.onebody.fitness.ui.dashboard.facetype.female.model.*
import com.onebody.fitness.ui.dashboard.facetype.models.FacePropertyModel
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.activity_female_face_type.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject
import java.util.*


class FemaleFaceTypeActivity : AppCompatActivity() {

    lateinit var binding: ActivityFemaleFaceTypeBinding

    lateinit var facePropertyModels: ArrayList<FacePropertyModel>
    private var skinColorFemaleAdapter: SkinColorFemaleAdapter? = null
    private var skinColorFemaleModel: MutableList<SkinColorFemaleModel.Data>? = null
    private var gridLayoutManager: GridLayoutManager? = null
    private var hairColorFemaleAdapter: HairColorFemaleAdapter? = null
    private var hairColorFemaleModel: MutableList<HairColorFemaleModel.Data>? = null
    private var hairStyleFemaleAdapter: HairStyleFemaleAdapter? = null
    private var hairStyleFemaleModel: MutableList<HairStyleFemaleModel.Data>? = null

    var selectedSkinColor: Int = -1
    var selectedHairColor: Int = -1
    var selectedHairStyle: Int = -1

    var bd: String = "0"
    var lastIndexPosition: Int = 0
    val LAUNCH_CLOTHES_ACTIVITY: Int = 1
    var clothesID: String = "0"

    var email: String = ""
    var password: String = ""

    private var mSessionManager: SessionManager? = null

    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityFemaleFaceTypeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        bd = intent.getStringExtra("BodyType").toString()

        mSessionManager = SessionManager(applicationContext)

        email = mSessionManager?.getData(SessionManager.EMAIL)!!
        password = mSessionManager?.getData(SessionManager.PASSWORD)!!

        mSessionManager = SessionManager(applicationContext)

        facePropertyModels = ArrayList()
        setButtonClickListner()
        binding.groupChoices.foregroundGravity = Gravity.CENTER_HORIZONTAL
        binding.ctSkinColor.performClick()

        ivPrevious.setOnClickListener(View.OnClickListener {
            Animatoo.animateSlideLeft(this)
            onBackPressed()
        })

        ivNotification.setOnClickListener(View.OnClickListener {
            Animatoo.animateSlideLeft(this)
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
        })

        binding.btnNext.setOnClickListener(View.OnClickListener {
            if (!checkValidationClothColorForAll()) {
                Animatoo.animateSlideLeft(this)
//                val intent = Intent(this, AvatarViewActivity::class.java)
//                intent.putExtra("BodyTypeF", bd)
//                intent.putExtra("SkinIDF", selectedSkinColor)
//                intent.putExtra("HairColorF", selectedHairColor)
//                intent.putExtra("HairStyleIDF", selectedHairStyle)
//                intent.putExtra("ClothIDF", clothesID)
//                startActivity(intent)
                SubmitFeMaleAvatarApi()
            }
        })
    }

    private fun checkValidationClothColorForAll(): Boolean {
        if (selectedHairColor == -1) {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select Hair color", Toast.LENGTH_LONG).show()
            return true
        } else if (selectedHairStyle == -1) {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select HairStyle Color", Toast.LENGTH_LONG).show()
            return true
        } else if (clothesID == "0") {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select Clothing Shorts", Toast.LENGTH_LONG).show()
            return true
        }
        return false
    }

    private fun SubmitFeMaleAvatarApi() {

        val map = HashMap<String, String>()
        map["BodyType"] = bd
        map["SkinID"] = selectedSkinColor.toString()
        map["HairColorID"] = selectedHairColor.toString()
        map["HairStyleID"] = selectedHairStyle.toString()
        map["ClothingShortId"] = clothesID
        if(mSessionManager!!.getBooleanData(SessionManager.IS_MALE)){
            map["Gender"] = "male"
        }else{
            map["Gender"] = "female"
        }

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_SUBMIT_AVATAR_FEMALE, map, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    val message = responseObj.getString("message")
                    if (status) {

                        Toast.makeText(this,"Avtar Submitted Successfully",Toast.LENGTH_SHORT).show()

                        if (mSessionManager!!.getBooleanData(SessionManager.IS_FROM_PROFILE)) {
                            val i = Intent(this, DashBoardActivity::class.java)
                            startActivity(i)
                            finish()
                        } else {
                            loginApiCall()
                        }
                    } else {
                        Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                }
            })
    }

    private fun loginApiCall() {
        val requestData = JsonObject()
        requestData.addProperty("email", email)
        requestData.addProperty("psw", password)

        ApiServiceProvider.getInstance(this)
            .sendPostData(Constants.UrlPath.LOGIN, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")

                        if (status) {
                            Toast.makeText(this,"Login Successfully",Toast.LENGTH_SHORT).show()

                            val response = responseObj.getJSONArray("data")
                            val name = response.getJSONObject(0).getString("name")
                            val id = response.getJSONObject(0).getString("id")
                            val email = response.getJSONObject(0).getString("email")
                            val isSocialLogin = response.getJSONObject(0).getString("IsSocialLogin")
                            val avatarStatus = response.getJSONObject(0).getString("avatarStatus")
                            val token = responseObj.getString("token")

                            mSessionManager?.setData(SessionManager.AUTH_TOKEN, token)
                            mSessionManager?.setData(SessionManager.USER_NAME, name)
                            mSessionManager?.setData(SessionManager.USER_ID, id)
                            mSessionManager?.setData(SessionManager.EMAIL, email)
                            mSessionManager?.setData(SessionManager.IIS_SOCIAL_LOGIN, isSocialLogin)
                            mSessionManager?.setData(SessionManager.AVATAR_STATUS, avatarStatus)

                            Animatoo.animateSlideLeft(this)

                            val i = Intent(this, DashBoardActivity::class.java)
                            startActivity(i)
                            finish()

                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }


    private fun setButtonClickListner() {
        binding.ctSkinColor.setOnClickListener(View.OnClickListener {
            lastIndexPosition = 0
            callSkinColorApi()
        })

        binding.ctHairColor.setOnClickListener(View.OnClickListener {
            lastIndexPosition = 1
            callHairColorApi()
        })

        binding.ctHairStyle.setOnClickListener(View.OnClickListener {
            if (!checkValidationHairStyle()) {
                lastIndexPosition = 2
                callHairStyleApi()
            }
        })

        binding.ctClothingShorts.setOnClickListener(View.OnClickListener {
            if (!checkValidationClothingShorts()) {
                lastIndexPosition = 3
                val intent = Intent(this, ClothTypeFemaleActivity::class.java)
                intent.putExtra("BodyType", bd)
                intent.putExtra("SkinID", selectedSkinColor)
                intent.putExtra("HairColor", selectedHairColor)
                intent.putExtra("HairStyleID", selectedHairStyle)
                startActivityForResult(intent, LAUNCH_CLOTHES_ACTIVITY)
            }
        })
    }

    private fun checkValidationHairStyle(): Boolean {
        if (selectedHairColor == -1) {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select Hair color", Toast.LENGTH_LONG).show()
            return true
        }
        return false

    }

    private fun checkValidationClothingShorts(): Boolean {
        if (selectedHairColor == -1) {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select Hair color", Toast.LENGTH_LONG).show()
            return true
        } else if (selectedHairStyle == -1) {
            selectToggleButtonWithCustomIndex(lastIndexPosition)
            Toast.makeText(this, "Please select HairStyle Color", Toast.LENGTH_LONG).show()
            return true
        }
        return false
    }

    private fun selectToggleButtonWithCustomIndex(index: Int) {
        when (index) {
            0 -> binding.ctSkinColor.isChecked = true
            1 -> binding.ctHairColor.isChecked = true
            //2 -> binding.ctEyeColor.isChecked = true
            2 -> binding.ctHairStyle.isChecked = true
            //4 -> binding.ctShoesColor.isChecked = true
            3 -> binding.ctClothingShorts.isChecked = true

        }
    }

    private fun callSkinColorApi() {
        val map = HashMap<String, String>()
        map.put("BodyType", bd)
        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_SKIN_COLOR_FEMALE, map, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    val message = responseObj.getString("message")
                    if (status) {
                        val data = responseObj.getJSONArray("data")
                        val gson = Gson()
                        val listType =
                            object : TypeToken<MutableList<SkinColorFemaleModel.Data?>?>() {}.type
                        skinColorFemaleModel = gson.fromJson<Any>(
                            data.toString(),
                            listType
                        ) as MutableList<SkinColorFemaleModel.Data>?
                        binding.tvSelectSkin.visibility = View.VISIBLE
                        binding.tvSelectSkin.text = resources.getString(R.string.select_your_skin)
                        setAdapterSKinColor()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun setAdapterSKinColor() {
        skinColorFemaleAdapter = SkinColorFemaleAdapter(this, skinColorFemaleModel!!)
        skinColorFemaleAdapter?.setOnClickListener {
            selectedSkinColor = it
        }
        gridLayoutManager = GridLayoutManager(this, 2)
        rvFaceType!!.layoutManager = gridLayoutManager
        rvFaceType!!.adapter = skinColorFemaleAdapter
        selectedSkinColor = 1

    }

    private fun callHairColorApi() {
        val map = HashMap<String, String>()
        map.put("BodyType", bd)
        map.put("SkinID", selectedSkinColor.toString())

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_HAIR_COLOR_FEMALE, map, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    if (status) {
                        val data = responseObj.getJSONArray("data")
                        val gson = Gson()
                        val listType =
                            object : TypeToken<MutableList<HairColorFemaleModel.Data?>?>() {}.type
                        hairColorFemaleModel = gson.fromJson<Any>(
                            data.toString(),
                            listType
                        ) as MutableList<HairColorFemaleModel.Data>?
                        binding.tvSelectSkin.visibility = View.VISIBLE
                        binding.tvSelectSkin.text =
                            resources.getString(R.string.select_your_hair_color)
                        setAdapterHairColor()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun setAdapterHairColor() {
        hairColorFemaleAdapter = HairColorFemaleAdapter(this, hairColorFemaleModel!!)
        hairColorFemaleAdapter?.setOnClickListener {
            selectedHairColor = it
        }
        gridLayoutManager = GridLayoutManager(this, 2)
        rvFaceType!!.layoutManager = gridLayoutManager
        rvFaceType!!.adapter = hairColorFemaleAdapter
        selectedHairColor = 1
    }

    private fun callHairStyleApi() {
        val map = HashMap<String, String>()
        map.put("BodyType", bd)
        map.put("SkinID", selectedSkinColor.toString())
        map.put("HairColorID", selectedHairColor.toString())

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.GET_HAIR_STYLE_FEMALE, map, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    val message = responseObj.getString("message")
                    if (status) {
                        val data = responseObj.getJSONArray("data")
                        val gson = Gson()
                        val listType =
                            object : TypeToken<MutableList<HairStyleFemaleModel.Data?>?>() {}.type
                        hairStyleFemaleModel = gson.fromJson<Any>(
                            data.toString(),
                            listType
                        ) as MutableList<HairStyleFemaleModel.Data>?
                        binding.tvSelectSkin.visibility = View.VISIBLE
                        binding.tvSelectSkin.text =
                            resources.getString(R.string.select_your_hair_style)
                        setAdapterHairStyle()
                    } else {
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun setAdapterHairStyle() {
        hairStyleFemaleAdapter = HairStyleFemaleAdapter(this, hairStyleFemaleModel!!)
        hairStyleFemaleAdapter?.setOnClickListener {
            selectedHairStyle = it
        }
        gridLayoutManager = GridLayoutManager(this, 2)
        rvFaceType!!.layoutManager = gridLayoutManager
        rvFaceType!!.adapter = hairStyleFemaleAdapter
        selectedHairStyle = 1
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode === LAUNCH_CLOTHES_ACTIVITY) {
            if (resultCode === RESULT_OK) {
                clothesID = data!!.getStringExtra("result").toString()
            }
            if (resultCode === RESULT_CANCELED) {
                Toast.makeText(
                    this,
                    "Please Choose Clothes and then Create Your Avatar",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

}