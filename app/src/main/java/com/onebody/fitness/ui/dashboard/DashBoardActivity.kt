package com.onebody.fitness.ui.dashboard

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.PopupWindow
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import com.onebody.fitness.retrofit.SessionManager
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityDashBoardBinding
import com.onebody.fitness.retrofit.AppUtils
import com.onebody.fitness.retrofit.AppUtils.dimBehind
import com.onebody.fitness.ui.dashboard.chatList.ChatListFragment
import com.onebody.fitness.ui.dashboard.contactus.ContactusActivity
import com.onebody.fitness.ui.dashboard.dailychallange.DailyChallangeFragment
import com.onebody.fitness.ui.dashboard.dailychallange.DailyChallengeActivity
import com.onebody.fitness.ui.dashboard.home.HomeFragment
import com.onebody.fitness.ui.dashboard.howitworks.HowItWorksActivity
import com.onebody.fitness.ui.dashboard.leaderboard.LeaderBoardActivity
import com.onebody.fitness.ui.dashboard.movement.MoveMentFragment
import com.onebody.fitness.ui.dashboard.myprofile.MyProfileFragment
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import com.onebody.fitness.ui.dashboard.setting.SettingActivity
import kotlinx.android.synthetic.main.nav_header.*
import kotlinx.android.synthetic.main.nav_header.view.*


class DashBoardActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    lateinit var binding: ActivityDashBoardBinding
    var actionBarDrawerToggle: ActionBarDrawerToggle? = null
    private lateinit var mSessionManager : SessionManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDashBoardBinding.inflate(layoutInflater)
        setContentView(binding.root)

        actionBarDrawerToggle = ActionBarDrawerToggle(this, binding.drawerlayout, 0, 0)
        actionBarDrawerToggle!!.isDrawerIndicatorEnabled = true
        binding.drawerlayout.addDrawerListener(actionBarDrawerToggle!!)
        binding.nvView.setNavigationItemSelectedListener(this@DashBoardActivity)
        binding.nvView.isVerticalScrollBarEnabled = true
        binding.nvView.bringToFront()
        actionBarDrawerToggle!!.syncState()


        supportActionBar?.setDisplayHomeAsUpEnabled(true);
        mSessionManager = SessionManager(this)


        binding.toolbar.ivMenu.setOnClickListener(View.OnClickListener {
            binding.drawerlayout.openDrawer(binding.nvView)
        })

        binding.nvView.setOnClickListener {
            binding.drawerlayout.closeDrawer(GravityCompat.START)
        }

        binding.toolbar.ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
        })

        setupBottomNavigation()
        clickLeftMenuItem()
    }

    fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.frameContainer, fragment).commit()
    }

    private fun clickLeftMenuItem() {
        val headerView: View = binding.nvView.getHeaderView(0)

        headerView.lnrMyProfile.setOnClickListener(View.OnClickListener {
            /*val intent = Intent(this, MyProfileActivity::class.java)
            startActivity(intent)*/
            binding.bnMenu.selectedItemId = R.id.mProfile
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameContainer, MyProfileFragment())
                .commit()
            binding.drawerlayout.closeDrawer(GravityCompat.START)

        })

        headerView.lnrDailyChallenge.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, DailyChallengeActivity::class.java)
            startActivity(intent)
            binding.drawerlayout.closeDrawer(GravityCompat.START)

        })
        headerView.img_back_nav.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        headerView.lnrLeaderBoard.setOnClickListener(View.OnClickListener {
            mSessionManager.setData(SessionManager.VIDEOID,"")
            val intent = Intent(this, LeaderBoardActivity::class.java)
            startActivity(intent)
            binding.drawerlayout.closeDrawer(GravityCompat.START)
        })

        headerView.llSetting.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, SettingActivity::class.java)
            startActivity(intent)
            binding.drawerlayout.closeDrawer(GravityCompat.START)
        })

        headerView.llContactUs.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, ContactusActivity::class.java)
            startActivity(intent)
            binding.drawerlayout.closeDrawer(GravityCompat.START)
        })

        headerView.txtHowItWork.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, HowItWorksActivity::class.java)
            startActivity(intent)
            binding.drawerlayout.closeDrawer(GravityCompat.START)

        })

        headerView.llLogOut.setOnClickListener(View.OnClickListener {
            binding.drawerlayout.closeDrawer(GravityCompat.START)
            showPopupWindowwLogOut(this.llLogOut)
        })
    }

    @SuppressLint("ClickableViewAccessibility", "InflateParams")
    fun showPopupWindowwLogOut(view: View) {
        val inflater = view.context.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView: View = inflater.inflate(R.layout.alert_logout, null)
        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = false

        val btn_yes_logout: Button = popupView.findViewById(R.id.btn_yes_logout) as Button
        val btn_no_logout: Button = popupView.findViewById(R.id.btn_no_logout) as Button

        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(view, Gravity.CENTER, width, height)

        dimBehind(popupWindow)

        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }

        btn_no_logout.setOnClickListener(View.OnClickListener {
            popupWindow.dismiss()
        })

        btn_yes_logout.setOnClickListener(View.OnClickListener {
            popupWindow.dismiss()
            logoutUser()
        })
    }

    private fun logoutUser() {
        SessionManager(this).logoutUser()
    }

    @SuppressLint("ResourceType")
    private fun setupBottomNavigation() {

        binding.bnMenu.itemIconTintList = null
        supportFragmentManager.beginTransaction().replace(R.id.frameContainer, HomeFragment())
            .commit()
        binding.bnMenu.setOnNavigationItemSelectedListener { item ->
            var fm: Fragment? = null
            when (item.itemId) {
                R.id.mhome -> fm = HomeFragment()
                R.id.mDailyChallenge -> fm = DailyChallangeFragment()
                R.id.mMoveMent -> fm = MoveMentFragment()
                R.id.mChat -> fm = ChatListFragment()
                R.id.mProfile -> fm = MyProfileFragment()

            }

            supportFragmentManager.beginTransaction().replace(R.id.frameContainer, fm!!).commit()
            true
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return true
    }

    override fun onBackPressed() {

        val frag: Fragment = supportFragmentManager.findFragmentById(R.id.frameContainer)!!
        if (this.binding.drawerlayout.isDrawerOpen(GravityCompat.START)) {
            this.binding.drawerlayout.closeDrawer(GravityCompat.START)
        } else if (frag is HomeFragment) {
            showPopupWindow()
        } else {
            replaceFragment(HomeFragment())
            binding.bnMenu.selectedItemId = R.id.mhome
        }
    }

    private fun showPopupWindow() {
        val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView: View = inflater.inflate(R.layout.alert_exit, null)
        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = false
        var btnYes: Button = popupView.findViewById(R.id.btnYes) as Button
        var btnNo: Button = popupView.findViewById(R.id.btnNo) as Button

        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(popupView, Gravity.CENTER, width, height)
        AppUtils.dimBehind(popupWindow)

        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }

        btnNo.setOnClickListener(View.OnClickListener {
            popupWindow.dismiss()
        })

        btnYes.setOnClickListener(View.OnClickListener {
            finishAffinity()
        })
    }
}