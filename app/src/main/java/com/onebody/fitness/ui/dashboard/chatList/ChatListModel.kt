package com.onebody.fitness.ui.dashboard.chatList

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ChatListModel : Serializable {


    @SerializedName("data")
    var data: MutableList<Data>? = null

    class Data : Serializable {

        @SerializedName("chatid")
        var chatid: Int = 0

        @SerializedName("userid")
        var userid: Int ? = 0

        @SerializedName("name")
        var name: String? = null

        @SerializedName("email")
        var email: String? = null

        @SerializedName("gender")
        var gender: String? = null

        @SerializedName("prof_pic")
        var prof_pic: String? = null

        @SerializedName("lastmsg")
        var lastmsg: String? = null

        @SerializedName("totalcount")
        var totalcount: String? = null

      /*  fun getCreateBy(): Any {
            val createdBy:String = ""
            return createdBy
        }*/

    }
}