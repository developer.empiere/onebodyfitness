package com.onebody.fitness.ui.dashboard.facetype.male

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.facetype.models.FacePropertyModel
import com.onebody.fitness.ui.other.MyConstants

class MaleFacePropertyAdapter(private val context: Context?, val faceTypeModels: ArrayList<FacePropertyModel>?, val faceProperty: MyConstants.Companion.FACE_PROPERTY
) : RecyclerView.Adapter<MaleFacePropertyAdapter.ViewHolder>() {

    lateinit var view : View
    override fun setHasStableIds(hasStableIds: Boolean) {
        super.setHasStableIds(true)
    }
    override fun onCreateViewHolder(parent: ViewGroup,viewType: Int): ViewHolder {

        if(faceProperty == MyConstants.Companion.FACE_PROPERTY.EYECOLOR){
            view = LayoutInflater.from(parent.context).inflate(R.layout.row_eye_color, parent, false)
        } else {
            view = LayoutInflater.from(parent.context).inflate(R.layout.row_face_property, parent, false)
        }

        return ViewHolder(view)
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val faceModel = faceTypeModels!!.get(position)
        holder.facePropertyImage.setImageDrawable(context!!.resources.getDrawable(faceModel.imgID))

        if(faceProperty == MyConstants.Companion.FACE_PROPERTY.EYECOLOR){

            holder.facePropertyImage.requestLayout()
        }

        if (faceModel.isSelected) {
            holder.facePropertyImage.setBackgroundResource(R.drawable.d_orange_strok)
        } else {
            holder.facePropertyImage.setBackgroundResource(0)
        }
        holder.facePropertyImage.setOnClickListener(View.OnClickListener {
           selectItem(position)
        })
    }
    @SuppressLint("NotifyDataSetChanged")
    fun selectItem(pos: Int) {
        for (model in faceTypeModels!!) {
            model.isSelected = false
        }
        faceTypeModels.get(pos).isSelected = true
        //(context as MaleFaceTypeActivity).updateClickData(pos, this.faceProperty)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return faceTypeModels!!.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        var facePropertyImage: ImageView = itemView.findViewById(R.id.imgFaceProperty)
    }
}