package com.onebody.fitness.ui.dashboard.howitworks

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class HowItWorkModel {

    @SerializedName("data")
    var data: MutableList<Data>? = null

    class Data : Serializable {

        @SerializedName("title")
        var title: String? = null

        @SerializedName("description")
        var description: String? = null

    }
}