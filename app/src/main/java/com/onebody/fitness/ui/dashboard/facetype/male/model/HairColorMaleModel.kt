package com.onebody.fitness.ui.dashboard.facetype.male.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class HairColorMaleModel {
    @SerializedName("data")
    var data: Data? = null

    class Data : Serializable {

        @SerializedName("id")
        var id: Int = 0

        @SerializedName("hair")
        var hair: String? = null

    }
}