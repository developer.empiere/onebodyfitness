package com.onebody.fitness.ui.dashboard.chatList

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.bumptech.glide.Glide
import com.makeramen.roundedimageview.RoundedImageView
import com.onebody.fitness.R
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.chatdetail.ChatDetailActivity
import org.json.JSONObject

class ChatListAdapter(
    private val context: Context?,
    var chatListModel: MutableList<ChatListModel.Data>?
) : RecyclerView.Adapter<ChatListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_chat_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val cm = chatListModel?.get(position)

        holder.tvChatName.text = cm!!.name

        if (cm.prof_pic != null) {
            Glide.with(context!!)
                .load(cm.prof_pic)
                .placeholder(R.drawable.ic_user_profile)
                .into(holder.imgProfile)
        }

        if (cm.totalcount.toString() == "") {
            holder.btnChatNotification.visibility = View.GONE
        } else {
            holder.btnChatNotification.text = cm!!.totalcount.toString()
        }
        holder.tvChatLastMessage.text = cm!!.lastmsg

        holder.rlChat.setOnClickListener(View.OnClickListener {
            chatCountUpdate(cm.userid, cm.chatid, cm.prof_pic, cm.name)
        })
    }

    override fun getItemCount(): Int {
        return chatListModel?.size!!
    }

    fun updateList(list: MutableList<ChatListModel.Data>) {
        chatListModel = list
        notifyDataSetChanged()
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val rlChat: RelativeLayout = itemView.findViewById(R.id.rlChat)
        val imgProfile: RoundedImageView = itemView.findViewById(R.id.imgProfile)
        val tvChatName: TextView = itemView.findViewById(R.id.tvChatName)
        val btnChatNotification: TextView = itemView.findViewById(R.id.btnChatNotification)
        val tvChatLastMessage: TextView = itemView.findViewById(R.id.tvChatLastMessage)
    }

    private fun chatCountUpdate(userid: Int?, chatid: Int, profPic: String?, name: String?) {
        val map = HashMap<String, String>()
        map["user_id"] = userid.toString()

        ApiServiceProvider.getInstance(context)
            .sendHashmapPostDataSearch(
                Constants.UrlPath.CHAT_COUNTUPDATE, map, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")

                        if (status) {
                            val intent = Intent(context, ChatDetailActivity::class.java)
                            intent.putExtra("id", "" + chatid)
                            intent.putExtra("name", "" + name)
                            intent.putExtra("profile_pic", "" + profPic)
                            context!!.startActivity(intent)
                            Animatoo.animateSlideLeft(context)
                        } else {
                            Toast.makeText(context, "" + message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }
}