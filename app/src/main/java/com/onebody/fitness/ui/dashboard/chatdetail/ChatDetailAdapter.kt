package com.onebody.fitness.ui.dashboard.chatdetail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.onebody.fitness.retrofit.SessionManager
import com.onebody.fitness.R
import com.onebody.fitness.retrofit.AppUtils
import java.text.SimpleDateFormat
import java.util.*

class ChatDetailAdapter(val name : String,
                        private val context: Context?,
                        val chatDetailModel: MutableList<ChatDetailModel.Data>?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mSessionManager = SessionManager(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        return when (viewType) {
            1 -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_chat_sender, parent, false)
                return SentMessageHolder(view)
            }
            2 -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_chat_receve, parent, false)
                return ReceivedMessageHolder(view)
            }
            else -> { 
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_chat_sender, parent, false)
                return SentMessageHolder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is SentMessageHolder) {
            holder.messageText.text = chatDetailModel!![position].message
            val time = AppUtils.ConvertChatingTime(chatDetailModel!![position].timestamp!!)
            holder.tvMsgTime.text = time
            val date = AppUtils.ConvertChatingDate(chatDetailModel!![position].timestamp!!)

            val c = Calendar.getInstance().time
            val df = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
            val formattedDate = df.format(c)

            if (date == formattedDate) {
                holder.tvToday.text = " " + context!!.resources.getString(R.string.todays)
            } else {
                holder.tvToday.text = " " + date
            }

        } else if (holder is ReceivedMessageHolder) {
            holder.messageText.text = chatDetailModel!![position].message
            val time = AppUtils.ConvertChatingTime(chatDetailModel!![position].timestamp!!)
            holder.tvMsgTime.text = time
            holder.tvName.text = name
        }
    }


    override fun getItemCount(): Int {
        return chatDetailModel?.size!!
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            chatDetailModel!![position].sender_userid!!.equals(
                mSessionManager.getData(
                    SessionManager.USER_ID
                )
            ) -> {
                1
            }
            chatDetailModel!![position].reciever_userid!!.equals(
                mSessionManager.getData(
                    SessionManager.USER_ID
                )
            ) -> {
                2
            }
            else -> {
                1
            }
        }
    }

    class ReceivedMessageHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var messageText: TextView
        var tvMsgTime: TextView
        var tvName: TextView

        init {
            messageText = itemView.findViewById(R.id.tvChatMsg)
            tvMsgTime = itemView.findViewById(R.id.tvChatTime)
            tvName = itemView.findViewById(R.id.tvRecName)
        }
    }

    class SentMessageHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var messageText: TextView
        var tvMsgTime: TextView
        var tvToday: TextView

        init {
            messageText = itemView.findViewById(R.id.tvChatMsg)
            tvMsgTime = itemView.findViewById(R.id.tvTime)
            tvToday = itemView.findViewById(R.id.tvToday)
        }
    }
}