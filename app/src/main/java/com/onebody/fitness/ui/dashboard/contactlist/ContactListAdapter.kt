package com.onebody.fitness.ui.dashboard.contactlist

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.makeramen.roundedimageview.RoundedImageView
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.chatdetail.ChatDetailActivity


class ContactListAdapter(
    private val context: Context?,
    var contactListModel: MutableList<ContactListModel.Data>?
) : RecyclerView.Adapter<ContactListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_contact_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cm = contactListModel?.get(position)

        holder.txtContctName.text = cm!!.name

        if (cm.prof_pic != null) {
            Glide.with(context!!)
                .load(cm.prof_pic)
                .placeholder(R.drawable.ic_user_profile)
                .into(holder.imgContactListprofile)
        }

        holder.rlChat.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, ChatDetailActivity::class.java)
            intent.putExtra("id", "" + cm.id)
            intent.putExtra("name", "" + cm.name)
            intent.putExtra("profile_pic", "" + cm.prof_pic)
            context!!.startActivity(intent)
            (context as Activity).finish()
        })
    }

    override fun getItemCount(): Int {
        return contactListModel?.size!!
    }

    fun updateList(list: MutableList<ContactListModel.Data>) {
        contactListModel = list
        notifyDataSetChanged()
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val rlChat: RelativeLayout = itemView.findViewById(R.id.rlChat)
        val txtContctName: TextView = itemView.findViewById(R.id.txtContctName)
        val imgContactListprofile: RoundedImageView =
            itemView.findViewById(R.id.imgContactListprofile)
    }
}