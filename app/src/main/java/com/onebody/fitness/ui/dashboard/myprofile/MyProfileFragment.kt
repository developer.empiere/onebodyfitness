package com.onebody.fitness.ui.dashboard.myprofile

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.bumptech.glide.Glide
import com.onebody.fitness.R
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.retrofit.SessionManager
import com.onebody.fitness.ui.dashboard.DashBoardActivity
import com.onebody.fitness.ui.dashboard.ImagePickerActivity
import com.onebody.fitness.ui.dashboard.bodytype.BodyTypeFemaleeActivity
import com.onebody.fitness.ui.dashboard.bodytype.BodyTypeMaleActivity
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.fragment_my_profile.*
import kotlinx.android.synthetic.main.fragment_my_profile.view.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.File

class MyProfileFragment : Fragment() {

    lateinit var mRootView: View
    private lateinit var adapter: ArrayAdapter<*>
    private val genderModel: ArrayList<ProfileModel> = ArrayList()

    // var selectText = "Select Gender"
    var uri: Uri? = null

    //    private var avatarCreated: Boolean = true
    var profilePath = ""
    var profileFile: File? = null
    var MultiPart: MultipartBody.Part? = null
    var avatarStatus: String = ""

    private lateinit var mSessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }

        mSessionManager = SessionManager(context)

        avatarStatus = mSessionManager.getData(SessionManager.AVATAR_STATUS)!!

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mRootView = inflater.inflate(R.layout.fragment_my_profile, container, false)

        mRootView.imgSelectProfile.setOnClickListener(View.OnClickListener {
            showImagePickerChooser()
        })

        getUserProfile()
        // setSpinnerAdapter()

        mRootView.btnSave.setOnClickListener {
            updateProfile()
        }
        /* if (avatarStatus.equals("1")){
             mRootView.btnCreateAvtar.setText("Update Your Avatar")
         }
             else{
             mRootView.btnCreateAvtar.setText("Create Your Avatar")

         }*/
        mRootView.btnUpdateAvatar.setOnClickListener {
            //   mSessionManager.getBooleanData(SessionManager.IS_MALE,true)

            if (mSessionManager.getBooleanData(SessionManager.IS_MALE, true)) {
                val intent = Intent(context, BodyTypeMaleActivity::class.java)
                startActivity(intent)
                Animatoo.animateSlideLeft(context)

            } else {
                val intent = Intent(context, BodyTypeFemaleeActivity::class.java)
                startActivity(intent)
                Animatoo.animateSlideLeft(context)
            }
        }

        return mRootView
    }

    private fun getUserProfile() {

        val map = HashMap<String, String>()

        ApiServiceProvider.getInstance(context).sendHashmapPostData(
            Constants.UrlPath.SET_USER_PROFILE, map, true,
            RetrofitListener { response ->
                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")
                    val message = responseObj.getString("message")

                    if (status) {

                        val data = responseObj.getJSONArray("data")
                        val jsonObjct = data.getJSONObject(0)

                        mRootView.txtName.text = jsonObjct.getString("name")
                        mRootView.edtAge.setText(jsonObjct.getString("age"))
                        mRootView.edtWeight.setText(jsonObjct.getString("weight"))
                        mRootView.edtHeight.setText(jsonObjct.getString("height"))
                        mRootView.txtRole.text = jsonObjct.getString("role")
                        mRootView.txtLevel.text = jsonObjct.getString("level")
                        mRootView.tvGender.text = jsonObjct.getString("gender")

                        Glide.with(requireActivity())
                            .load(jsonObjct.getString("prof_pic"))
                            .into(mRootView.imgProfile)


                        Glide.with(requireActivity())
                            .load(jsonObjct.getString("avtarurl"))
                            .into(mRootView.imgAvatar)
                    } else {
                        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    private fun updateProfile() {

        val map = HashMap<String, String>()
        map.put("Name", mRootView.txtName.text.toString())
        map.put("Gender", "" + mRootView.tvGender.text.toString())
        map.put("Age", mRootView.edtAge.text.toString())
        map.put("Height", mRootView.edtHeight.text.toString())
        map.put("Weight", mRootView.edtWeight.text.toString())

        if (profileFile != null) {

            MultiPart = MultipartBody.Part.createFormData(
                "ProfilePic",
                profileFile!!.name,
                RequestBody.create("multipart/form-data".toMediaTypeOrNull(), profileFile!!)
            )
        }
        ApiServiceProvider.getInstance(context).updateProfile(
            Constants.UrlPath.UPDATE_USER_PROFILE, map, MultiPart, true,
            RetrofitListener { response ->

                Toast.makeText(context, "Profile Updated successfully", Toast.LENGTH_SHORT).show()

                (context as DashBoardActivity).replaceFragment(MyProfileFragment())

                /*    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")
                        if (status) {

                            val data = responseObj.getJSONArray("data")
                            val jsonObjct = data.getJSONObject(0)
                            Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show()
                            mRootView.txtName.text = jsonObjct.getString("name")
                            mRootView.edtAge.setText(jsonObjct.getString("age"))
                            mRootView.edtWeight.setText(jsonObjct.getString("weight"))
                            mRootView.edtHeight.setText(jsonObjct.getString("height"))
                            mRootView.txtRole.text = jsonObjct.getString("role")
                            mRootView.txtLevel.text = jsonObjct.getString("level")
                            mRootView.tvGender.text = jsonObjct.getString("gender")

                            Glide.with(requireActivity())
                                .load(jsonObjct.getString("prof_pic"))
                                .into(mRootView.imgProfile)

                            Glide.with(requireActivity())
                                .load(jsonObjct.getString("avtarurl"))
                                .into(mRootView.imgAvatar)

                            mSessionManager.setData(
                                SessionManager.AUTH_TOKEN,
                                responseObj.getString("token")
                            )
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }*/
            })
    }

    private fun showImagePickerChooser() {
        ImagePickerActivity.showImagePickerOptions(
            activity,
            object : ImagePickerActivity.PickerOptionListener {
                override fun onTakeCameraSelected() {
                    launchCameraIntent()
                }

                override fun onChooseGallerySelected() {
                    launchGalleryIntent()
                }
            })
    }

    private fun launchCameraIntent() {
        val intent = Intent(activity, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_IMAGE_CAPTURE
        )
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000)
        startActivityForResult(intent, Constants.REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(activity, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_GALLERY_IMAGE
        )
        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, Constants.REQUEST_IMAGE)
    }

/*    @RequiresApi(Build.VERSION_CODES.O)
    private fun setSpinnerAdapter() {
        adapter = ArrayAdapter(
            requireActivity(),
            R.layout.spinnerlayout,
            resources.getStringArray(R.array.gender_array)
        )
        mRootView.spinnerGender.prompt = selectText

        mRootView.spinnerGender.adapter = adapter

        mRootView.spinnerGender.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
                ) {
                    val selectedText = resources.getStringArray(R.array.gender_array)[position]

                    if (position == 0) {
                        imgAvatar.setImageResource(R.drawable.ic_male)

                    } else {
                        imgAvatar.setImageResource(R.drawable.ic_female)
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {

                }
            }
        setGenderTypeData(resources.getStringArray(R.array.gender_array)[0])
    }
    private fun setGenderTypeData(genderType: String?) {
        genderModel.clear()
    }*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.REQUEST_IMAGE) {
            if (resultCode == RESULT_OK) {
                uri = data!!.getParcelableExtra<Uri>("path")
                profilePath = uri!!.getPath().toString()
                profileFile = File(profilePath)
                imgProfile.setImageURI(uri)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Constants.CAMERA -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(requireContext(), this)
            } else {
                Toast.makeText(
                    context,
                    "Storage and Camera permission is required for " + "upload profile image.",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

/*    override fun onResume() {
        super.onResume()
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);

    }*/
}
