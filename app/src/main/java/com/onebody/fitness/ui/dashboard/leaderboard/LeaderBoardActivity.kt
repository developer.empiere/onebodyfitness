package com.onebody.fitness.ui.dashboard.leaderboard

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityLeaderBoardBinding
import com.onebody.fitness.retrofit.*
import com.onebody.fitness.ui.dashboard.dailychallange.DailyChallengeActivity
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.activity_daily_challenge_detail.*
import kotlinx.android.synthetic.main.activity_leader_board.*
import kotlinx.android.synthetic.main.popup_goalarchivedworkout.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONArray
import org.json.JSONObject


class LeaderBoardActivity : AppCompatActivity() {
    lateinit var binding: ActivityLeaderBoardBinding
    private var leaderBoardAdapter: LeaderBoardAdapter? = null
    private var layoutManager: LinearLayoutManager? = null
    private var leaderBoardModel: MutableList<LeaderBoardModel.Data>? = null
    private lateinit var mSessionManager: SessionManager
    var videoId: String = ""
    lateinit var goalId: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLeaderBoardBinding.inflate(layoutInflater)
        setContentView(binding.root)


        mSessionManager = SessionManager(applicationContext)
        videoId = mSessionManager.getData(SessionManager.VIDEOID)!!

        if (videoId.equals("")) {
            binding.btnSetGoal.visibility = View.GONE
        } else {
            binding.btnSetGoal.visibility = View.VISIBLE
            binding.btnSetGoal.setOnClickListener {
                showPopupSubmitWorkOut(this.btnSetGoal)
            }
        }
        getMyLeaderBoardList()

        ivPrevious.setOnClickListener(View.OnClickListener {
            Animatoo.animateSlideRight(this)
            onBackPressed()
        })
        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        })
    }

    private fun showPopupSubmitWorkOut(view: View) {
        val inflater = view.context.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView: View = inflater.inflate(R.layout.popup_goalarchivedworkout, null)
        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = false

        val btnSubmitGoal: Button = popupView.findViewById(R.id.btnSubmitGoal) as Button
        goalId = popupView.findViewById(R.id.edtGoal) as EditText

        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(view, Gravity.CENTER, width, height)

        AppUtils.dimBehind(popupWindow)

        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }

        btnSubmitGoal.setOnClickListener(View.OnClickListener {
            //mSessionManager.setData(SessionManager.TIME, mTime)
            if (checkValidation()) {
                goalArchivedWorkOut()
                popupWindow.dismiss()
            }
        })
    }

    private fun goalArchivedWorkOut() {
        val requestData = JsonObject()
        requestData.addProperty("videoid", videoId)
        requestData.addProperty("comments", goalId.text.toString())

        ApiServiceProvider.getInstance(this)
            .sendPostData(
                Constants.UrlPath.GOALARCHIVEDWORKOUT, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")

                        if (status) {
                            Toast.makeText(
                                this,
                                "" + message,
                                Toast.LENGTH_SHORT
                            ).show()
                            val intent = Intent(this, DailyChallengeActivity::class.java)
                            intent.putExtra("Goal", "" + goalId.text.toString())
                            startActivity(intent)
                            Animatoo.animateSlideLeft(this)
                            finish()

                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun checkValidation(): Boolean {
        if (AppUtils.isEditTextEmpty(goalId)) {
            goalId.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_your_goal))
            return false
        }
        return true
    }

    private fun getMyLeaderBoardList() {
        val requestData = JsonObject()
        ApiServiceProvider.getInstance(this)
            .sendPostData(
                Constants.UrlPath.GET_MY_LEADER_BOARD_LIST, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")
                        if (status) {
                            val data = responseObj.getJSONArray("data")
                            leaderBoardModel = Gson().fromJson<Any>(
                                data.toString(),
                                object : TypeToken<MutableList<LeaderBoardModel.Data>?>() {}.type
                            )
                                    as MutableList<LeaderBoardModel.Data>
                            object : TypeToken<MutableList<LeaderBoardModel.Data?>?>() {}.type
                            setAdapterMyLeaderBoardList()

                            if (responseObj.has("my_position")) {
                                val my_position = responseObj.getJSONArray("my_position")

                                for (i in 0 until my_position.length()) {
                                    val a = my_position.getJSONObject(i)
                                    val username = a.getString("username")
                                    val profile = a.getString("profile")
                                    val completedPercentage = a.getString("completed_percentage")
                                    val position = a.getString("position")
                                    // binding.tvYourPositon.text = "Your Positon "+position
                                    binding.tvCompletedPercentage.text = position
                                    Glide.with(this)
                                        .load(profile)
                                        .into(binding.profile)
                                    binding.tvUserName.text = username
                                }
                            } else {
                                binding.llLeft.visibility = View.GONE
                                binding.tvYourPositon.visibility = View.GONE
                            }
                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun setAdapterMyLeaderBoardList() {
        //  leaderBoardModel!!.sortBy { it.completed_percentage }
        val size = leaderBoardModel!!.size
        if (size != null) {
            Glide.with(this)
                .load(leaderBoardModel!!.get(0).profile)
                .into(binding.imgOne)
            Glide.with(this)
                .load(leaderBoardModel!!.get(1).profile)
                .into(binding.imgTwo)
            Glide.with(this)
                .load(leaderBoardModel!!.get(2).profile)
                .into(binding.imgThree)
        } else {
            Glide.with(this)
                .load(R.drawable.ic_user_profile)
                .into(binding.imgOne)
            Glide.with(this)
                .load(R.drawable.ic_user_profile)
                .into(binding.imgTwo)
            Glide.with(this)
                .load(R.drawable.ic_user_profile)
                .into(binding.imgThree)
        }
        leaderBoardModel!!.removeAt(0)
        leaderBoardModel!!.removeAt(0)
        leaderBoardModel!!.removeAt(0)
        leaderBoardAdapter = LeaderBoardAdapter(this, leaderBoardModel)
        layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.recyclrLeaderBoard.layoutManager = layoutManager
        binding.recyclrLeaderBoard.adapter = leaderBoardAdapter
    }

    override fun onBackPressed() {
        finish()
    }
}