package com.onebody.fitness.ui.dashboard.facetype.female.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class EyeColorFemaleModel {
    @SerializedName("data")
    var data: Data? = null

    class Data : Serializable {

        @SerializedName("id")
        var id: Int = 0

        @SerializedName("eye")
        var eye: String? = null


    }
}