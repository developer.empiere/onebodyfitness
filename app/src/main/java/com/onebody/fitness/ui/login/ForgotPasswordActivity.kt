package com.onebody.fitness.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.Toast
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.gson.JsonObject
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityForgotPasswordBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.AppUtils
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.PsDialogs
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import com.onebody.fitness.ui.other.AsteriskPasswordTransformationMethod
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject

class ForgotPasswordActivity : AppCompatActivity() {

    lateinit var binding: ActivityForgotPasswordBinding
    var isShowText = true
    var email: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.edtNewPassword.setTransformationMethod(AsteriskPasswordTransformationMethod())
        binding.edtConfPassword.setTransformationMethod(AsteriskPasswordTransformationMethod())

        email = getIntent().getStringExtra("email").toString()

        btnChangePassword.setOnClickListener(View.OnClickListener {
            if (checkValidation()) {
                ChangePasswordApi()
            }
        })
        ivPrevious.setOnClickListener(View.OnClickListener {
            onBackPressed()
            Animatoo.animateSlideRight(this)
        })
        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        })

        binding.imgNewPasswordOn.setOnClickListener(View.OnClickListener {
            isShowText
            edtNewPassword.setTransformationMethod(PasswordTransformationMethod.getInstance())
            binding.edtNewPassword.setSelection(binding.edtNewPassword.length())
            binding.imgNewPasswordOn.visibility = View.GONE
            binding.imgNewPasswordOff.visibility = View.VISIBLE
        })

        binding.imgNewPasswordOff.setOnClickListener(View.OnClickListener {
            isShowText = false
            edtNewPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance())
            binding.edtNewPassword.setSelection(binding.edtNewPassword.length())
            binding.imgNewPasswordOff.visibility = View.GONE
            binding.imgNewPasswordOn.visibility = View.VISIBLE
        })

        binding.imgConfPasswordOn.setOnClickListener(View.OnClickListener {
            isShowText
            edtConfPassword.setTransformationMethod(PasswordTransformationMethod.getInstance())
            binding.edtConfPassword.setSelection(binding.edtConfPassword.length())
            binding.imgConfPasswordOn.visibility = View.GONE
            binding.imgConfPasswordOff.visibility = View.VISIBLE
        })

        binding.imgConfPasswordOff.setOnClickListener(View.OnClickListener {
            isShowText = false
            edtConfPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance())
            binding.edtConfPassword.setSelection(binding.edtConfPassword.length())
            binding.imgConfPasswordOff.visibility = View.GONE
            binding.imgConfPasswordOn.visibility = View.VISIBLE
        })

    }

    private fun ChangePasswordApi() {
        val requestData = JsonObject()
        requestData.addProperty("Email", email)
        requestData.addProperty("IsVerifiedOTP", "1")
        requestData.addProperty("NewPassword", edtNewPassword.text.toString())
        requestData.addProperty("ConfirmPassword", edtConfPassword.text.toString())

        ApiServiceProvider.getInstance(this)
            .sendPostData(
                Constants.UrlPath.FORGOT_PSWD, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")
                        if (status) {
                            Animatoo.animateSlideLeft(this)
                            val i = Intent(this, LoginActivity::class.java)
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(i)
                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun checkValidation(): Boolean {

        if (AppUtils.isEditTextEmpty(binding.edtNewPassword)) {
            binding.edtNewPassword.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_new_password))
            return false
        } else if (AppUtils.getEditTextValue(binding.edtNewPassword)!!.length < 3) {
            edtPassword.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_less_pass))
            return false
        }  else if (AppUtils.isEditTextEmpty(binding.edtConfPassword)) {
            binding.edtNewPassword.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_confirm_password))
            return false
        } else if (!binding.edtNewPassword.text.toString()
                .equals(binding.edtConfPassword.text.toString())
        ) {
            binding.edtNewPassword.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_not_same_password))
            return false
        }
        return true
    }
}