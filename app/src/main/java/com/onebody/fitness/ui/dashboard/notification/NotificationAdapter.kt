package com.onebody.fitness.ui.dashboard.notification

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.onebody.fitness.R

class NotificationAdapter(
    private val context: Context?,
    val notificationModel: ArrayList<NotificationModel>?
) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_notification, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (position % 2 == 1) {
            holder.itemView.setBackgroundColor(Color.parseColor("#F8F8F8"));
        } else {
            holder.itemView.setBackgroundColor(Color.parseColor("#FFFFFF"))

        }
    }

    override fun getItemCount(): Int {
        return 5
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        // val rlChat: RelativeLayout = itemView.findViewById(R.id.rlChat)
    }
}