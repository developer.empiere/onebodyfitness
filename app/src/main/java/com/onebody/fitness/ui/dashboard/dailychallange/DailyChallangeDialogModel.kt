package com.onebody.fitness.ui.dashboard.dailychallange

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DailyChallangeDialogModel {

    @SerializedName("data")
    var data: MutableList<Data>? = null

    class Data : Serializable {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("name")
        var name: String? = null

    }
}