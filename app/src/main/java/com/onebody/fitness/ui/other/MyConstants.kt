package com.onebody.fitness.ui.other

class MyConstants {

    val faceId: String = "FaceId"
    val faceStyle: String = "FaceStyle"

    companion object
    {
        enum class FACE_PROPERTY
        {
            SKINCOLOR, HAIRCOLOR, EYECOLOR, FACIALHAIR, CLOTHINGSHORTS, HAIR_STYLE, SHOESCOLOR
        }
    }
}