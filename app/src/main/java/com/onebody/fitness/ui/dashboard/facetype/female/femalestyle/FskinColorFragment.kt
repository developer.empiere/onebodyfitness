package com.onebody.fitness.ui.dashboard.facetype.female.femalestyle

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.facetype.female.FemaleBodyTypeActivity
import com.onebody.fitness.ui.dashboard.facetype.female.FemaleFacePropertyAdapter
import com.onebody.fitness.ui.dashboard.facetype.models.FacePropertyModel
import com.onebody.fitness.ui.other.MyConstants
import kotlinx.android.synthetic.main.fragment_fskin_color.view.*

class FskinColorFragment : Fragment() {

    lateinit var mRootView: View
    private var femaleFacePropertyAdapter: FemaleFacePropertyAdapter? = null
    private var layoutManager: GridLayoutManager? = null
    var facePropertyModels = ArrayList<FacePropertyModel>()

    var selectedSkinColor: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_fskin_color, container, false)
        selectButton(MyConstants.Companion.FACE_PROPERTY.SKINCOLOR)
        mRootView.btnSubmit.setOnClickListener(View.OnClickListener {
            try {
                (requireActivity() as FemaleBodyTypeActivity).moveToNext()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
        return mRootView
    }

    private fun selectButton(faceProperty: MyConstants.Companion.FACE_PROPERTY) {
        if (faceProperty == MyConstants.Companion.FACE_PROPERTY.SKINCOLOR) {
            generateSkinColorArray()
            return
        }
    }


    fun updateClickData(pos: Int, faceProperty: MyConstants.Companion.FACE_PROPERTY) {
        selectedSkinColor = pos
        generateSkinColorArray()
        femaleFacePropertyAdapter!!.notifyDataSetChanged()
    }

    private fun generateSkinColorArray() {
        facePropertyModels.clear()
        facePropertyModels.add(
            FacePropertyModel(
                if (selectedSkinColor == 0) true else false,
                "1",
                R.drawable.ic_gface_one,
                1
            )
        )
        facePropertyModels.add(
            FacePropertyModel(
                if (selectedSkinColor == 1) true else false,
                "2",
                R.drawable.ic_gface_five,
                2
            )
        )
        facePropertyModels.add(
            FacePropertyModel(
                if (selectedSkinColor == 2) true else false,
                "3",
                R.drawable.ic_gface_three,
                3
            )
        )
        facePropertyModels.add(
            FacePropertyModel(
                if (selectedSkinColor == 3) true else false,
                "4",
                R.drawable.ic_gface_four,
                4
            )
        )
        facePropertyModels.add(
            FacePropertyModel(
                if (selectedSkinColor == 4) true else false,
                "5",
                R.drawable.ic_gface_two,
                5
            )
        )
        setAdapter()
    }

    fun setAdapter() {
        //set
        femaleFacePropertyAdapter = FemaleFacePropertyAdapter(
            context,
            facePropertyModels,
            MyConstants.Companion.FACE_PROPERTY.HAIR_STYLE
        )
        femaleFacePropertyAdapter!!.setHasStableIds(true)
        layoutManager = GridLayoutManager(context, 2)
        layoutManager!!.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                val spanSize: Int = if (position + 1 < facePropertyModels.size) 1 else 2
                println("Position -->" + position + " --> " + spanSize)
                return spanSize
            }
        }
        mRootView.rvSkinColor.layoutManager = layoutManager
        mRootView.rvSkinColor.adapter = femaleFacePropertyAdapter
    }


    companion object {

        fun newInstance(param1: String, param2: String) =
            FskinColorFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}