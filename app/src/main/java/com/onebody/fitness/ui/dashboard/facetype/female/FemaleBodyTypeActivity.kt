package com.onebody.fitness.ui.dashboard.facetype.female

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.onebody.fitness.databinding.ActivityFemaleBodyTypeBinding

class FemaleBodyTypeActivity : AppCompatActivity() {

    lateinit var binding: ActivityFemaleBodyTypeBinding
    private var viewPagerAdapter: FemalePagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityFemaleBodyTypeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setViewPager()
    }

    private fun setViewPager() {

        viewPagerAdapter = FemalePagerAdapter(this.supportFragmentManager, 8)
        binding.viewPager.adapter = viewPagerAdapter
    }

    fun moveToNext() {
        if (binding.viewPager.currentItem < viewPagerAdapter!!.count) binding.viewPager.currentItem =
            binding.viewPager.currentItem + 1
    }

    fun moveToPos(pos: Int) {
        if (binding.viewPager.currentItem < viewPagerAdapter!!.count)
            binding.viewPager.currentItem = pos
    }

    fun moveToPrevious() {
        if (binding.viewPager.currentItem < viewPagerAdapter!!.count) binding.viewPager.currentItem =
            binding.viewPager.currentItem - 1
    }
}
