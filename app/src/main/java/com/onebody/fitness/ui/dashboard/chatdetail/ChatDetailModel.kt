package com.onebody.fitness.ui.dashboard.chatdetail

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ChatDetailModel : Serializable{

    @SerializedName("data")
    var data: MutableList<Data>? = null

    class Data : Serializable {

        @SerializedName("id")
        var id: Int = 0

        @SerializedName("sender_userid")
        var sender_userid: Int ? = 0

        @SerializedName("reciever_userid")
        var reciever_userid: String? = null

        @SerializedName("message")
        var message: String? = null

        @SerializedName("timestamp")
        var timestamp: String? = null

    }
}