package com.onebody.fitness.ui.dashboard.home

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.onebody.fitness.retrofit.SessionManager
import com.onebody.fitness.R
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.Calories.CaloriesActivity
import com.onebody.fitness.ui.dashboard.dailychallange.ArchivedWorkOutAdapter
import com.onebody.fitness.ui.dashboard.dailychallange.ArchivedWorkOutModel
import com.onebody.fitness.ui.dashboard.dailychallange.DailyChallengeModel
import com.onebody.fitness.ui.dashboard.homemore.HomeMoreStationActivity
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.home_progress.view.*
import org.json.JSONObject
import java.util.concurrent.TimeUnit


class HomeFragment : Fragment() {

    lateinit var mRootView: View
    private var marketSliderAdapter: HomeAdapter? = null
    private var layoutManager: LinearLayoutManager? = null
    private var archivedWorkOutAdapter: ArchivedWorkOutAdapter? = null
    private var archivedWorkOutModel: MutableList<ArchivedWorkOutModel.Data>? = null
    private var dailyChallengeList: ArrayList<DailyChallengeModel>? = null
    private lateinit var mSessionManager: SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
        mSessionManager = SessionManager(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mRootView = inflater.inflate(R.layout.fragment_home, container, false)

        //getMyArchivedWorkouts()


        getMyProgress()

        /*   mRootView.txtSeeMoreStation.setOnClickListener(View.OnClickListener {
               val intent = Intent(context, HomeMoreStationActivity::class.java)
               startActivity(intent)
               Animatoo.animateSlideLeft(context)
           })*/

        mRootView.txtSeeMore.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, CaloriesActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(context)
        })

        return mRootView
    }

/*    private fun getMyArchivedWorkouts() {
        var jsonObject = JsonObject()

        ApiServiceProvider.getInstance(context).sendPostData(
            Constants.UrlPath.MY_ARCHIVED_WORKOUTS, jsonObject, true,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")

                    if (status) {
                        val data = responsObj.getJSONArray("data")
                        archivedWorkOutModel = Gson().fromJson<Any>(
                            data.toString(),
                            object : TypeToken<MutableList<ArchivedWorkOutModel.Data>?>() {}.type
                        ) as MutableList<ArchivedWorkOutModel.Data>
                        object : TypeToken<MutableList<ArchivedWorkOutModel.Data?>?>() {}.type
                        setStationAdapter()
                    }
                } catch (e: Exception) {

                    e.printStackTrace()
                }
            })
    }*/

    @SuppressLint("SetTextI18n")
    private fun getMyProgress() {
        var jsonObject = JsonObject()

        ApiServiceProvider.getInstance(context).sendPostData(
            Constants.UrlPath.GET_MY_PROGRESS, jsonObject, true,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")
                    val message = responsObj.getString("message")

                    if (status) {
                        val data = responsObj.getJSONArray("data")
                        val size = data.length()
                        if (size == 0) {
                            mRootView.layoutProgress.visibility = View.GONE

                        } else if (size == 1) {

                            mRootView.layoutProgress.rlexe2.visibility = View.GONE
                            mRootView.layoutProgress.rlexe3.visibility = View.GONE
                            val jsonObjectone = data.getJSONObject(0)

                            mRootView.layoutProgress.tvFirstProgressName.text =
                                jsonObjectone.getString("exc_counts")

                            mRootView.layoutProgress.tvFirstPer.text =
                                jsonObjectone.getString("completed_percentage") + "%"

                            val totalMinutes: String =
                                jsonObjectone.getString("workoutminutes")

                            mRootView.layoutProgress.tvFirstProgressMinute.text = totalMinutes


                        } else if (size == 2) {

                            mRootView.layoutProgress.rlexe3.visibility = View.GONE
                            val jsonObjectone = data.getJSONObject(0)
                            mRootView.layoutProgress.tvFirstProgressName.text =
                                jsonObjectone.getString("exc_counts")
                            mRootView.layoutProgress.tvFirstPer.text =
                                jsonObjectone.getString("completed_percentage") + "%"
                            val totalMinutes: String =
                                jsonObjectone.getString("workoutminutes")
                            mRootView.layoutProgress.tvFirstProgressMinute.text =
                                totalMinutes


                            val jsonObjectTwo = data.getJSONObject(1)
                            mRootView.layoutProgress.tvSecondProgressName.text =
                                jsonObjectTwo.getString("exc_counts")
                            mRootView.layoutProgress.tvSecondPer.text =
                                jsonObjectTwo.getString("completed_percentage") + "%"
                            val totalMinutesSecond: String =
                                jsonObjectTwo.getString("workoutminutes")
                            mRootView.layoutProgress.tvSecondProgressMinute.text =
                                totalMinutesSecond


                        } else if (size == 3) {

                            val jsonObjectone = data.getJSONObject(0)
                            mRootView.layoutProgress.tvFirstProgressName.text =
                                jsonObjectone.getString("exc_counts")
                            println("Counts:-" + jsonObjectone.getString("exc_counts"))

                            val totalMinutes: String =
                                jsonObjectone.getString("workoutminutes")
                            mRootView.layoutProgress.tvFirstProgressMinute.text = totalMinutes
                            mRootView.layoutProgress.tvFirstPer.text =
                                jsonObjectone.getString("completed_percentage") + "%"


                            val jsonObjectTwo = data.getJSONObject(1)
                            mRootView.layoutProgress.tvSecondProgressName.text =
                                jsonObjectTwo.getString("exc_counts")
                            val totalMinutesSecond: String =
                                jsonObjectTwo.getString("workoutminutes")
                            mRootView.layoutProgress.tvSecondProgressMinute.text =
                                totalMinutesSecond
                            mRootView.layoutProgress.tvSecondPer.text =
                                jsonObjectTwo.getString("completed_percentage") + "%"


                            val jsonObjectThree = data.getJSONObject(2)
                            mRootView.layoutProgress.tvThirdProgressName.text =
                                jsonObjectThree.getString("exc_counts")
                            val totalMinutesThird: String =
                                jsonObjectThree.getString("workoutminutes")
                            mRootView.layoutProgress.tvThirdProgressMinute.text =
                                totalMinutesThird
                            mRootView.layoutProgress.tvThirdPer.text =
                                jsonObjectThree.getString("completed_percentage") + "%"


                        } else if (size > 3) {

                            val jsonObjectone = data.getJSONObject(0)

                            mRootView.layoutProgress.tvFirstProgressName.text =
                                jsonObjectone.getString("exc_counts")

                            val totalMinutes: String =
                                jsonObjectone.getString("workoutminutes")
                            mRootView.layoutProgress.tvFirstProgressMinute.text =
                                totalMinutes

                            mRootView.layoutProgress.tvFirstPer.text =
                                jsonObjectone.getString("completed_percentage") + "%"

                            val jsonObjectTwo = data.getJSONObject(1)
                            mRootView.layoutProgress.tvSecondProgressName.text =
                                jsonObjectTwo.getString("exc_counts")

                            val totalMinutesSecond: String =
                                jsonObjectTwo.getString("workoutminutes")
                            mRootView.layoutProgress.tvSecondProgressMinute.text =
                                totalMinutesSecond
                            mRootView.layoutProgress.tvSecondPer.text =
                                jsonObjectTwo.getString("completed_percentage") + "%"


                            val jsonObjectThree = data.getJSONObject(2)
                            mRootView.layoutProgress.tvThirdProgressName.text =
                                jsonObjectThree.getString("exc_counts")
                            val totalMinutesThird: String =
                                jsonObjectThree.getString("workoutminutes")
                            mRootView.layoutProgress.tvThirdProgressMinute.text =
                                totalMinutesThird
                            mRootView.layoutProgress.tvThirdPer.text =
                                jsonObjectThree.getString("completed_percentage") + "%"
                        }
                    } else {
                        Toast.makeText(context, "" + message, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    fun fromMinutesToHHmm(minutes: Int): String {
        val hours = TimeUnit.MINUTES.toHours(minutes.toLong())
        val remainMinutes = minutes - TimeUnit.HOURS.toMinutes(hours)
        return String.format("%02d : %02d", hours, remainMinutes)
    }

    /*private fun setStationAdapter() {
        marketSliderAdapter = HomeAdapter(context, dailyChallengeList, true)
        layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        mRootView.rvHomeStation!!.layoutManager = layoutManager
        mRootView.rvHomeStation!!.adapter = marketSliderAdapter
    }*/
    /* private fun setStationAdapter() {
         archivedWorkOutAdapter = ArchivedWorkOutAdapter(context, archivedWorkOutModel, true)
         layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
         mRootView.rvHomeStation!!.layoutManager = layoutManager
         mRootView.rvHomeStation!!.adapter = archivedWorkOutAdapter
     }*/
}