package com.onebody.fitness.ui.dashboard.facetype.models

import java.io.Serializable

class FacePropertyModel : Serializable {

    var isSelected: Boolean = false
    var id: String = ""
    var imgID: Int = 0
    var index: Int = 0

    constructor(isSelected: Boolean, id: String, imgID: Int, index: Int) {
        this.isSelected = isSelected
        this.id = id
        this.imgID = imgID
        this.index = index
    }
}