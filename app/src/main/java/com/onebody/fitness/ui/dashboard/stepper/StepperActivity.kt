package com.onebody.fitness.ui.dashboard.stepper

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityProductListBinding
import com.onebody.fitness.databinding.ActivityStepperBinding

class StepperActivity : AppCompatActivity() {
    lateinit var binding: ActivityStepperBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityStepperBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}