package com.onebody.fitness.ui.dashboard.howitworks

import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.media.MediaPlayer
import android.media.ThumbnailUtils
import android.net.Uri
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.VideoView
import androidx.recyclerview.widget.RecyclerView
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.dailychallangedetail.DailyChallengeDetailActivity
import com.onebody.fitness.ui.dashboard.dailychallengelist.DailyChallengeListAdapter
import com.onebody.fitness.ui.dashboard.dailychallengelist.DailyChallengeListModel

class HowItWorkAdapter(
    private val context: Context?,
    var howItWorkModel: MutableList<HowItWorkModel.Data>?
) : RecyclerView.Adapter<HowItWorkAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_how_it_work, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val hiw = howItWorkModel?.get(position)

        holder.tvTitle.text = hiw?.title
        holder.tvDescription.text = hiw?.description

    }

    override fun getItemCount(): Int {
        return howItWorkModel?.size!!
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {

        val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        val tvDescription: TextView = itemView.findViewById(R.id.tvDescription)

    }
}


