package com.onebody.fitness.ui.dashboard.facetype.female.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.facetype.female.FemaleFaceTypeActivity
import com.onebody.fitness.ui.dashboard.facetype.female.model.HairStyleFemaleModel
import kotlinx.android.synthetic.main.row_hair_style_female.view.*

class HairStyleFemaleAdapter(
    private val context: Context?,
    private val hairStyleFemaleModel: MutableList<HairStyleFemaleModel.Data>
) :
    RecyclerView.Adapter<HairStyleFemaleAdapter.ViewHolder>() {
    var selectedPosition = (context as FemaleFaceTypeActivity).selectedHairColor - 1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_hair_style_female, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = hairStyleFemaleModel[position]
        context?.let {
            Glide.with(it)
                .load(hairStyleFemaleModel.get(position).style)
                .error(R.drawable.ic_launcher_background)
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(holder.imgMaleClothShort)
        }

        if (selectedPosition < 0) {
            selectedPosition = 0
        }

        if (selectedPosition == position) {
            holder.cv_Hair_Style_Female.background =
                context!!.resources.getDrawable(R.drawable.red_outside_border)
        } else {
            holder.cv_Hair_Style_Female.background =
                context!!.resources.getDrawable(R.drawable.white_outside_border)
        }

        holder.cv_Hair_Style_Female.setOnClickListener {
            selectedPosition = position
            onItemClickListener?.let {
                it(model.id)
            }
            notifyDataSetChanged()
        }
    }

    private var onItemClickListener: ((Int) -> Unit)? = null

    fun setOnClickListener(listener: (Int) -> Unit) {
        onItemClickListener = listener
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imgMaleClothShort: ImageView = itemView.imgHairStyleFamale
        val cv_Hair_Style_Female = itemView.cv_Hair_Style_Female

    }

    override fun getItemCount(): Int {
        return hairStyleFemaleModel.size
    }
}