package com.onebody.fitness.ui.dashboard.movement

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.media.MediaPlayer
import android.media.ThumbnailUtils
import android.net.Uri
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.VideoView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.SimpleExoPlayer
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.OpenVideoActivity
import java.io.IOException

class MovementAdapter(
    private val context: Context?,
    var moveMentModel: MutableList<MoveMentModel.Data>?
) : RecyclerView.Adapter<MovementAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_movement, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SourceLockedOrientationActivity", "UseRequireInsteadOfGet")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val mmModel = moveMentModel?.get(position)

        holder.tvTitle.text = mmModel?.title
        var video = mmModel?.video

        holder.videoView.setVideoURI(Uri.parse(video))
        holder.videoView.seekTo(1000)
        holder.videoView.setOnCompletionListener(MediaPlayer.OnCompletionListener {
            holder.iconVideoPlay.setVisibility(View.VISIBLE)
            holder.iconVideoPause.setVisibility(View.GONE)
        })
        try {
            val thumb = ThumbnailUtils.createVideoThumbnail(
                video.toString(),
                MediaStore.Images.Thumbnails.MINI_KIND
            )
            val bitmapDrawable = BitmapDrawable(thumb)
            holder.videoView.setBackgroundDrawable(bitmapDrawable)

        } catch (e: IOException) {
            e.printStackTrace()
        }
        holder.iconVideoPlay.setOnClickListener {
            holder.videoView.start()
            holder.iconVideoPlay.setVisibility(View.GONE)
            holder.iconVideoPause.setVisibility(View.VISIBLE)
        }
        holder.iconVideoPause.setOnClickListener {
            holder.videoView.pause()
            holder.iconVideoPlay.setVisibility(View.VISIBLE)
            holder.iconVideoPause.setVisibility(View.GONE)
        }
        holder.btnFullscreen.setOnClickListener {
            val intent = Intent(context, OpenVideoActivity::class.java)
            intent.putExtra("video", "" + mmModel?.video)
            context!!.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return moveMentModel?.size!!
    }

    fun updateList(list: MutableList<MoveMentModel.Data>) {
        moveMentModel = list
        notifyDataSetChanged()
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val videoView: VideoView = itemView.findViewById(R.id.videoView)
        val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        val btnFullscreen: ImageView = itemView.findViewById(R.id.btnFullscreen)
        val iconVideoPlay: ImageView = itemView.findViewById(R.id.iconVideoPlay)
        val iconVideoPause: ImageView = itemView.findViewById(R.id.iconVideoPause)
    }
}


