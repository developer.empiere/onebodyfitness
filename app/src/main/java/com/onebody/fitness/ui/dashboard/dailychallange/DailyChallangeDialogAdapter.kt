package com.onebody.fitness.ui.dashboard.dailychallange

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import com.onebody.fitness.R


class DailyChallangeDialogAdapter(
    private val context: Context?,
    var dailyChallangeDialogModel: MutableList<DailyChallangeDialogModel.Data>?
) : RecyclerView.Adapter<DailyChallangeDialogAdapter.ViewHolder>() {

    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.popup_daily_change_dialog, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        val dcdm = dailyChallangeDialogModel?.get(position)


        holder.rbItem.text = dcdm?.name

        holder.rbItem.setChecked(selectedPosition == position)


        holder.rbItem.setOnClickListener {

            selectedPosition = position
           // var rbItemPosition = dailyChallangeDialogModel!!.get(position).id
            onItemClickListener?.let {
                it(dcdm!!.id!!.toInt())
            }
            notifyDataSetChanged()
        }
    }

    private var onItemClickListener: ((Int) -> Unit)? = null

    fun setOnClickListener(listener: (Int) -> Unit) {
        onItemClickListener = listener
    }

    override fun getItemCount(): Int {

        return dailyChallangeDialogModel?.size!!
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        var rbItem: RadioButton = itemView.findViewById(R.id.rbItem)
    }
}