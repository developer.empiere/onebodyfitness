package com.onebody.fitness.ui.dashboard.facetype.male.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class HairStyleMaleModel {
    @SerializedName("data")
    var data: Data? = null

    class Data : Serializable {

        @SerializedName("id")
        var id: Int = 0

        @SerializedName("style")
        var style: String? = null

    }
}