package com.onebody.fitness.ui.dashboard.bodytype

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.bumptech.glide.Glide
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityBodyTypeMaleBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.facetype.male.MaleFaceTypeActivity
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject

class BodyTypeMaleActivity : AppCompatActivity() {

    lateinit var binding: ActivityBodyTypeMaleBinding
    var bodyType: String = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBodyTypeMaleBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getBodyTypeMale()

        ivPrevious.setOnClickListener(View.OnClickListener {
            Animatoo.animateSlideRight(this)
            onBackPressed()
        })

        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(this)
        })

        binding.txtEcotomorph.setOnClickListener(View.OnClickListener {
            bodyType = "0"
            binding.txtEcotomorph.setBackgroundColor(R.drawable.d_red_17)
            binding.txtEcotomorph.setBackgroundDrawable(resources.getDrawable(R.drawable.d_red_17))
            binding.txtEcotomorph.setTextColor(resources.getColor(R.color.white))

            binding.txtEndomorph.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.txtEndomorph.setTextColor(resources.getColor(R.color.black))
            binding.txtMesomorph.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.txtMesomorph.setTextColor(resources.getColor(R.color.black))
        })
        binding.txtMesomorph.setOnClickListener(View.OnClickListener {
            bodyType = "1"
            binding.txtMesomorph.setBackgroundDrawable(resources.getDrawable(R.drawable.d_red_17))
            binding.txtMesomorph.setTextColor(resources.getColor(R.color.white))

            binding.txtEcotomorph.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.txtEcotomorph.setTextColor(resources.getColor(R.color.black))
            binding.txtEndomorph.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.txtEndomorph.setTextColor(resources.getColor(R.color.black))
        })
        binding.txtEndomorph.setOnClickListener(View.OnClickListener {
            bodyType = "2"
            binding.txtEndomorph.setBackgroundDrawable(resources.getDrawable(R.drawable.d_red_17))
            binding.txtEndomorph.setTextColor(resources.getColor(R.color.white))

            binding.txtMesomorph.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.txtMesomorph.setTextColor(resources.getColor(R.color.black))
            binding.txtEcotomorph.setBackgroundDrawable(resources.getDrawable(R.drawable.d_grey_17))
            binding.txtEcotomorph.setTextColor(resources.getColor(R.color.black))
        })

        binding.btnSubmit.setOnClickListener(View.OnClickListener {
            Animatoo.animateSlideLeft(this)
            val intent = Intent(this, MaleFaceTypeActivity::class.java)
            intent.putExtra("BodyType", bodyType)
            startActivity(intent)
        })
    }

    private fun getBodyTypeMale() {
        val map = HashMap<String, String>()

        ApiServiceProvider.getInstance(this).sendHashmapPostData(
            Constants.UrlPath.BODYTYPEMALE, map, true,
            RetrofitListener { response ->

                try {
                    val responseObj = JSONObject(response.body().toString())
                    val status = responseObj.getBoolean("status")

                    if (status) {
                        val data = responseObj.getJSONArray("data")
                        val size = data.length()
                        if (size == 1) {
                            val jsonObjectone = data.getJSONObject(0)
                            Glide.with(this)
                                .load(jsonObjectone.getString("skin"))
                                .into(binding.imgEctomorph)

                        } else if (size == 2) {
                            val jsonObjectone = data.getJSONObject(0)
                            val jsonObjecttwo = data.getJSONObject(1)

                            Glide.with(this)
                                .load(jsonObjectone.getString("skin"))
                                .into(binding.imgEctomorph)

                            Glide.with(this)
                                .load(jsonObjecttwo.getString("skin"))
                                .into(binding.imgMesomorph)
                        } else if(size == 3)
                        {
                            val jsonObjectone = data.getJSONObject(0)
                            val jsonObjecttwo = data.getJSONObject(1)
                            val jsonObjectthree = data.getJSONObject(2)


                            Glide.with(this)
                                .load(jsonObjectone.getString("skin"))
                                .into(binding.imgEctomorph)

                            Glide.with(this)
                                .load(jsonObjecttwo.getString("skin"))
                                .into(binding.imgMesomorph)

                            Glide.with(this)
                                .load(jsonObjectthree.getString("skin"))
                                .into(binding.imgEndomorph)
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            })
    }

    override fun onBackPressed() {
        finish()
    }
}