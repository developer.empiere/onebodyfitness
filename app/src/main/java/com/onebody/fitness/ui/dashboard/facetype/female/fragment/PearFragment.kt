package com.onebody.fitness.ui.dashboard.facetype.female.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.facetype.female.FemaleBodyTypeActivity
import com.onebody.fitness.ui.dashboard.facetype.female.FemaleFaceTypeActivity
import com.onebody.fitness.ui.dashboard.gender.GenderActivity
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.female_body_bottom.view.*
import kotlinx.android.synthetic.main.fragment_pear.view.*
import kotlinx.android.synthetic.main.toolbar.view.*

class PearFragment : Fragment() {

    lateinit var mRootView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        mRootView = inflater.inflate(R.layout.fragment_pear, container, false)

        mRootView.femaleBody.txtBodyAngle.text = "Pear"

        mRootView.femaleBody.ivPreviousBody.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, GenderActivity::class.java)
            startActivity(intent)

        })

        mRootView.femaleBody.ivNextBody.setOnClickListener(View.OnClickListener {
           (activity as FemaleBodyTypeActivity ).moveToNext()

        })

        mRootView.btnSubmit.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, FemaleFaceTypeActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(context)
        })

        mRootView.toolbar.ivPrevious.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, GenderActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideRight(context)

        })
        mRootView.toolbar.ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, NotificationActivity::class.java)
            startActivity(intent)
            Animatoo.animateSlideLeft(context)

        })

        return mRootView
    }

    companion object {
        fun newInstance() =
            PearFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}