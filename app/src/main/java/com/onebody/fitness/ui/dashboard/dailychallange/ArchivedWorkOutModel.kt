package com.onebody.fitness.ui.dashboard.dailychallange

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ArchivedWorkOutModel {
    @SerializedName("data")
    var data: MutableList<Data>? = null

    class Data : Serializable {

        @SerializedName("id")
        var id: String? = null

        @SerializedName("videoid")
        var videoid: Int? = 0

        @SerializedName("station_name")
        var station_name: String? = null

        @SerializedName("date")
        var date: String? = null

        @SerializedName("workoutmInutes")
        var workoutmInutes: String? = null

        @SerializedName("goal_comment")
        var goal_comment: String? = null

    }

}