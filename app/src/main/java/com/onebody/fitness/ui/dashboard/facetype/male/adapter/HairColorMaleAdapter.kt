package com.onebody.fitness.ui.dashboard.facetype.male.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.facetype.male.MaleFaceTypeActivity
import com.onebody.fitness.ui.dashboard.facetype.male.model.HairColorMaleModel
import kotlinx.android.synthetic.main.row_hair_color_male.view.*

class HairColorMaleAdapter(
    private val context: Context?,
    private val hairColorMaleModel: MutableList<HairColorMaleModel.Data>
) :
    RecyclerView.Adapter<HairColorMaleAdapter.ViewHolder>() {

    var selectedPosition = (context as MaleFaceTypeActivity).selectedHairColor - 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_hair_color_male, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = hairColorMaleModel[position]
        context?.let {
            Glide.with(it)
                .load(hairColorMaleModel.get(position).hair)
                .error(R.drawable.ic_launcher_background)
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(holder.imgMaleHairColor)
        }

        if (selectedPosition < 0) {
            selectedPosition = 0
        }

        if (selectedPosition == position) {
            holder.cv_hair_color.background =
                context!!.resources.getDrawable(R.drawable.red_outside_border)
        } else {
            holder.cv_hair_color.background =
                context!!.resources.getDrawable(R.drawable.white_outside_border)
        }

        holder.cv_hair_color.setOnClickListener {
            selectedPosition = position
            onItemClickListener?.let {
                it(selectedPosition + 1)
            }
            notifyDataSetChanged()
        }
    }

    private var onItemClickListener: ((Int) -> Unit)? = null

    fun setOnClickListener(listener: (Int) -> Unit) {
        onItemClickListener = listener
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imgMaleHairColor: ImageView = itemView.imgMaleHairColor
        val cv_hair_color = itemView.cv_hair_color

    }

    override fun getItemCount(): Int {
        return hairColorMaleModel.size
    }
}