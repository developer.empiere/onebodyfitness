package com.onebody.fitness.ui.dashboard.leaderboard

import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.media.MediaPlayer
import android.media.ThumbnailUtils
import android.net.Uri
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.onebody.fitness.R
import com.onebody.fitness.ui.dashboard.dailychallangedetail.DailyChallengeDetailActivity
import com.onebody.fitness.ui.dashboard.dailychallengelist.DailyChallengeListModel

class LeaderBoardAdapter(
    private val context: Context?,
    var LeaderBoardModel: MutableList<LeaderBoardModel.Data>?
) : RecyclerView.Adapter<LeaderBoardAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_leaderboard, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

//
        val cm = LeaderBoardModel?.get(position)

        holder.tvCompletedPercentage.text = (position + 4).toString()

        holder.tvUserName.text = cm?.username

        context?.let {
            Glide.with(it)
                .load(LeaderBoardModel!!.get(position).profile)
                .placeholder(R.drawable.ic_user_profile)
                .into(holder.profile)
        }
    }

    override fun getItemCount(): Int {
        return LeaderBoardModel?.size!!
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {

        val tvCompletedPercentage: TextView = itemView.findViewById(R.id.tvCompletedPercentage)
        val tvUserName: TextView = itemView.findViewById(R.id.tvUserName)
        val profile: ImageView = itemView.findViewById(R.id.profile)

    }
}


