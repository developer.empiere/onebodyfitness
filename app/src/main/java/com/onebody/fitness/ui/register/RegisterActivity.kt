package com.onebody.fitness.ui.register

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.gson.JsonObject
import com.onebody.fitness.retrofit.SessionManager
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivityRegisterBinding
import com.onebody.fitness.retrofit.*
import com.onebody.fitness.ui.dashboard.ImagePickerActivity.TAG
import com.onebody.fitness.ui.dashboard.gender.GenderActivity
import com.onebody.fitness.ui.login.LoginActivity
import com.onebody.fitness.ui.other.AsteriskPasswordTransformationMethod
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONArray
import org.json.JSONObject

class RegisterActivity : AppCompatActivity() {
    lateinit var binding: ActivityRegisterBinding
    lateinit var context: Context
    private lateinit var mSessionManager: SessionManager
    var isShowText = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mSessionManager = SessionManager(applicationContext)

        btnRegister.setOnClickListener(View.OnClickListener {
            if (checkValidation()) {
                //generateMindBodyToken()
                signUpAPICall()
            }
        })

        edtPswd.transformationMethod = AsteriskPasswordTransformationMethod()
        edtConfPassword.transformationMethod = AsteriskPasswordTransformationMethod()

        binding.imgPasswordOn.setOnClickListener(View.OnClickListener {
            isShowText
            edtPswd.transformationMethod = PasswordTransformationMethod.getInstance()
            binding.edtPswd.setSelection(binding.edtPswd.length())
            binding.imgPasswordOn.visibility = View.GONE
            binding.imgPasswordOff.visibility = View.VISIBLE
        })

        binding.imgPasswordOff.setOnClickListener(View.OnClickListener {
            isShowText = false
            edtPswd.transformationMethod = HideReturnsTransformationMethod.getInstance()
            binding.edtPswd.setSelection(binding.edtPswd.length())
            binding.imgPasswordOff.visibility = View.GONE
            binding.imgPasswordOn.visibility = View.VISIBLE
        })

        binding.imgConfPasswordOn.setOnClickListener(View.OnClickListener {
            isShowText
            edtConfPassword.transformationMethod = PasswordTransformationMethod.getInstance()
            binding.edtConfPassword.setSelection(binding.edtConfPassword.length())
            binding.imgConfPasswordOn.visibility = View.GONE
            binding.imgConfPasswordOff.visibility = View.VISIBLE
        })

        binding.imgConfPasswordOff.setOnClickListener(View.OnClickListener {
            isShowText = false
            edtConfPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
            binding.edtConfPassword.setSelection(binding.edtConfPassword.length())
            binding.imgConfPasswordOff.visibility = View.GONE
            binding.imgConfPasswordOn.visibility = View.VISIBLE
        })
    }

    private fun generateMindBodyToken() {
        val requestData = JsonObject()
        requestData.addProperty("Username", Constants.UrlPath.MINDBODY_USERNAME)
        requestData.addProperty("Password", Constants.UrlPath.MINDBODY_PASSWORD)

        ApiServiceProvider.getInstance(this, Constants.UrlPath.MINDBODY_ISSUE_TOKEN)
            .sendPostData(Constants.UrlPath.MINDBODY_ISSUE_TOKEN, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        mSessionManager.setData(
                            SessionManager.MINDBODY_TOKEN,
                            responseObj.getString("AccessToken")
                        )
                        val user: JSONObject = responseObj.getJSONObject("User")
                        mSessionManager.setData(
                            SessionManager.MINDBODY_USER_ID,
                            user.getString("Id")
                        )
                        Log.d(TAG, "generateMindBodyToken: "+responseObj.getString("AccessToken"))

                        getMindBodyClients()

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun getMindBodyClients() {

        ApiServiceProvider.getInstance(this, Constants.UrlPath.MINDBODY_GET_CLIENTS)
            .sendPostDataWithoutParams(
                Constants.UrlPath.MINDBODY_GET_CLIENTS,
                "",
                "",
                "",
                edtEmail.text.toString(),
                "",
                "",
                true,
                RetrofitListener { response ->
                    try {

                        val responseObj = JSONObject(response.body().toString())
                        Log.d("TAG", "getMindBodyClients: $responseObj")
                        val clients: JSONArray = responseObj.getJSONArray("Clients")
                        for (i in 0 until clients.length()) {
                            val jsonObject: JSONObject = clients.getJSONObject(i)
                            val suspensionInfo: JSONObject =
                                jsonObject.getJSONObject("SuspensionInfo")
                            if (edtEmail.text.toString() == jsonObject.getString("Email")) {
                                if (suspensionInfo.getBoolean("BookingSuspended")) {
                                    //signUpAPICall()
                                } else {
                                    Toast.makeText(
                                        this,
                                        "you are registered with Mindbody but, your booking is suspended",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            } else {
                                Toast.makeText(
                                    this,
                                    "You are not register with Mindbody... so, you can't login with this app",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }

                        Log.d("mindbody clients", "getMindBodyClients: $responseObj")

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })

    }

    fun checkValidation(): Boolean {

        if (AppUtils.isEditTextEmpty(binding.edtName)) {
            binding.edtName.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_name))
            return false
        } else if (AppUtils.isEditTextEmpty(binding.edtContact)) {
            binding.edtContact.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_contact))
            return false
        } else if (!AppUtils.isContact(binding.edtContact)) {
            binding.edtContact.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_invalid_contact))
            return false
        } else if (AppUtils.getEditTextValue(binding.edtContact)!!.length < 10) {
            binding.edtContact.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_less_contact))
            return false
        } else if (AppUtils.isEditTextEmpty(binding.edtEmail)) {
            binding.edtEmail.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_email))
            return false
        } else if (!AppUtils.isValidEmail(binding.edtEmail)) {
            binding.edtEmail.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_invalid_email))
            return false
        } else if (AppUtils.isEditTextEmpty(binding.edtPswd)) {
            binding.edtPswd.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_password))
            return false
        } else if (AppUtils.getEditTextValue(binding.edtPswd)!!.length < 3) {
            binding.edtPswd.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_less_pass))
            return false
        } else if (AppUtils.isEditTextEmpty(binding.edtConfPassword)) {
            binding.edtConfPassword.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_enter_conf_password))
            return false
        } else if (AppUtils.getEditTextValue(binding.edtPswd) != AppUtils.getEditTextValue(binding.edtConfPassword)) {
            binding.edtConfPassword.requestFocus()
            PsDialogs().error(this, getString(R.string.alert_do_not_match_pass))
            return false
        }

        return true
    }

    @SuppressLint("HardwareIds")
    private fun signUpAPICall() {

        val android_id: String = Settings.Secure.getString(
            applicationContext.contentResolver,
            Settings.Secure.ANDROID_ID
        )
        val os_version: String = android.os.Build.VERSION.RELEASE
        val device_type: String = android.os.Build.BRAND

        val requestData = JsonObject()
        requestData.addProperty("Name", AppUtils.getEditTextValue(edtName))
        requestData.addProperty("ContactNumber", AppUtils.getEditTextValue(edtContact))
        requestData.addProperty("Email", AppUtils.getEditTextValue(edtEmail))
        requestData.addProperty("Password", AppUtils.getEditTextValue(edtPswd))
        requestData.addProperty("ConfirmPassword", AppUtils.getEditTextValue(edtConfPassword))
        requestData.addProperty("DeviceToken", android_id)
        requestData.addProperty("DeviceType", "android")
        requestData.addProperty("OSVersion", os_version)
        requestData.addProperty("DeviceName", device_type)
        ApiServiceProvider.getInstance(this)
            .sendPostData(Constants.UrlPath.SIGNUP, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")
                        if (status) {


                            val data = responseObj.getJSONArray("data")

                            val token = responseObj.getString("token")

                            mSessionManager.setData(SessionManager.AUTH_TOKEN, token)
                            val email = data.getJSONObject(0).getString("email")
                            val psw = data.getJSONObject(0).getString("psw")

                            SessionManager(this).setUserLogin(true)

                            mSessionManager.setData(SessionManager.EMAIL, email)
                            mSessionManager.setData(SessionManager.PASSWORD, psw)

                            mSessionManager.setData(SessionManager.IS_FROM_PROFILE, false)

                            val i = Intent(this, GenderActivity::class.java)
                            startActivity(i)
                            finish()
                            Animatoo.animateSlideLeft(this)

                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    override fun onBackPressed() {
        SessionManager(applicationContext).setUserLogin(false)
        val intent = Intent(this, LoginActivity::class.java)
        intent.flags =
            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
        finish()
    }

}