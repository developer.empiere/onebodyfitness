package com.onebody.fitness.ui.other

class MyClothConstantsFemale {

    val clothId: String = "ClothId"
    val clothStyle: String = "ClothStyle"

    companion object
    {
        enum class CLOTH_STYLE_PROPERTY_FEMALE{
            LEGGIES, TANK, SORTS, TSHIRT, SWEATS
        }
    }
}