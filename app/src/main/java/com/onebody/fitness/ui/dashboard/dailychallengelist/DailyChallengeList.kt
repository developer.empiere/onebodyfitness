package com.onebody.fitness.ui.dashboard.dailychallengelist

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.onebody.fitness.databinding.ActivityDailyChallengeListBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.dailychallange.ArchivedWorkOutModel
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject

class DailyChallengeList : AppCompatActivity() {

    lateinit var binding: ActivityDailyChallengeListBinding

    private var dailyChallengeListAdapter: DailyChallengeListAdapter? = null
    private var layoutManager: LinearLayoutManager? = null
    private var dailyChallengeListModel: MutableList<DailyChallengeListModel.Data>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDailyChallengeListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getDailyChallenge()
/*
        if (savedInstanceState != null) {
            sec = savedInstanceState.getInt("seconds");
            is_running = savedInstanceState.getBoolean("running");
            was_running = savedInstanceState .getBoolean("wasRunning");
        }
        running_Timer();
    }*/


        ivPrevious.setOnClickListener(View.OnClickListener {
            onBackPressed()
            Animatoo.animateSlideRight(this)

        })

        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)

            Animatoo.animateSlideLeft(this)

        })
    }

    private fun getDailyChallenge() {
        val requestData = JsonObject()

        ApiServiceProvider.getInstance(this)
            .sendPostData(Constants.UrlPath.GET_DAILY_CHALLENGE, requestData, true,
                RetrofitListener { response ->
                    try {
                        val responseObj = JSONObject(response.body().toString())
                        val status = responseObj.getBoolean("status")
                        val message = responseObj.getString("message")

                        if (status) {

                            val data = responseObj.getJSONArray("data")

                            dailyChallengeListModel = Gson().fromJson<Any>(data.toString(),
                                object :TypeToken<MutableList<DailyChallengeListModel.Data>?>() {}.type) as MutableList<DailyChallengeListModel.Data>
                            object : TypeToken<MutableList<ArchivedWorkOutModel.Data?>?>() {}.type

                            setAdapterDailyChallengeList()

                        } else {
                            Toast.makeText(this, "" + message, Toast.LENGTH_LONG).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                })
    }

    private fun setAdapterDailyChallengeList() {
        dailyChallengeListAdapter = DailyChallengeListAdapter(this, dailyChallengeListModel)
        layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.rvDailyChallengeList.layoutManager = layoutManager
        binding.rvDailyChallengeList.adapter = dailyChallengeListAdapter
    }
}