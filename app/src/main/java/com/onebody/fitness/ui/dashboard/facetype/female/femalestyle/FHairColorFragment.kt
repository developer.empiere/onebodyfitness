package com.onebody.fitness.ui.dashboard.facetype.female.femalestyle

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.onebody.fitness.R

class FHairColorFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_f_hair_color, container, false)
    }

    companion object {

        fun newInstance(param1: String, param2: String) =
            FHairColorFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}