package com.onebody.fitness.ui.dashboard.setting

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.gson.JsonObject
import com.onebody.fitness.retrofit.SessionManager
import com.onebody.fitness.R
import com.onebody.fitness.databinding.ActivitySettingBinding
import com.onebody.fitness.retrofit.ApiServiceProvider
import com.onebody.fitness.retrofit.Constants
import com.onebody.fitness.retrofit.RetrofitListener
import com.onebody.fitness.ui.dashboard.notification.NotificationActivity
import com.onebody.fitness.ui.login.ChangePasswordActivity
import kotlinx.android.synthetic.main.activity_setting.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONObject


class SettingActivity : AppCompatActivity() {

    lateinit var binding: ActivitySettingBinding
    private lateinit var mSessionManager: SessionManager
    var loginType: String = ""
    private val mGoogleSignInClient: GoogleSignInClient? = null
    val auth: Auth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySettingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mSessionManager = SessionManager(applicationContext)

        ivPrevious.setOnClickListener(View.OnClickListener {
            Animatoo.animateSlideRight(this)
            onBackPressed()
        })

        ivNotification.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, NotificationActivity::class.java)
            startActivity(intent)
        })

        loginType = mSessionManager.getData(SessionManager.IIS_SOCIAL_LOGIN)!!

        rlLogout.setOnClickListener(View.OnClickListener {
            showPopupWindowwLogout(this.rlLogout)
        })

        rlDelete.setOnClickListener(View.OnClickListener {
            showPopupWindowwDelete(this.rlDelete)
        })

        rlPassword.setOnClickListener(View.OnClickListener {
            val i = Intent(this, ChangePasswordActivity::class.java)
            startActivity(i)
            Animatoo.animateSlideLeft(this)

        })

        if (loginType.equals("1")) {
            binding.rlPassword.visibility = View.GONE
        } else {
            binding.rlPassword.visibility = View.VISIBLE
        }
    }

    @SuppressLint("ClickableViewAccessibility", "InflateParams")
    fun showPopupWindowwLogout(view: View) {
        val inflater = view.context.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView: View = inflater.inflate(R.layout.alert_logout, null)
        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = false

        val btn_yes_logout: Button = popupView.findViewById(R.id.btn_yes_logout) as Button
        val btn_no_logout: Button = popupView.findViewById(R.id.btn_no_logout) as Button

        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(view, Gravity.CENTER, width, height)
        //ErrorMsgPopUp.dimBehind(popupWindow)

        dimBehind(popupWindow)

        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }

        btn_no_logout.setOnClickListener(View.OnClickListener {
            popupWindow.dismiss()
        })

        btn_yes_logout.setOnClickListener(View.OnClickListener {
            logoutUser()
        })
    }

    @SuppressLint("ClickableViewAccessibility", "InflateParams")
    fun showPopupWindowwDelete(view: View) {
        val inflater =
            view.context.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val popupView: View = inflater.inflate(R.layout.alert_delete_account, null)
        val width = LinearLayout.LayoutParams.WRAP_CONTENT
        val height = LinearLayout.LayoutParams.WRAP_CONTENT
        val focusable = false

        val btn_yes_delete: Button = popupView.findViewById(R.id.btn_yes_delete) as Button
        val btn_no_delete: Button = popupView.findViewById(R.id.btn_no_delete) as Button

        val popupWindow = PopupWindow(popupView, width, height, focusable)
        popupWindow.showAtLocation(view, Gravity.CENTER, width, height)

        dimBehind(popupWindow)

        popupView.setOnTouchListener { v, event ->
            popupWindow.dismiss()
            true
        }

        btn_no_delete.setOnClickListener(View.OnClickListener {
            popupWindow.dismiss()
        })

        btn_yes_delete.setOnClickListener(View.OnClickListener {
            deleteAccount()
        })
    }

    private fun deleteAccount() {
        var jsonObject = JsonObject()

        ApiServiceProvider.getInstance(this).sendPostData(
            Constants.UrlPath.DELETE_ACCOUNT, jsonObject, true,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")
                    val message = responsObj.getString("message")

                    if (status) {
                        SessionManager(applicationContext).setUserLogin(false)
                        SessionManager(this).logoutUser()
                    }
                    else{
                        Toast.makeText(this,""+message,Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {

                    e.printStackTrace()
                }
            })
    }


    fun dimBehind(searchPopup: PopupWindow) {
        val container: View = if (searchPopup.background == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                searchPopup.contentView.parent as View
            } else {
                searchPopup.contentView
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                searchPopup.contentView.parent.parent as View
            } else {
                searchPopup.contentView.parent as View
            }
        }
        val context = searchPopup.contentView.context
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val p = container.layoutParams as WindowManager.LayoutParams
        p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
        p.dimAmount = 0.5f
        wm.updateViewLayout(container, p)
    }

    private fun logoutUser() {

        var jsonObject = JsonObject()

        ApiServiceProvider.getInstance(this).sendPostData(
            Constants.UrlPath.LOGOUT, jsonObject, true,
            RetrofitListener { response ->
                try {
                    val responsObj = JSONObject(response.body().toString())
                    val status = responsObj.getBoolean("status")
                    val message = responsObj.getString("message")

                    if (status) {
                        SessionManager(applicationContext).setUserLogin(false)
                        SessionManager(this).logoutUser()
                    }
                    else{
                        Toast.makeText(this,""+message,Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {

                    e.printStackTrace()
                }
            })
    }

    override fun onBackPressed() {
        finish()
    }
}