package com.onebody.fitness.ui.dashboard.clothtypemale

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.onebody.fitness.R

class ClothTypeMaleAdapter(
    private val context: Context?,
    var clothTypeMaleModel: MutableList<ClothTypeMaleModel.Data>
) : RecyclerView.Adapter<ClothTypeMaleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_cloth_male_style, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val cm = clothTypeMaleModel?.get(position)


        if (cm!!.cloths != null) {
            Glide.with(context!!)
                .load(cm!!.cloths)
                .placeholder(R.drawable.ic_onebody_app)
                .into(holder.imgCloth)
        }
    }

    override fun getItemCount(): Int {
        return clothTypeMaleModel?.size!!
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imgCloth: ImageView = itemView.findViewById(R.id.imgCloth)
    }
}