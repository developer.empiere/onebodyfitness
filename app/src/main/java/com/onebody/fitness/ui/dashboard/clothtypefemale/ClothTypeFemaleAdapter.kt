package com.onebody.fitness.ui.dashboard.clothtypefemale

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.onebody.fitness.R

class ClothTypeFemaleAdapter(private val context: Context?,
                             var clothTypeFemaleModel: MutableList<ClothTypeFemaleModel.Data>?
) : RecyclerView.Adapter<ClothTypeFemaleAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_cloth_female_style, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val cm = clothTypeFemaleModel?.get(position)

        if (cm!!.cloths != null) {
            Glide.with(context!!)
                .load(cm!!.cloths)
                .placeholder(R.drawable.ic_onebody_app)
                .into(holder.imgCloth)
        }
    }

    override fun getItemCount(): Int {
        return clothTypeFemaleModel?.size!!
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imgCloth: ImageView = itemView.findViewById(R.id.imgCloth)
    }
}