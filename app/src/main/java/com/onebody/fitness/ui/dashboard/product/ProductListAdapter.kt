package com.onebody.fitness.ui.dashboard.product

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.onebody.fitness.R

class ProductListAdapter(
    private val context: Context?,
    val dailyListModel: ArrayList<ProductModel>?
) : RecyclerView.Adapter<ProductListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_product_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.cvProduct.setOnClickListener(View.OnClickListener {

            val intent = Intent(context, ProductDetailActivity::class.java)
            context!!.startActivity(intent)
        })
    }

    override fun getItemCount(): Int {
        return 10
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        var cvProduct: CardView = itemView.findViewById(R.id.cvProduct)

    }
}
