package com.onebody.fitness.ui.dashboard.shoes

import java.io.Serializable

class ShoesDetailModel : Serializable {
    var isSelected: Boolean = false
    var id: String = ""
    var imgID: Int = 0

    constructor(isSelected: Boolean, id: String, imgID: Int) {
        this.isSelected = isSelected
        this.id = id
        this.imgID = imgID

    }
}