package com.onebody.fitness.retrofit;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiServices {

    @Multipart
    @POST
    Call<JsonElement> doUpdateProfile(@Url String url,
                                      @Part MultipartBody.Part Name,
                                      @Part MultipartBody.Part Gender,
                                      @Part MultipartBody.Part Age,
                                      @Part MultipartBody.Part Height,
                                      @Part MultipartBody.Part Weight,
                                      @Part MultipartBody.Part ProfilePic);

    @POST()
    Call<JsonElement> sendPostData(@Url String url, @Body JsonObject requestData);

    @POST()
    Call<JsonElement> sendPostDataWithoutParams(@Url String url);

    @POST()
    Call<JsonElement> sendPostSkinMale(@Url String url, @Body JsonObject requestData);

    @POST()
    Call<JsonElement> sendPlaceBidData(@Url String url, @Body JsonObject requestData);

    @GET()
    Call<JsonElement> sendGetData(@Url String url);

    @POST()
    @FormUrlEncoded
    Call<JsonElement> sendGetDataSkin(@Url String url, @FieldMap HashMap<String, String> map);

    @GET
    Call<JsonElement> sendGetData(@Url String url, @Query("limit") String limit,
                                  @Query("offset") String offset,
                                  @Query("clientIDs") String clientIDs,
                                  @Query("searchText") String searchText,
                                  @Query("isProspect") String isProspect,
                                  @Query("lastModifiedDate") String lastModifiedDate);

    @POST()
    @FormUrlEncoded
    Call<JsonElement> sendGetDataSearch(@Url String url, @FieldMap HashMap<String, String> map);

    @Multipart
    @POST
    Call<JsonElement> sendOfficialHostImage(@Url String url,
                                            @Part("type") RequestBody requestData,
                                            @Part MultipartBody.Part image);
}
