package com.onebody.fitness.retrofit;

import android.content.Context;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiServiceProvider extends RetrofitBase {

    private ApiServices apiServices;
    private LoadingDialog mDialog;
    private String mApiUrl;

    public ApiServiceProvider(Context context) {
        super(context, Constants.UrlPath.API_URL, true);
        apiServices = retrofit.create(ApiServices.class);
        mDialog = new LoadingDialog(context);
    }

    private boolean checkIsGrantedAPI(String mApiUrl) {
        return true;
    }

    private void dismissDialog() {
        if (mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
    }

    public ApiServiceProvider(Context context, String baseUrl) {
        super(context, baseUrl, true);
        apiServices = retrofit.create(ApiServices.class);
        mDialog = new LoadingDialog(context);
    }

    public static ApiServiceProvider getInstance(Context context) {
        return new ApiServiceProvider(context);
    }

    private void manageResponse(Response response, RetrofitListener retrofitListener) {
        switch (response.code()) {
            case Constants.ResponseCode.CODE_200:
            case Constants.ResponseCode.CODE_204:
                validateResponse(response, retrofitListener);
                break;
            case Constants.ResponseCode.CODE_500:
            case Constants.ResponseCode.CODE_400:
            case Constants.ResponseCode.CODE_401:
            case Constants.ResponseCode.CODE_403:
            case Constants.ResponseCode.CODE_404:
            case Constants.ResponseCode.CODE_422:
            case Constants.ResponseCode.CODE_428:
            case Constants.ResponseCode.CODE_429:
                try {
                    if (checkIsGrantedAPI(mApiUrl)) {
                        validateResponse(response, retrofitListener);
                    } else {
                        JSONObject resObj = new JSONObject(response.errorBody().string());
                        if (resObj.has("message")) {
                            AppUtils.INSTANCE.showAlertDialog(context, resObj.getString("message"), "Ok");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public static ApiServiceProvider getInstance(Context context, String baseUrl) {
        return new ApiServiceProvider(context, baseUrl);
    }

    public void sendPostData(String url, JsonObject requestData, Boolean isProgressShown, final RetrofitListener retrofitListener) {

        if (isProgressShown)
            mDialog.show();

        mApiUrl = url;
        Call<JsonElement> call = apiServices.sendPostData(url, requestData);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                dismissDialog();
                manageResponse(response, retrofitListener);
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                dismissDialog();
                t.printStackTrace();
                new ConnectionError(context, t).setListener(() -> sendPostData(url, requestData, isProgressShown, retrofitListener));
            }
        });
    }

    public void sendHashmapPostData(String url, HashMap<String, String> map, Boolean isProgressShown, final RetrofitListener retrofitListener) {
        if (isProgressShown)
            mDialog.show();

        mApiUrl = url;

        Call<JsonElement> call = apiServices.sendGetDataSkin(url, map);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                dismissDialog();
                manageResponse(response, retrofitListener);
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                dismissDialog();
                t.printStackTrace();
                new ConnectionError(context, t).setListener(() -> sendHashmapPostData(url, map, isProgressShown, retrofitListener));
            }
        });
    }
    public void updateProfile(String url, HashMap<String, String> map, MultipartBody.Part file, Boolean isProgressShown, final RetrofitListener retrofitListener) {
        if (isProgressShown)
            mDialog.show();
        mApiUrl = url;
        MultipartBody.Part Name = MultipartBody.Part.createFormData("Name", map.get("Name"));
        MultipartBody.Part Gender = MultipartBody.Part.createFormData("Gender", map.get("Gender"));
        MultipartBody.Part Age = MultipartBody.Part.createFormData("Age", map.get("Age"));
        MultipartBody.Part Height = MultipartBody.Part.createFormData("Height", map.get("Height"));
        MultipartBody.Part Weight = MultipartBody.Part.createFormData("Weight", map.get("Weight"));

        Call<JsonElement> call = apiServices.doUpdateProfile(mApiUrl, Name, Gender, Age, Height, Weight, file);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                dismissDialog();
                manageResponse(response, retrofitListener);
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                dismissDialog();
                t.printStackTrace();
                new ConnectionError(context, t).setListener(() -> updateProfile(url, map, file, isProgressShown, retrofitListener));
            }
        });
    }

    public void updateProfileFromHome(String url, HashMap<String, String> map, MultipartBody.Part file, Boolean isProgressShown, final RetrofitListener retrofitListener) {
        if (isProgressShown)
            mDialog.show();

        mApiUrl = url;
        MultipartBody.Part Name = MultipartBody.Part.createFormData("Name", map.get("Name"));
        MultipartBody.Part Gender = MultipartBody.Part.createFormData("Gender", map.get("Gender"));
        MultipartBody.Part Age = MultipartBody.Part.createFormData("Age", map.get("Age"));
        MultipartBody.Part Height = MultipartBody.Part.createFormData("Height", map.get("Height"));
        MultipartBody.Part Weight = MultipartBody.Part.createFormData("Weight", map.get("Weight"));

        Call<JsonElement> call = apiServices.doUpdateProfile(mApiUrl, Name, Gender, Age, Height, Weight, file);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                dismissDialog();
                manageResponse(response, retrofitListener);
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                dismissDialog();
                t.printStackTrace();
                new ConnectionError(context, t).setListener(() -> updateProfile(url, map, file, isProgressShown, retrofitListener));
            }
        });
    }

    public void sendPostDataWithoutParams(String url, String limit, String offset, String clientID, String searchText, String isProspect, String lastDate, Boolean isProgressShown,
                                          final RetrofitListener retrofitListener) {

        if (isProgressShown)
            mDialog.show();

        mApiUrl = url;
        Call<JsonElement> call = apiServices.sendGetData(url, limit, offset, clientID, searchText, isProspect, lastDate);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                dismissDialog();
                manageResponse(response, retrofitListener);
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                dismissDialog();
                t.printStackTrace();
                new ConnectionError(context, t).setListener(() -> sendPostDataWithoutParams(url, limit, offset, clientID, searchText, isProspect, lastDate, isProgressShown, retrofitListener));
            }
        });
    }

    public void sendHashmapPostDataSearch(String url, HashMap<String, String> map, Boolean isProgressShown,
                                          final RetrofitListener retrofitListener) {

        if (isProgressShown)
            mDialog.show();

        mApiUrl = url;
        Call<JsonElement> call = apiServices.sendGetDataSearch(url,map);
        call.enqueue(new Callback<JsonElement>() {

            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                dismissDialog();
                manageResponse(response, retrofitListener);
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                dismissDialog();
                t.printStackTrace();
                new ConnectionError(context, t).setListener(() -> sendHashmapPostDataSearch(url, map,isProgressShown, retrofitListener));
            }
        });
    }
}

