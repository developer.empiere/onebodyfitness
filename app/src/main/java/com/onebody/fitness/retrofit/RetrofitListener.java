package com.onebody.fitness.retrofit;


import retrofit2.Response;

public interface RetrofitListener {
    void onResponseSuccess(Response response);
    //void onResponseError(ErrorObject errorObject, Throwable throwable, String apiFlag);
}