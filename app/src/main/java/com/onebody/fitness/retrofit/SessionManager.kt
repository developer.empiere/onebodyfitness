package com.onebody.fitness.retrofit

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import androidx.fragment.app.Fragment
import com.onebody.fitness.ui.dashboard.DashBoardActivity
import com.onebody.fitness.ui.login.LoginActivity


class SessionManager(private val _context: Context?) {

    private val pref: SharedPreferences
    private val introPref: SharedPreferences
    private val editor: Editor
    private val introEditor: Editor
    private val fragment: Fragment? = null
    private val PRIVATE_MODE = 0

    fun setData(key: String?, value: String?) {
        editor.putString(key, value)
        editor.commit()
    }

    fun setData(key: String, value: Boolean) {
        editor.putBoolean(key, value)
        editor.commit()
    }

    fun setData(key: String, value: Int) {
        editor.putInt(key, value)
        editor.commit()
    }

    fun setLanguage(key: String, value: String) {
        editor.putString(key, value)
        editor.commit()
    }

    fun setUserLogin(isUserLogin: Boolean) {
        editor.putBoolean(IS_USER_LOGIN, isUserLogin)
        editor.commit()
    }

    fun checkLogin() {
        isLoggedIn = pref.getBoolean(IS_USER_LOGIN, false)
        if (isLoggedIn) {
            val i = Intent(_context, DashBoardActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            _context!!.startActivity(i)
        }
    }

    fun signup() {
        /*  if () {
              // val i = Intent(_context, LoginActivity::class.java)
               i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
               _context!!.startActivity(i)*//*
        } else {
            val i = Intent(_context, DashBoardActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            _context!!.startActivity(i)
        }*/

    }

    var isLoggedIn: Boolean = false


    fun getData(key: String?): String? {
        return pref.getString(key, "")
    }

    fun getData(key: String?, defaultValue: String?): String? {
        return pref.getString(key, defaultValue)
    }

    fun getAuthToken(key: String?): String? {
        return pref.getString(key, "")
    }

    fun getIntData(key: String?): Int {
        return pref.getInt(key, 11)
    }

    fun getLanguage(key: String?, defaultValue: String): String? {
        return pref.getString(key, defaultValue)
    }

    fun getBooleanData(key: String?): Boolean {
        return pref.getBoolean(key, true)
    }

    fun getBooleanData(key: String?, defaultValue: Boolean): Boolean {
        return pref.getBoolean(key, defaultValue)
    }

    fun logoutUser() {
        editor.clear()
        editor.commit()
        introEditor.clear()
        introEditor.commit()
        val i = Intent(_context, LoginActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        _context!!.startActivity(i)
    }

    companion object {

        private const val PREFER_NAME = "4hire"
        private const val INTRO_PREFER_NAME = "4hire_intro"
        private const val IS_USER_LOGIN = "is_user_loggedIn"
        const val USER_ID = "id"
        const val MINDBODY_USER_ID = "mindbody_user_id"
        const val AUTH_TOKEN = "auth_token"
        const val MINDBODY_TOKEN = "mindbody_token"
        const val EMAIL = "email"
        const val TIME = "time"
        const val WEIGHT = "weight"
        const val REPS = "reps"
        const val USER_NAME = "name"
        const val IIS_SOCIAL_LOGIN = "its_social_login"
        const val AVATAR_STATUS = "avatarStatus"
        const val PASSWORD = "password"
        const val IS_FROM_PROFILE = "is_from_profile"
        const val IS_MALE = "IS_MALE"
        const val VIDEOID = "VIDEOID"
    }

    init {
        pref = _context!!.getSharedPreferences(PREFER_NAME, PRIVATE_MODE)
        introPref = _context!!.getSharedPreferences(INTRO_PREFER_NAME, PRIVATE_MODE)
        editor = pref.edit()
        introEditor = introPref.edit()
    }
}


