package com.onebody.fitness.retrofit

import android.app.Activity
import android.content.Context
import com.onebody.fitness.R
import org.aviran.cookiebar2.CookieBar

class PsDialogs() {

    fun error(context: Context?, message: String) {
        CookieBar.build(context as Activity?)
            .setTitle(context!!.getString(R.string.title_alert))
            .setMessage(message)
            .setBackgroundColor(R.color.red_light)
            .setCookiePosition(CookieBar.TOP)
            .show();

    }

    fun success(context: Context?, message: String) {
        CookieBar.build(context as Activity?)
            .setTitle(context!!.getString(R.string.title_success))
            .setMessage(message)
            .setBackgroundColor(R.color.white)
            .setCookiePosition(CookieBar.TOP)
            .show();

    }
}