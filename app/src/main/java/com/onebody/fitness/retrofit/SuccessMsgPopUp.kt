package com.onebody.fitness.retrofit

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.onebody.fitness.R
import com.skydoves.elasticviews.ElasticImageView

class SuccessMsgPopUp {
    companion object {
        fun showPopupWindow(view: View?, msgPopUp: String) {
            val inflater =
                view!!.context.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val popupView: View = inflater.inflate(R.layout.popup_success_msg, null)
            val width = LinearLayout.LayoutParams.MATCH_PARENT
            val height = LinearLayout.LayoutParams.WRAP_CONTENT
            val focusable = false
            var txtSuccessMsg: TextView = popupView.findViewById(R.id.txtSuccessMsg) as TextView
            txtSuccessMsg.setText(msgPopUp)
            var imgSuccess: ElasticImageView =
                popupView.findViewById(R.id.imgSuccess) as ElasticImageView

            val popupWindow = PopupWindow(popupView, width, height, focusable)
            popupWindow.showAtLocation(view, Gravity.CENTER, width, height)
            dimBehind(popupWindow)

            popupView.setOnTouchListener { v, event ->
                popupWindow.dismiss()
                true
            }

            imgSuccess.setOnClickListener(View.OnClickListener {
                popupWindow.dismiss()
            })
        }
        fun showTitlePopupWindow(view: View?, msgPopUp: String, title: String) {
            val inflater =
                view!!.context.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val popupView: View = inflater.inflate(R.layout.popup_success_msg, null)
            val width = LinearLayout.LayoutParams.MATCH_PARENT
            val height = LinearLayout.LayoutParams.WRAP_CONTENT
            val focusable = false
            var txtSuccessMsg: TextView = popupView.findViewById(R.id.txtSuccessMsg) as TextView
            txtSuccessMsg.setText(msgPopUp)
            var imgSuccess: ElasticImageView = popupView.findViewById(R.id.imgSuccess) as ElasticImageView
            var tvTitle: TextView = popupView.findViewById(R.id.tvTitle) as TextView
            tvTitle.setText(title)

            val popupWindow = PopupWindow(popupView, width, height, focusable)
            popupWindow.showAtLocation(view, Gravity.CENTER, width, height)
            dimBehind(popupWindow)

            popupView.setOnTouchListener { v, event ->
                popupWindow.dismiss()
                true
            }

            imgSuccess.setOnClickListener(View.OnClickListener {
                popupWindow.dismiss()
            })

        }



        fun dimBehind(searchPopup: PopupWindow) {
            val container: View = if (searchPopup.background == null) {
                searchPopup.contentView.parent as View
            } else {
                searchPopup.contentView.parent.parent as View
            }
            val context = searchPopup.contentView.context
            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val p = container.layoutParams as WindowManager.LayoutParams
            p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
            p.dimAmount = 0.5f
            wm.updateViewLayout(container, p)
        }
    }
}