package com.onebody.fitness.retrofit

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

object Constants {
    lateinit var USERID: Any

    // var USERID = "UserID"
    val REQUEST_IMAGE: Int = 104

    const val CAMERA = 101
    /*interface validationKey {
        companion object {
            val name1_exp by lazy { "^[a-zA-Z]+[\-'\s]?[a-zA-Z ]+$" }
        }
    }*/

    interface RequestPermission {
        companion object {
            const val REQUEST_GALLERY = 1000
            const val REQUEST_CAMERA = 1001
            const val REQUEST_CODE = 100
        }
    }

    interface RequestCode {
        companion object {
            const val REQUEST_FROM_EMAIL_ADAPTER = 1002
            const val REQUEST_FROM_PHONE = 1003
            const val REQUEST_FROM_CONTACT_REQUEST_ADAPTER = 1004
            const val REQUEST_FOR_REFFER_CODE = 1005
        }
    }

    interface VerifyType {
        companion object {
            const val REQUEST_LOGIN_REQUEST = "LOGIN_REQUEST"
            const val REQUEST_REGISTRATION_REQUEST = "REGISTRATION_REQUEST"
            const val REQUEST_RESET_PASSWORD = "reset_password"
            const val REQUEST_MOBILE_VERIFICATION = "MOBILE_VERIFICATION_REQUEST"
            const val REQUEST_CHANGE_REQUEST = "change_request"
        }
    }

    interface TimeOut {
        companion object {
            const val SPLASH_TIME_OUT = 2000
            const val IMAGE_UPLOAD_CONNECTION_TIMEOUT = 120
            const val IMAGE_UPLOAD_SOCKET_TIMEOUT = 120
            const val SOCKET_TIME_OUT = 60
            const val CONNECTION_TIME_OUT = 60
        }
    }

    interface DateFormat {
        companion object {
            const val DATE_INPUT_FORMAT = "yyyy-mm-dd"
            const val YEAR_OUTPUT_FORMAT = "yyyy"
            const val DAY_OUTPUT_FORMAT = "dd"
            const val MONTH_OUTPUT_FORMAT = "MM"
        }
    }

    interface UrlPath {

        companion object {

            //mindbody Credential
            const val MINDBODY_API_KEY = "ac92cb140f8249c797273c9f4342fff6"
            const val MINDBODY_SITEID = "-99"
            const val MINDBODY_USERNAME = "Siteowner"
            const val MINDBODY_PASSWORD = "apitest1234"

            const val MINDBODY_URL = "https://api.mindbodyonline.com/public/v6/"
            const val API_URL = "http://1body1fitness.jdinfotech.net:3000/api/"

            const val LOGIN = "login"
            const val LOGOUT = "logout"
            const val FORGOT_PASSWORD = "forgotpassword"
            const val VERIFY_OTP = "verifyOTP"
            const val SIGNUP = "createaccount"
            const val CHANGE_PSWD = "changepassword"
            const val FORGOT_PSWD = "frgtpassword_setnewpsw"
            const val GET_SKINS_MALE = "getskinsmale"
            const val GET_HAIR_COLOR_MALE = "haircolormale"
            const val GET_EYE_COLOR_MALE = "eyecolormale"
            const val GET_FACIAL_HAIR_MALE = "facialhairmale"
            const val GET_SUBMIT_AVATAR_MALE = "submitmaleavatar"
            const val GET_PREVIEWAVTAR = "previewavtar"
            const val GET_SUBMIT_AVATAR_FEMALE = "submitfemaleavatar"
            const val GET_HAIR_STYLE_MALE = "hairstylemale"
            const val GET_SKIN_COLOR_FEMALE = "getskinsfemale"
            const val GET_HAIR_COLOR_FEMALE = "haircolorfemale"
            const val GET_EYE_COLOR_FEMALE = "eyecolorfemale"
            const val GET_HAIR_STYLE_FEMALE = "hairstylefemale"
            const val GET_SHOES_COLOR_FEMALE = "shoosescolorfemale"
            const val SET_USER_PROFILE = "getprofile"
            const val UPDATE_USER_PROFILE = "updateprofile"
            const val API_KEY = "a3586603-76d4-4a22-9cfe-b7c01676a82c"
            const val GET_REFERRAL_CODE = "user/apply-referral"
            const val BODYTYPEMALE = "bodytypemale"
            const val BODYTYPEFEMALE = "bodytypefemale"
            const val GET_CLOTHING_SHORT_MALE = "clothingshortsmale"
            const val GET_CLOTHING_SHORT_FEMALE = "clothingshortfemale"
            const val MY_ARCHIVED_WORKOUTS = "myarchivedworkouts"
            const val MOVEMENT = "movements"
            const val MOVEMENT_SEARCH = "movement_search"
            const val SUBMITWORKOUT = "submitworkout"
            const val GOALARCHIVEDWORKOUT = "goalarchivedworkout"
            const val GET_DAILY_CHALLENGE = "getdailychallenge"
            const val GET_MY_LEADER_BOARD_LIST = "myleaderboardlist"
            const val GET_LEVELS = "levels"
            const val GET_LEVELS_SUBMIT = "levels_submit"
            const val GET_ALL_WORK_OUTS = "getallworkouts"
            const val MY_CHAT_LIST = "chat_mychatlist"
            const val CHAT_CONTACT_LIST = "chat_contactlist"
            const val CREATE_CHAT = "chat_createchat"
            const val SEND_MESSAGE = "chat_sendmessage"
            const val CHAT_MESSAGE_LIST = "chat_chatmessagelist"
            const val DELETE_ACCOUNT = "deleteaccount"
            const val GET_MY_PROGRESS = "getmyprogress"
            const val CONTACTUS = "contactus"
            const val HOWITWORKS = "howitworks"
            const val CHAT_COUNTUPDATE = "chat_countupdate"


            //mind body sub url's
            const val MINDBODY_ISSUE_TOKEN = MINDBODY_URL + "usertoken/issue/"
            const val MINDBODY_GET_CLIENTS = MINDBODY_URL + "client/clients/"

        }
    }

    interface LocalPath {
        companion object {
            const val IMAGE = "/Media/Images/"
        }
    }

    interface ErrorClass {
        companion object {
            const val CODE = "code"
            const val STATUS = "status"
            const val MESSAGE = "message"
            const val DEVELOPER_MESSAGE = "developerMessage"
        }
    }

    interface ResponseCode {
        companion object {
            const val CODE_200 = 200
            const val CODE_204 = 204
            const val CODE_500 = 500
            const val CODE_400 = 400
            const val CODE_401 = 401
            const val CODE_403 = 403
            const val CODE_404 = 404
            const val CODE_422 = 422
            const val CODE_428 = 428
            const val CODE_429 = 429
            const val SESSION_EXPIRED = "-1"
        }
    }

    //Check Permisson
    fun Check_STORAGE(context: Context?): Boolean {
        val result =
            ContextCompat.checkSelfPermission(context!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return result == PackageManager.PERMISSION_GRANTED
    }

}