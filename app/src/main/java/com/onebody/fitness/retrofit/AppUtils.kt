package com.onebody.fitness.retrofit

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.ParseException
import android.net.Uri
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.PopupWindow
import androidx.core.graphics.drawable.DrawableCompat
import com.onebody.fitness.R
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.SimpleDateFormat
import java.util.*

object AppUtils {

    fun printHashKey(pContext: Context) {
        try {
            val info = pContext.packageManager.getPackageInfo("com.ideal.gamez",PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
    }

    fun dimBehind(searchPopup: PopupWindow) {
        val container: View = if (searchPopup.background == null) {
            searchPopup.contentView.parent as View
        } else {
            searchPopup.contentView.parent.parent as View
        }
        val context = searchPopup.contentView.context
        val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val p = container.layoutParams as WindowManager.LayoutParams
        p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND
        p.dimAmount = 0.5f
        wm.updateViewLayout(container, p)
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view: View? = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
    }

   /* fun showAlertDialog(context: Context?, message: String?, positiveButton: String?) {
        val alertDialog = AlertDialog(context)
        alertDialog.setCancelable(false)
        alertDialog.message = message
        alertDialog.setPositveButton(positiveButton, View.OnClickListener { alertDialog.dismiss() })
        alertDialog.show()
    }*/

    fun isEditTextEmpty(editText: EditText): Boolean {
        return editText.text.toString().trim { it <= ' ' } == ""
    }

    fun getEditTextValue(editText: EditText): String? {
        return editText.text.toString()
    }

    fun isValidEmail(editText: EditText): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(getEditTextValue(editText)).matches()
//        return CharSequence?.isValidEmail() = !isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

    }

    fun ConvertChatingTime(date: String): String? {
        if (date.isEmpty()) return ""
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")   /// 2022-02-11T11:57:32.000Z
        sdf.timeZone = TimeZone.getTimeZone("UTC")
        val date1 = sdf.parse(date)
        val outputFormat = SimpleDateFormat("hh:mm a", Locale.ENGLISH)
        outputFormat.timeZone = TimeZone.getTimeZone("Asia/Kolkata")
        return outputFormat.format(date1)
    }
    fun ConvertChatingDate(date: String): String? {
        if (date.isEmpty()) return ""
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")   /// 2022-02-11T11:57:32.000Z
        sdf.timeZone = TimeZone.getTimeZone("UTC")
        val date1 = sdf.parse(date)
        val outputFormat = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
        outputFormat.timeZone = TimeZone.getTimeZone("Asia/Kolkata")
        return outputFormat.format(date1)
    }


    fun isContact(editText: EditText): Boolean {
        return Patterns.PHONE.matcher(getEditTextValue(editText)).matches()
    }

    fun getColoredDrawable(context: Context, imgDrawable: Drawable, hasFocus: Boolean): Drawable {
        var drawable: Drawable? = imgDrawable
        drawable = DrawableCompat.wrap(drawable!!)
        if (hasFocus)
            DrawableCompat.setTint(drawable, context.resources.getColor(R.color.colorPrimary))
        return drawable
    }

    fun getColoredDrawable(context: Context,imgDrawable: Drawable,hasFocus: Boolean,editText: EditText): Drawable {
        var drawable: Drawable? = imgDrawable
        drawable = DrawableCompat.wrap(drawable!!)
        if (hasFocus)
            DrawableCompat.setTint(drawable, context.resources.getColor(R.color.colorPrimary))
        else
            if (editText.text.toString().isEmpty())
                DrawableCompat.setTint(drawable, Color.LTGRAY)
        return drawable
    }

    fun getColoredDrawable(context: Context,imgDrawable: Drawable,editText: EditText): Drawable {
        var drawable: Drawable? = imgDrawable //Your drawable image
        drawable = DrawableCompat.wrap(drawable!!)
        if (editText.text.toString().length > 0)
            DrawableCompat.setTint(drawable, context.resources.getColor(R.color.colorPrimary))
        else
            if (!editText.hasFocus())
                DrawableCompat.setTint(drawable, Color.LTGRAY)
        // Set whatever color you want
        return drawable
    }

    fun getRealPathFromURI(context: Context, contentUri: Uri): String? {
        var result: String? = null
        val cursor = context.contentResolver.query(contentUri, null, null, null, null)
        if (cursor == null) {
            result = contentUri.path
        } else {
            if (cursor.moveToFirst()) {
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                result = cursor.getString(idx)
            }
            cursor.close()
        }
        return result
    }

    fun getAge(DOB_Year: Int, DOB_Month: Int, DOB_Day: Int): Int {
        var age: Int
        val calenderToday = Calendar.getInstance()
        val currentYear = calenderToday[Calendar.YEAR]
        val currentMonth = 1 + calenderToday[Calendar.MONTH]
        val todayDay = calenderToday[Calendar.DAY_OF_MONTH]
        age = currentYear - DOB_Year
        if (DOB_Month > currentMonth) {
            --age
        } else if (DOB_Month == currentMonth) {
            if (DOB_Day > todayDay) {
                --age
            }
        }
        return age
    }

    fun getTodayDate(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val currentDate = sdf.format(Date())
        return currentDate
    }

    @Throws(ParseException::class)
    fun formatDateFromDateString(inputDateFormat: String?,outputDateFormat: String?,inputDate: String?): String? {
        val mParsedDate: Date
        val mOutputDateString: String
        val mInputDateFormat = SimpleDateFormat(inputDateFormat, Locale.getDefault())
        val mOutputDateFormat = SimpleDateFormat(outputDateFormat, Locale.getDefault())
        mParsedDate = mInputDateFormat.parse(inputDate)
        mOutputDateString = mOutputDateFormat.format(mParsedDate)
        return mOutputDateString
    }
    fun showAlertDialog(context: Context, message: String, Ok: String) {

    }
}